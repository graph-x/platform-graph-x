<?php

namespace Tests\Feature;

use App\Address;
use App\Client;
use App\ClientContact;
use App\Division;
use App\Item;
use App\Project;
use App\Quantity;
use App\Supplier;
use App\User;
use Tests\TestCase;

class CreateItemsTest extends TestCase
{

    /** @test */
    public function items_can_be_added_to_project()
    {
        $this->withoutExceptionHandling();
        // Given we have a logged in admin

        $admin = User::create([
            'role' => 'admin',
            'name' => 'admin',
            'email' => 'admin@example.com',
            'password' => bcrypt('password'),
        ]);

        // Set up
        $client = Client::create([
            'name' => 'Client 1',
            'phone' => '123 123 123',
            'address_line_1' => 'Lararoad 8',
            'city' => 'Laraville',
            'province' => 'Laprovince',
            'country' => 'Laraland',
            'postal_code' => 'PC123',
        ]);

        $user = User::create([
            'role' => 'supplier',
            'name' => 'Some user supplier',
            'email' => 'supplier@example.com',
            'password' => bcrypt('password'),
        ]);

        $supplierOne = Supplier::create([
            'user_id' => $user->id,
            'name' => 'Supplier name',
            'phone' => '123 123 123',
            'address_line_1' => 'Some stuff',
            'city' => 'laraville',
            'province' => 'random provincius',
            'country' => 'countrius',
            'postal_code' => '123123',
        ]);

        $supplierTwo = Supplier::create([
            'user_id' => $user->id,
            'name' => 'Supplier two Name',
            'phone' => '123 123 123',
            'address_line_1' => 'Some stuff',
            'city' => 'laraville',
            'province' => 'random provincius',
            'country' => 'countrius',
            'postal_code' => '123123',
        ]);

        $division = Division::create([
            'client_id' => $client->id,
            'name' => 'Client 1 Division',
        ]);

        // Given we have a project
        $project = Project::create([
            'client_id' => $client->id,
            'division_id' => $division->id,
            'start' => now(),
            'end' => now()->addMonth(),
            'title' => 'Example project title',
            'po_number' => '123456',
            'status' => 'in_progress',
        ]);

        $address = Address::create([
            'client_id' => $client->id,
            'address_line_1' => 'Belveu road 123',
            'city' => 'Laraville',
            'province' => 'Laprovince',
            'country' => 'Laraland',
            'postal_code' => 'PC123',
            'types' => ["physical_approval", "delivery"],
        ]);

        $deliveryClientContact = ClientContact::create([
            'client_id' => $client->id,
            'name' => 'Client Contact Name',
            'phone' => '123456',
            'mobile' => '123456',
            'email' => 'client-contact@example.com',
            'roles' => ["buyer", "pdf_approval", "physical_approval", "accounting", "delivery"],

        ]);

        $existingItem = Item::create([
            'project_id' => $project->id,
            'name' => 'random item name',
            'description' => 'Existing Item description',
            'delivery_deadline' => now()->subMonth(),
            'address_id' => $address->id,
            'delivery_contact_id' => $deliveryClientContact->id,
        ]);

        $quantity100 = Quantity::create([
            'value' => 100,
        ]);

        $existingItem->quantities()->attach($quantity100);
        $existingItem->suppliers()->attach($supplierOne);

        // When the user submits item data to item creation endpoint, then the items should be persisted to the DB.
        $response = $this->actingAs($admin)->post("/api/projects/{$project->id}/items", [
            'items' => [
                [
                    'id' => null,
                    'name' => 'Name of the item one',
                    'description' => 'Description of the item one',
                    'delivery_deadline' => now()->addMonth(),
                    'address_id' => $address->id,
                    'delivery_contact_id' => $deliveryClientContact->id,
                    'quantities' => [
                        ['value' => 100],
                        ['value' => 200],
                        ['value' => 300],
                        ['value' => 400],
                        ['value' => $quantity100->value],
                    ],
                    'suppliers_ids' => [$supplierOne->id],
                ],
                [
                    'id' => null,
                    'name' => 'Name of the item2',
                    'description' => 'Description of the item2',
                    'delivery_deadline' => now()->addMonth(),
                    'address_id' => $address->id,
                    'delivery_contact_id' => $deliveryClientContact->id,
                    'quantities' => [
                        ['id' => null, 'value' => 422],
                        ['id' => null, 'value' => 500],
                        ['id' => $quantity100->id, 'value' => $quantity100->value],
                    ],
                    'suppliers_ids' => [$supplierOne->id],
                ],
                [
                    'id' => $existingItem->id,
                    'name' => 'Name item2',
                    'description' => 'Description of the item2',
                    'delivery_deadline' => now()->addMonth(),
                    'address_id' => $address->id,
                    'delivery_contact_id' => $deliveryClientContact->id,
                    'quantities' => [
                        ['id' => null, 'value' => 399],
                        ['id' => $quantity100->id, 'value' => $quantity100->value],
                    ],
                    'suppliers_ids' => [$supplierOne->id, $supplierTwo->id],
                ],
            ],
        ]);

        // $projectIndex = $this->actingAs($admin)->get("/api/projects/{$project->id}/items");
        // dd($projectIndex);

        // Then the items should be created, and project should have the item count
        $this->assertCount(3, $project->projectItems);
        $this->assertCount(2, $existingItem->quantities);
        $this->assertCount(2, $existingItem->suppliers);
    }
}