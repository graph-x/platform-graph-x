<?php

namespace Tests\Feature;

use App\User;
use App\Client;
use App\Project;
use App\Supplier;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GlobalSearchTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function admin_can_search_resources_globally()
    {
        $admin = factory(User::class)->create(['role' => 'admin']);

        // Given we have 12 resources, 4 projects, 4 clients and 4 suppliers
        // 3 of each, whose title / name begins with same 3 letters
        factory(Project::class, 3)->create(['title' => 'AAAA']);
        factory(Project::class)->create(['title' => 'random title, exclude out of search']);

        factory(Client::class, 3)->create(['name' => 'AAAB']);
        factory(Client::class)->create(['name' => 'random name, exclude out of search']);

        factory(Supplier::class, 3)->create(['name' => 'AAAC']);
        factory(Supplier::class)->create(['name' => 'random name, exclude out of search']);

        // When a search is submitted that includes those 3 letters
        $response = $this->actingAs($admin)->json('GET', '/api/search?query=aaa');
        // Then the response should contain 9 results, grouped

        $this->assertEquals(9, collect($response->json())->count());
        $this->assertTrue(collect($response->json())->pluck('title')->contains('AAAA'));
        $this->assertTrue(collect($response->json())->pluck('title')->contains('AAAB'));
        $this->assertTrue(collect($response->json())->pluck('title')->contains('AAAC'));
        $this->assertFalse(collect($response->json())->pluck('title')->contains('random'));
    }
}
