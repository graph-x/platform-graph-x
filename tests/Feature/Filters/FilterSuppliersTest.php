<?php

namespace Tests\Feature\Filters;

use App\Supplier;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FilterSuppliersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admin_can_filter_suppliers_based_on_their_name_in_asc_or_desc()
    {
        auth()->login(factory(User::class)->create(['role' => 'admin']));

        // Given we have 3 suppliers
        $supplierA = factory(Supplier::class)->create(['name' => 'A']);
        $supplierB = factory(Supplier::class)->create(['name' => 'B']);
        $supplierC = factory(Supplier::class)->create(['name' => 'C']);

        $responseAsc = $this->get('/api/admin/suppliers?name=asc');

        $this->assertCount(3, $responseAsc['data']);

        $this->assertTrue($responseAsc['data'][0]['id'] === $supplierA['id']);
        $this->assertTrue($responseAsc['data'][1]['id'] === $supplierB['id']);
        $this->assertTrue($responseAsc['data'][2]['id'] === $supplierC['id']);
        $this->assertFalse($responseAsc['data'][0]['id'] === $supplierC['id']);

        $responseDesc = $this->get('/api/admin/suppliers?name=desc');

        $this->assertCount(3, $responseDesc['data']);

        $this->assertTrue($responseDesc['data'][0]['id'] === $supplierC['id']);
        $this->assertTrue($responseDesc['data'][1]['id'] === $supplierB['id']);
        $this->assertTrue($responseDesc['data'][2]['id'] === $supplierA['id']);
        $this->assertFalse($responseDesc['data'][0]['id'] === $supplierB['id']);
    }

    /** @test */
    public function admin_can_quick_search_suppliers_based_on_their_name()
    {
        auth()->login(factory(User::class)->create(['role' => 'admin']));

        // Given we have 3 suppliers
        $supplierA = factory(Supplier::class)->create(['name' => 'Ana']);
        $supplierB = factory(Supplier::class)->create(['name' => 'Boba']);

        $responseAsc = $this->get('/api/admin/suppliers?search=ana');

        $this->assertCount(1, $responseAsc['data']);

        $this->assertTrue($responseAsc['data'][0]['id'] === $supplierA['id']);
    }
}
