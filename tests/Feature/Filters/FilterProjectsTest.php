<?php

namespace Tests\Feature\Filters;

use App\Client;
use App\Project;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FilterProjectsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admin_can_filter_projects_for_the_current_month()
    {
        $this->artisan('db:seed --class=StatusTableSeeder');
        $user = factory(User::class)->create([
            'role' => 'admin',
        ]);

        auth()->login($user);

        // Given we have 3 projects
        // One was created in previous month, and two in this month
        $projectInPreviousMonth = factory(Project::class)->create([
            'created_at' => now()->subMonth(2),
        ]);
        $projectInCurrentMonthA = factory(Project::class)->create();
        $projectInCurrentMonthB = factory(Project::class)->create();

        // When filters are applied
        $response = $this->get("/api/projects?month=current");

        // Then two projects should be returned
        $filtered = collect($response->json())->pluck('unique_code');
        $this->assertNotContains($projectInPreviousMonth->unique_code, $filtered);
        $this->assertContains($projectInCurrentMonthA->unique_code, $filtered);
        $this->assertContains($projectInCurrentMonthB->unique_code, $filtered);
    }

    /** @test */
    public function admin_can_filter_projects_that_were_created_in_the_previous_month_from_now()
    {
        $this->artisan('db:seed --class=StatusTableSeeder');
        $user = factory(User::class)->create([
            'role' => 'admin',
        ]);

        auth()->login($user);

        // Given we have 3 projects
        // One was created in previous month, and two in this month
        $projectInPreviousMonth = factory(Project::class)->create([
            'created_at' => (new Carbon('first day of last month'))->addDays(5),
        ]);
        $projectInCurrentMonthA = factory(Project::class)->create();
        $projectInCurrentMonthB = factory(Project::class)->create();

        // When filters are applied
        $response = $this->get("/api/projects?month=previous");

        // Then two projects should be returned
        $filteredProjects = collect($response->json())->pluck('unique_code');
        $this->assertContains($projectInPreviousMonth->unique_code, $filteredProjects);
        $this->assertNotContains($projectInCurrentMonthA->unique_code, $filteredProjects);
        $this->assertNotContains($projectInCurrentMonthB->unique_code, $filteredProjects);
    }

    /** @test */
    public function admin_can_filter_projects_that_were_created_in_last_three_months_from_today()
    {
        $this->artisan('db:seed --class=StatusTableSeeder');
        $user = factory(User::class)->create([
            'role' => 'admin',
        ]);

        auth()->login($user);

        // Given we have 3 projects
        // One was created in previous month, and two in this month
        $projectInPreviousTwoMonths = factory(Project::class)->create([
            'created_at' => now()->subMonth(2)->startOfMonth(),
        ]);
        $projectInCurrentMonthA = factory(Project::class)->create();
        $projectInCurrentMonthB = factory(Project::class)->create();

        // When filters are applied
        $response = $this->get("/api/projects?month=lastThree");

        // Then two projects should be returned
        $filteredProjects = collect($response->json())->pluck('unique_code');
        $this->assertContains($projectInPreviousTwoMonths->unique_code, $filteredProjects);
        $this->assertContains($projectInCurrentMonthA->unique_code, $filteredProjects);
        $this->assertContains($projectInCurrentMonthB->unique_code, $filteredProjects);
    }

    /** @test */
    public function admin_can_filter_all_projects_that_were_created_ever_by_all_months()
    {
        $this->artisan('db:seed --class=StatusTableSeeder');
        $user = factory(User::class)->create([
            'role' => 'admin',
        ]);

        auth()->login($user);

        // Given we have 3 projects
        // One was created in previous month, and two in this month
        $projectInPreviousTwoMonths = factory(Project::class)->create();
        $projectInCurrentMonthA = factory(Project::class)->create();
        $projectInCurrentMonthB = factory(Project::class)->create();

        // When filters are applied
        $response = $this->get("/api/projects?month=all");

        // Then two projects should be returned
        $filteredProjects = collect($response->json())->pluck('unique_code');
        $this->assertContains($projectInPreviousTwoMonths->unique_code, $filteredProjects);
        $this->assertContains($projectInCurrentMonthA->unique_code, $filteredProjects);
        $this->assertContains($projectInCurrentMonthB->unique_code, $filteredProjects);
    }

    /** @test */
    public function admin_can_filter_projects_according_to_their_status()
    {
        $this->artisan('db:seed --class=StatusTableSeeder');
        $user = factory(User::class)->create([
            'role' => 'admin',
        ]);

        auth()->login($user);
        $clientA = factory(Client::class)->create();
        $clientB = factory(Client::class)->create();

        $projectWithFilteredStatus = factory(Project::class)->create([
            'status' => 'quote_requests_to_be_made',
        ]);

        $otherProject = factory(Project::class)->create([
            'status' => 'awaiting_supplier_prices',
        ]);

        $response = $this->get("/api/projects?status=quote_requests_to_be_made");

        $filteredProjects = collect($response->json())->pluck('unique_code');
        $this->assertContains($projectWithFilteredStatus->unique_code, $filteredProjects);
        $this->assertNotContains($otherProject->unique_code, $filteredProjects);
    }

    /** @test */
    public function admin_can_filter_projects_according_to_their_clients()
    {
        $this->artisan('db:seed --class=StatusTableSeeder');
        $user = factory(User::class)->create([
            'role' => 'admin',
        ]);

        auth()->login($user);
        $clientA = factory(Client::class)->create();
        $clientB = factory(Client::class)->create();

        $projectBelongingToClientA = factory(Project::class)->create([
            'client_id' => $clientA->id,
        ]);

        $projectBelongingToClientB = factory(Project::class)->create([
            'client_id' => $clientB->id,
        ]);

        // When filters are applied
        $response = $this->get("/api/projects?client={$clientA->id}");

        // Then two projects should be returned
        $filteredProjects = collect($response->json())->pluck('unique_code');
        $this->assertContains($projectBelongingToClientA->unique_code, $filteredProjects);
        $this->assertNotContains($projectBelongingToClientB->unique_code, $filteredProjects);
    }

    /** @test */
    public function admin_can_quick_search_projects_according_to_clients_name()
    {
        $this->artisan('db:seed --class=StatusTableSeeder');
        $user = factory(User::class)->create([
            'role' => 'admin',
        ]);

        auth()->login($user);
        $clientA = factory(Client::class)->create(['name' => 'Searchio Testa']);
        $clientA = factory(Client::class)->create(['name' => 'Brother Testa']);
        $clientB = factory(Client::class)->create(['name' => 'Willnot Show']);

        $projectBelongingToClientA = factory(Project::class)->create([
            'client_id' => $clientA->id,
        ]);
        $projectBelongingToClientB = factory(Project::class)->create([
            'client_id' => $clientA->id,
        ]);

        $projectBelongingToClientC = factory(Project::class)->create([
            'client_id' => $clientB->id,
        ]);

        // When filters are applied
        $response = $this->get("/api/projects?search=testa");

        // Then two projects should be returned
        $filteredProjects = collect($response->json())->pluck('unique_code');
        $this->assertContains($projectBelongingToClientA->unique_code, $filteredProjects);
        $this->assertContains($projectBelongingToClientB->unique_code, $filteredProjects);
        $this->assertNotContains($projectBelongingToClientC->unique_code, $filteredProjects);
    }
}
