<?php

namespace Tests\Feature\Filters;

use App\Client;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FilterClientsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function admin_can_filter_clients_based_on_their_name_in_asc_or_desc()
    {
        auth()->login(factory(User::class)->create(['role' => 'admin']));

        // Given we have 3 clients
        $clientA = factory(Client::class)->create(['name' => 'A']);
        $clientB = factory(Client::class)->create(['name' => 'B']);
        $clientC = factory(Client::class)->create(['name' => 'C']);

        $responseAsc = $this->get('/api/clients?name=asc');

        $this->assertCount(3, $responseAsc['data']);

        $this->assertTrue($responseAsc['data'][0]['id'] === $clientA['id']);
        $this->assertTrue($responseAsc['data'][1]['id'] === $clientB['id']);
        $this->assertTrue($responseAsc['data'][2]['id'] === $clientC['id']);
        $this->assertFalse($responseAsc['data'][0]['id'] === $clientC['id']);

        $responseDesc = $this->get('/api/clients?name=desc');

        $this->assertCount(3, $responseDesc['data']);

        $this->assertTrue($responseDesc['data'][0]['id'] === $clientC['id']);
        $this->assertTrue($responseDesc['data'][1]['id'] === $clientB['id']);
        $this->assertTrue($responseDesc['data'][2]['id'] === $clientA['id']);
        $this->assertFalse($responseDesc['data'][0]['id'] === $clientB['id']);
    }

    /** @test */
    public function admin_can_quick_search_clients_based_on_their_name()
    {
        auth()->login(factory(User::class)->create(['role' => 'admin']));

        // Given we have 3 clients
        $clientA = factory(Client::class)->create(['name' => 'Ana']);
        $clientB = factory(Client::class)->create(['name' => 'Boba']);

        $responseAsc = $this->get('/api/clients?search=ana');

        $this->assertCount(1, $responseAsc['data']);

        $this->assertTrue($responseAsc['data'][0]['id'] === $clientA['id']);
    }
}