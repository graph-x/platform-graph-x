<?php

namespace Tests\Feature\Invoicing;

use App\Item;
use App\Client;
use App\Project;
use App\Invoice;
use App\Address;
use App\Supplier;
use App\ClientContact;
use App\InvoiceLineItem;
use Illuminate\Support\Str;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InvoiceTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function invoice_can_be_created()
    {
        // Arrange - given we have required conditions for creating an invoice
        // Project
        // Client
        // Supplier that is assigned to the project
        $client = factory(Client::class)->create();

        $clientContact =  factory(ClientContact::class)->create([
            'client_id' => $client->id
        ]);

        $clientAddress = factory(Address::class)->create([
                'client_id' => $client->id
        ]);

        $project = factory(Project::class)->create([
            'client_id' => $client['id'],
            'status' => 'work_completed_billing'
        ]);

        $item = factory(Item::class)->create([
            'project_id' => $project->id,
            'delivery_deadline' => now()->addMonth(),
            'address_id' => $clientAddress->id,
            'delivery_contact_id' => $clientContact->id,
        ]);

        $supplier = factory(Supplier::class)->create();

        $project->suppliers()->sync($supplier['id']);

        // Need to approve the item here, so we can create a invoice line from it.
        // If there are no approved items, no invoice lines can be created.

        // When we create an invoice for that project
        $unique_invoice_id = Str::upper(Str::substr(Str::orderedUuid(), 0, 20));

        $invoice = Invoice::create([
            'unique_code' => "IN-{$unique_invoice_id}",
            'project_id' => $project->id,
            'client_id' => $project->id,
            'client_name' => $client->name,
            'title' => 'Invoice for project',
            'description' => 'Description of the invoice',
            'due_date' => now()->addMonth(),
            'paid' => false,
            'status' => 'open', // ['open', 'paid', 'draft']
            'currency' => 'CAD',
            'project_management_title' => 'Ive done some management',
            'project_management_description' => 'Ive done some management description',
            'project_management_fee' => 1000, // GESTION DE PROJET -- separate item
            'subtotal' => 40000, // SOUS-TOTAL
            'tps_tax_rate' => 0.05, // in % TPS - taxes Goods and services tax 5%
            'tvq_tax_rate' => 0.09975, // in % TVQ, - taxes  9.975%
            'tps_amount' => 100, // TPS amount should be calculated automatically from the line items
            'tvq_amount' => 100, // TVQ amount should be calculated from the line items
            'total_amount' => (1000 + 40000 + 100 + 100 + 4000), // need to sum all of the InvoiceLineItems.
            'profit' => 4000, // total profit includes project management fee + profit per item?
        ]);

        // Store prices in cents instead of the hundreds ?
        // 10 usd = 1000 cents
        foreach ($project->approvedQuotes() as $quote) {
            // need to get approved items -> item_supplier
            // need to create invoice line item from that
            $invoice_line_item = InvoiceLineItem::create([
                'invoice_id' => $invoice->id,
                'project_id' => $project->id,
                'item_id' => $item->id,
                'supplier_id' => $supplier->id, // because we need to get the approved/submission price.
                'title' => $item->name,
                'description' => $item->description,
                'cost_price' => 15000, // PRIX COÙTANT,
                'customer_price' => 18000, // PRIX CLIENT approved price + admin profit (approved price === cost price atm)
                'approved_price' => 15000,
                'delivery_date' => now()->subDay(),
                'profit_type' => 'fixed_amount',
                'admin_profit' => 3000, // BÉNÉFICE PER ITEM / Profit per item for admin,
                'custom_created' => false
            ]);
        }




        // Than it should contain proper information and cost prices (taxes, and other)
    }

    /** @test */
    public function creating_invoice()
    {
        // Arrange
        // Given that we have a project with some items, and a client that 'owns' the project
        // And that project is assigned to certain supplier who submitted a quote / proposal which was accepted
        // And the project is completed (supplier work is finished) work_completed_billing status
        // Then the Invoice should become available (created)
        $client = factory(Client::class)->create();
        $project = factory(Project::class)->create([
            'client_id' => $client['id'],
            'status' => 'work_completed_billing'
        ]);
        $supplier = factory(Supplier::class)->create();
        $project->suppliers()->sync($supplier['id']);

        // Act / When
        // When we visit a certain endpoint /project/{projectId}/invoice

        // Assert / Then
        // Then the invoice should contain proper data - InvoiceLines
    }
}
