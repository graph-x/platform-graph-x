<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdditionalColumnsToProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->date('estimates_end_date')->nullable()->after('notes');
            $table->boolean('choose_estimates_automatically')->default(false)->after('estimates_end_date');
        });

        Artisan::call('projects:update-estimates-end-date');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn(['estimates_end_date', 'choose_estimates_automatically']);
        });
    }
}
