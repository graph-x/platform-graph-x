<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUniqueConstraintOnQuotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotes', function (Blueprint $table) {
            $table->unique(['supplier_id', 'project_id', 'item_id', 'option_group_id', 'quantity_id'], 'quotes_unique');
            $table->dropUnique(['supplier_id', 'project_id', 'item_id', 'option_group_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotes', function (Blueprint $table) {
            $table->unique(['supplier_id', 'project_id', 'item_id', 'option_group_id']);
        });
    }
}
