<?php

use App\Invoice;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unique_code')->unique();
            $table->unsignedBigInteger('project_id');
            $table->unsignedBigInteger('client_id');
            $table->string('client_name')->nullable();
            $table->string('title');
            $table->longText('description');
            $table->timestamp('due_date')->nullable();
            $table->boolean('paid')->default(false);
            $table->string('status')->default(Invoice::OPEN);
            $table->string('currency')->default(Invoice::DEFAULT_CURRENCY);
            $table->decimal('tps_tax_rate', $precision = 5, $scale = 5)->default(Invoice::TPS_TAX_RATE);
            $table->decimal('tvq_tax_rate', $precision = 5, $scale = 5)->default(Invoice::TVQ_TAX_RATE);
            $table->decimal('subtotal', $precision = 8, $scale = 2)->nullable();
            $table->decimal('tps_amount', $precision = 8, $scale = 2)->nullable();
            $table->decimal('tvq_amount', $precision = 8, $scale = 2)->nullable();
            $table->decimal('total_amount', $precision = 8, $scale = 2)->nullable();
            $table->decimal('profit', $precision = 8, $scale = 2)->nullable();
            $table->timestamp('notified_at')->nullable();
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
