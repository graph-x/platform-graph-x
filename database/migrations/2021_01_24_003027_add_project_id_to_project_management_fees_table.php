<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProjectIdToProjectManagementFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_management_fees', function (Blueprint $table) {
            $table->unsignedBigInteger('project_id')->after('invoice_id')->nullable();
            $table->unsignedBigInteger('invoice_id')->nullable()->change();
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_management_fees', function (Blueprint $table) {
            $table->dropForeign('project_management_fees_project_id_foreign');
            $table->dropColumn('project_id');
        });
    }
}
