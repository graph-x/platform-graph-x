<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSentAtToSupplierProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('supplier_projects', function (Blueprint $table) {
            $table->timestamp('quotes_request_sent_at')->nullable();
            $table->timestamp('quotes_submitted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('supplier_projects', function (Blueprint $table) {
            $table->dropColumn('quotes_request_sent_at');
            $table->dropColumn('quotes_submitted_at');
        });
    }
}