<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeStringToDecimalColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('quotes', function (Blueprint $table) {
            $table->decimal('value', $precision = 8, $scale = 2)->nullable()->change();
            $table->decimal('value_for_client_approval', $precision = 8, $scale = 2)->nullable()->change();

            $table->decimal('admin_profit', $precision = 8, $scale = 2)->nullable();
            $table->boolean('custom_made')->default(false)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotes', function (Blueprint $table) {
            $table->string('value')->nullable()->change();
            $table->string('value_for_client_approval')->nullable()->change();
            $table->dropColumn(['admin_profit', 'custom_made']);
        });
    }
}
