<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdditionalColumnsToItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->unsignedBigInteger('parent_item_id')->nullable()->after('id');
            $table->string('hash_id')->unique()->after('parent_item_id')->nullable();
            $table->boolean('is_linked')->default(false)->after('hash_id');

            $table->foreign('parent_item_id')->references('id')->on('items')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('items', function (Blueprint $table) {
            $table->dropForeign('parent_item_id');
            $table->dropColumn(['hash_id', 'is_linked']);
        });
    }
}
