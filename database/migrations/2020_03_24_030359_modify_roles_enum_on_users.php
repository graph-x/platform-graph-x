<?php

use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyRolesEnumOnUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $originals = User::all();
        
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('role');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->enum('role', ['admin', 'client', 'supplier'])->after('id')->default('admin');
        });

        User::chunk(500, function ($results) use($originals) {
            foreach ($results as $result) {
                $original = $originals->where('id', $result->id)->first();

                if ($original) {
                    $result->update(['role' => $original->role]);
                }
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('role');            
        });

        Schema::table('users', function (Blueprint $table) {
            $table->enum('role', ['admin', 'supplier'])->after('id')->default('admin');
        });
    }
}
