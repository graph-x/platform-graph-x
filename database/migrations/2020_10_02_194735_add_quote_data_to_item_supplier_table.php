<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQuoteDataToItemSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_supplier', function (Blueprint $table) {
            $table->longText('quote_data')->nullable()->after('item_data');
            $table->timestamp('notified_at')->nullable()->after('quote_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_supplier', function (Blueprint $table) {
            $table->dropColumn(['quote_data', 'notified_at']);
        });
    }
}
