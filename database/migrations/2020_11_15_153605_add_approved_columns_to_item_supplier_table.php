<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddApprovedColumnsToItemSupplierTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_supplier', function (Blueprint $table) {
            $table->timestamp('quotes_approved_at')->nullable()->after('notified_at');
            $table->timestamp('quotes_declined_at')->nullable()->after('quotes_approved_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_supplier', function (Blueprint $table) {
            $table->dropColumn(['quotes_approved_at', 'quotes_declined_at']);
        });
    }
}
