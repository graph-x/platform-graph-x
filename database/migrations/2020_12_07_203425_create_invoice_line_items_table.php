<?php

use App\InvoiceLineItem;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceLineItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_line_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('project_id');
            $table->unsignedBigInteger('invoice_id');
            $table->unsignedBigInteger('item_id')->nullable();
            $table->unsignedBigInteger('supplier_id');
            $table->string('title');
            $table->longText('description');
            $table->string('quantity');
            $table->string('supplier_name')->nullable();
            $table->decimal('approved_price', $precision = 8, $scale = 2)->nullable();
            $table->decimal('cost_price', $precision = 8, $scale = 2)->nullable();
            $table->decimal('customer_price', $precision = 8, $scale = 2)->nullable();
            $table->timestamp('delivery_date')->nullable();
            $table->string('profit_type')->default(InvoiceLineItem::FIXED_AMOUNT_PROFIT);
            $table->decimal('admin_profit', $precision = 8, $scale = 2)->nullable();
            $table->boolean('custom_made')->default(false);
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            $table->foreign('invoice_id')->references('id')->on('invoices')->onDelete('cascade');
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_line_items');
    }
}
