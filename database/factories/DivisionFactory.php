<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use App\Division;
use Faker\Generator as Faker;

$factory->define(Division::class, function (Faker $faker) {
    return [
        'client_id' => function () {
            return factory(Client::class)->create()->id;
        },
        'name' => $faker->company
    ];
});
