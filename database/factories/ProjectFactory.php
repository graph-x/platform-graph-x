<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use App\Project;
use App\Address;
use App\Division;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    $statusKey = 'quote_requests_to_be_made';
    return [
        'favorite' => $faker->boolean,
        'unique_code' => Str::upper(Str::substr(Str::orderedUuid(), 0, 20)),
        'address_id' => function () {
            return factory(Address::class)->create()->id;
        },
        'client_id' => function () {
            return factory(Client::class)->create()->id;
        },
        'division_id' => function () {
            return factory(Division::class)->create()->id;
        },
        'start' => $faker->date(),
        'end' => $faker->date(),
        'title' => $faker->words(3, true),
        'short_description' => $faker->realText(20),
        'po_number' => $faker->postcode,
        'status' => $statusKey,
        'require_pdf' => false,
        'require_physical' => false
    ];
});
