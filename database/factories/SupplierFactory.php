<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Supplier;
use App\User;
use Faker\Generator as Faker;

$factory->define(Supplier::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'name' => $faker->name,
        'phone' => '(555) 555-5555',
        'address_line_1' => $faker->streetAddress,
        'city' => $faker->city,
        'province' => $faker->word,
        'country' => $faker->countryCode,
        'postal_code' => $faker->postcode,
    ];
});