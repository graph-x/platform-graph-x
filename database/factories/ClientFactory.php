<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use App\User;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(User::class)->create()->id;
        },
        'name' => "{$faker->firstName} {$faker->lastName}",
        'phone' => '(555) 555-5555',
        'address_line_1' => $faker->streetAddress,
        'city' => $faker->city,
        'province' => $faker->word,
        'country' => $faker->countryCode,
        'postal_code' => $faker->postcode,
    ];
});