<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use App\ClientContact;
use Faker\Generator as Faker;

$factory->define(ClientContact::class, function (Faker $faker) {
    return [
        'client_id' => function () {
            return factory(Client::class)->create()->id;
        },
        'name' => $faker->name,
        'phone' => "(555) 555-5555",
        'mobile' => "(555) 555-5555",
        'email' => $faker->email,
        'roles' => ["buyer", "pdf_approval", "physical_approval", "accounting", "invoicing", "delivery"],
    ];
});