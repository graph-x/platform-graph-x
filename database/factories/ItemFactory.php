<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Item;
use App\Address;
use App\Project;
use App\ClientContact;
use Faker\Generator as Faker;

$factory->define(Item::class, function (Faker $faker) {
    return [
        'project_id' => function ()  {
            return factory(Project::class)->create()->id;
        },
        'name' => $faker->word(),
        'description' => $faker->text(200),
        'delivery_deadline' => now()->addMonth(),
        'address_id' => function () {
            return factory(Address::class)->create()->id;
        },
        'delivery_contact_id' => function () {
            return factory(ClientContact::class)->create()->id;
        }
    ];
});
