<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use App\Address;
use Faker\Generator as Faker;

$factory->define(Address::class, function (Faker $faker) {
    return [
        'client_id' => function () {
            return factory(Client::class)->create()->id;
        },
        'address_line_1' => $faker->streetAddress,
        'city' => $faker->city,
        'province' => $faker->word,
        'country' => $faker->countryCode,
        'postal_code' => $faker->postcode,
        'types' => [Address::DELIVERY, Address::PHYSICAL_APPROVAL]
    ];
});
