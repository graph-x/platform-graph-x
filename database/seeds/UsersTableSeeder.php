<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'role' => 'admin',
            'name' => 'Paul Rousseau',
            'email' => 'graph-x@codable.com',
            'password' => bcrypt('print456!'),
        ]);

        $user->assignRole(Role::query()->admin());
    }
}