<?php

use App\Status;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (Status::$predefined as $status) {
            DB::table('statuses')->insert([
                'key' => $status['key'],
                'html_label' => $status['html_label'],
                'short_label' => $status['short_label'],
                'hex_color' => $status['color'],
            ]);
        }
    }
}
