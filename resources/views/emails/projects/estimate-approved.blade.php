@component('mail::message')
# Introduction

Your estimate for has been approved. <br/>

Item: {{ $item->name }}<br/>
Project: {{ $item->project->title}}

@component('mail::button', ['url' => $url])
Check it out
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
