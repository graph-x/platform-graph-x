@component('mail::message')
# Invoice generated

Hello, your invoice for {{ $project->title }} is ready. <br/>

Project name: <strong>{{ $project->title }}</strong><br/>
Project description: {!! nl2br($project->short_description) !!}

@component('mail::button', ['url' => $url])
Link to pdf
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
