@component('mail::message')
# Introduction

Hello, a new item is awaiting your estimates: <br/>

Project name: <strong>{{ $item->project->title }}</strong>

Item description:
<br/>
{!! nl2br($item->description) !!}

@component('mail::button', ['url' => ''])
Check it out
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
