@component('mail::message')
# Submission declined

Hello, submission for {{ $project->title }} has been declined by the client. <br/>
The project's status has been changed to 'En attente des prix fournisseurs' <br/>

Project name: <strong>{{ $project->title }}</strong><br/>
Project description: {!! nl2br($project->short_description) !!}

@component('mail::button', ['url' => $url])
Link to project
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
