@component('mail::message')
# Submission ready for review

Hello, submission for {{ $project->title }} is ready for review. <br/>

Project name: <strong>{{ $project->title }}</strong><br/>
Project description: {!! nl2br($project->short_description) !!}

@component('mail::button', ['url' => $url])
Link to project
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
