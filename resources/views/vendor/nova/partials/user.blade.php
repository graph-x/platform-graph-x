<!--dropdown-trigger class="flex items-center"-->
    <a class="block p-6 text-white no-underline" href="#">
        {{ $user->name ?? $user->email ?? __('Nova User') }}
    </a>
<!--/dropdown-trigger-->

{{-- 
<dropdown-menu slot="menu" width="200" direction="rtl">
    <ul class="list-reset">
        <li>
            <a href="{{ route('nova.logout') }}" class="block no-underline text-90 hover:bg-30 p-3">
                {{ __('Logout') }}
            </a>
        </li>
    </ul>
</dropdown-menu>
--}}