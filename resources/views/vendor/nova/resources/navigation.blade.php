@if (count(\Laravel\Nova\Nova::availableResources(request())))
    @foreach($navigation as $group => $resources)
        @foreach($resources as $resource)
            <router-link :to="{
                name: 'index',
                params: {
                    resourceName: '{{ $resource::uriKey() }}'
                }
            }" class="border-l-1 border-white-15% flex items-center px-6 py-6 text-white hover:bg-white hover:text-black no-underline uppercase">
                {{ $resource::label() }}
            </router-link>
        @endforeach                    
    @endforeach
@endif
