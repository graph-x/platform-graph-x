<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-full font-sans antialiased">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=1280">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ \Laravel\Nova\Nova::name() }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,800,800i,900,900i" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" rel="stylesheet">

    <!-- Vendor -->
    <link href="{{ asset('/vendor/mdi/css/materialdesignicons.min.css') }}" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('app.css', 'vendor/nova') }}">
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">

    <!-- Tool Styles -->
    @foreach(\Laravel\Nova\Nova::availableStyles(request()) as $name => $path)
        <link rel="stylesheet" href="/nova-api/styles/{{ $name }}">
    @endforeach

    <!-- Custom Meta Data -->
    @include('nova::partials.meta')

    <!-- Theme Styles -->
    @foreach(Nova::themeStyles() as $publicPath)
        <link rel="stylesheet" href="{{ $publicPath }}">
    @endforeach
</head>
@if(!$removeNavigation)
<body class="min-w-site bg-40 text-black min-h-full">
@else
<body class="bg-40 text-black min-h-full">
@endif
    <div id="nova">
        <div v-cloak class="flex flex-col min-h-screen">
            @if(!$removeNavigation)
                <!-- Sidebar -->
                <div class="px-6 bg-nav w-full">
                    <div class="flex justify-between">
                        <nav class="flex leading-tight text-sm -mx-6">
                            <a class="flex items-center px-6 py-6 text-white hover:bg-white hover:text-black no-underline" href="#">
                                <span class="fas fa-list-ul transform-180"></span>
                            </a>

                            <a class="flex items-center px-6 py-6 text-white hover:bg-white hover:text-black no-underline" href="#">
                                <span class="fas fa-search"></span>
                            </a>

                            @foreach (\Laravel\Nova\Nova::availableTools(request()) as $tool)
                                {!! $tool->renderNavigation() !!}
                            @endforeach
                        </nav>

                        <nav class="flex leading-tight text-sm -mx-6">
                            <a class="flex items-center px-6 py-6 text-white hover:bg-white hover:text-black no-underline" href="#">
                                {{ $user->name ?? $user->email ?? __('Nova User') }}
                            </a>
                            <a class="flex items-center px-6 py-6 text-white-50% hover:bg-white hover:text-black no-underline border-l-1 border-white-15%" href="#">
                                <span class="fas fa-portrait text-2xl"></span>
                            </a>
                            <a class="flex items-center px-6 py-6 text-white-50% hover:bg-white hover:text-black no-underline border-l-1 border-white-15%" href="#">
                                <span class="fas fa-cog text-2xl"></span>
                            </a>
                            <a class="flex items-center px-6 py-6 text-white-50% hover:bg-white hover:text-black no-underline border-l-1 border-white-15%" href="{{ route('nova.logout') }}">
                                <span class="fas fa-sign-in-alt text-2xl"></span>
                            </a>
                        </nav>
                    </div>
                </div>
            @endif
            <!-- Content -->
            <div class="content max-w-full flex flex-col flex-1">
                <div data-testid="content" class="px-view py-view mx-auto min-h-full w-full flex flex-col flex-1">

                    @yield('content')

                    {{-- @include('nova::partials.footer') --}}
                </div>
            </div>
        </div>
    </div>

    <script>
        window.config = @json(\Laravel\Nova\Nova::jsonVariables(request()));
    </script>

    @if(!$removeNavigation)
    <script>
        window.iframeMessage = {
            message: false
        }
    </script>
    @else
    <script src="https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/4.2.9/iframeResizer.contentWindow.min.js"></script>
    @endif

    <!-- Include Google Places API -->
    <script src="https://maps.googleapis.com/maps/api/js?key={{config('google-autocomplete.api_key')}}&libraries=places"></script>

    <!-- Scripts -->
    <script src="{{ mix('manifest.js', 'vendor/nova') }}"></script>
    <script src="{{ mix('vendor.js', 'vendor/nova') }}"></script>
    <script src="{{ mix('app.js', 'vendor/nova') }}"></script>

    <!-- Build Nova Instance -->
    <script>
        window.Nova = new CreateNova(config)
    </script>

    <!-- Tool Scripts -->
    @foreach (\Laravel\Nova\Nova::availableScripts(request()) as $name => $path)
        @if (\Illuminate\Support\Str::startsWith($path, ['http://', 'https://']))
            <script src="{!! $path !!}"></script>
        @else
            <script src="/nova-api/scripts/{{ $name }}"></script>
        @endif
    @endforeach

    <!-- Start Nova -->
    <script>
        Nova.liftOff()
    </script>
</body>
</html>
