@extends('layouts.app')

@section('content')
    <supplier-profile :data="{{ json_encode($supplier) }}"></supplier-profile>
@endsection
