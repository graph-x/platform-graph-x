@extends('layouts.app')

@section('content')
    <supplier-project-details
        :project="{{ $project }}"
        :client="{{ $client }}"
        :supplier="{{ $supplier }}"
        :statuses="{{ $statuses }}"
        :approved_estimates_count="{{ $approved_estimates_count }}">
    </supplier-project-details>
@endsection
