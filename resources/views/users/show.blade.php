@extends('layouts.app')

@section('content')
    <user-details :data="{{ $user }}"></user-details>
@endsection
