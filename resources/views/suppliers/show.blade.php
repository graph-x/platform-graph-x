@extends('layouts.app')

@section('content')
    <supplier-details :data="{{ $supplier }}"></supplier-details>
@endsection
