@extends('layouts.app')

@section('content')
    <client-profile :data="{{ json_encode($client) }}"></client-profile>
@endsection
