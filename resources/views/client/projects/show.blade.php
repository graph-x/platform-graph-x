@extends('layouts.app')

@section('content')
    <client-project-details
        :project="{{ $project }}"
        :client="{{ $client }}"
        :statuses="{{ $statuses }}"
        :approved_submissions_count="{{ $approved_submissions_count }}"
        :invoice_count="{{ $invoice_count }}"
        :invoice_amount="{{ $invoice_amount }}"
        >
    </client-project-details>
@endsection
