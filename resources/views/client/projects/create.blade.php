@extends('layouts.app')

@section('content')
    <request-submissions :client="{{ json_encode($client) }}"></request-submissions>
@endsection
