@extends('layouts.app')

@section('content')
    <projects-list get-projects-route="/client/projects"></projects-list>
@endsection
