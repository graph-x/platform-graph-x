@extends('layouts.app')

@section('content')
    <project-details
        :project="{{ $project }}"
        :client="{{ $client }}"
        :statuses="{{ $statuses }}"
        :admin_approved_quotes_count="{{ $admin_approved_quotes_count }}"
        :invoice_count="{{ $invoice_count }}"
        :invoice_amount="{{ $invoice_amount }}"
    >
    </project-details>
@endsection
