@extends('layouts.app')

@section('content')
    <admin-profile :user="{{ json_encode($admin) }}"></admin-profile>
@endsection
