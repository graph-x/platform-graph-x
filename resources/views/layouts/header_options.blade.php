<div class="container-fluid">
    <section id="header_content" class="row">
        <div class="col-8 d-flex justify-content-start align-items-center">
            <div class="header_content_option d-inline-flex justify-content-start align-items-center">
                <i class="las la-filter"></i>
                <p class="option_title">Afficher :</p>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="header_option_afficher"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">A-Z<i
                            class="las la-angle-down"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="header_option_afficher">
                        <a class="dropdown-item" href="#">A-Z</a>
                        <a class="dropdown-item" href="#">Z-A</a>
                    </div>
                </div>
            </div> <!-- /.header_content_option -->
            <div class="header_content_option d-inline-flex justify-content-start align-items-center">
                <i class="las la-info-circle"></i>
                <p class="option_title">Statut :</p>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="header_option_statut"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tous<i
                            class="las la-angle-down"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="header_option_statut">
                        <a class="dropdown-item" href="#">En soumission</a>
                        <a class="dropdown-item" href="#">En attente</a>
                        <a class="dropdown-item" href="#">En approbation</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Tous</a>
                    </div>
                </div>
            </div> <!-- /.header_content_option -->
        @if (! \Request::is('clients*') && ! \Request::is('suppliers*'))
            <div class="header_content_option d-inline-flex justify-content-start align-items-center">
                <i class="las la-calendar"></i>
                <p class="option_title">Date :</p>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="header_option_statut"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Ce mois-ci<i
                            class="las la-angle-down"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="header_option_statut">
                        <a class="dropdown-item" href="#">Ce mois-ci</a>
                        <a class="dropdown-item" href="#">Mois précédent</a>
                        <a class="dropdown-item" href="#">Les trois derniers mois</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Tous</a>
                    </div>
                </div>
            </div> <!-- /.header_content_option -->
            <div class="header_content_option d-inline-flex justify-content-start align-items-center">
                <i class="las la-user-alt"></i>
                <p class="option_title">Client :</p>
                <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="header_option_statut"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tous<i
                            class="las la-angle-down"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="header_option_statut">
                        <a class="dropdown-item" href="#">Choix #1</a>
                        <a class="dropdown-item" href="#">Choix #2</a>
                        <a class="dropdown-item" href="#">Choix #3</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Tous</a>
                    </div>
                </div>
            </div> <!-- /.header_content_option -->
            @endif
        </div>
        <div class="col-4">
            <div id="wrap_header_content_search" class="input-group">
                <input id="header_content_search" type="text" class="form-control col-12"
                       placeholder="Recherche rapide ...">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button" id="header_content_search_button_addon">
                        <i class="las la-search"></i>
                    </button>
                </div>
            </div>
        </div>
    </section>
</div>
