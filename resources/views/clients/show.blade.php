@extends('layouts.app')

@section('content')
    <client-details :data="{{ $client }}"></client-details>
@endsection
