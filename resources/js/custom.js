// Calcul du nombre restant de jour pour la date de livraison d'un projet
// Ce qui change la couleur "left_edge" (e.g. [Page:Dashboard])
function get_left_edge_color(date_livraison) {
	var bg_left_edge = 'bg_green'; // by default

	var today = new Date();
	var day = today.getDate();
	var month = today.getMonth() + 1;

	var project_date = date_livraison;
	var split_project_date = project_date.split('-');
	var project_day = split_project_date[2];
	var project_month = split_project_date[1];

	if(project_month == month) {
		var remaining_days = project_day - day;
		if(remaining_days <= 1) {
			bg_left_edge = 'bg_red';
		} else if(remaining_days <= 2) {
			bg_left_edge = 'bg_orange';
		}
	}	

	return bg_left_edge;

}

$(document).ready(function() {

	// Recherche rapide [Page:Dashboard]
	$('#header_content_search').keyup(function(e) {
		var search_string = $(this).val().toLowerCase();
		$.each($('.project .nom_client'), function(i, val) {
			var nom_client = $(this).text().toLowerCase();
			if(nom_client.indexOf(search_string) >= 0) {
				$(this).parents('.project').show();
			} else {
				$(this).parents('.project').hide();
			}
		});
	});

	// HEADER LINK ACTIVE STATE 
	$(function() {
		var pathname = window.location.pathname;
		var split_pathname = pathname.split('/');

		if(split_pathname.length > 3) {
			// we are in a sub-page
			var base_pathname = pathname.split('/')[1];
			$('header a[href="/'+base_pathname+'/"]').addClass('active');
		} else {
			$('header a[href="'+pathname+'"]').addClass('active');
		}
	});

	// SINGLE PROJECT TABS
	$('#single_project_tabs .row').click(function(e) {
		let div_id = $(this).data('show');
		$(this).siblings().removeClass('active');
		$(this).addClass('active');
		$('#single_project_content > div').siblings().removeClass('active');
		$(div_id).addClass('active');
	});

	// SORTABLE PROJECTS
	var from_project_card_ID, to_project_card_ID;
	$('.project_card-content').sortable({
		connectWith: '.project_card-content',
		helper: 'clone',
		opacity: 0.6,
		cursor:'move',
		start: function(event, ui) {
			from_project_card_ID = ui.item.parents('.project_card-content').data('statut');
		},
		update: function(event, ui) {
			var favorite = ui.item.data('favorite');
			to_project_card_ID = ui.item.parents('.project_card-content').data('statut');
			if(favorite === 1) {
				// move project to the top of the list if the project is moving from another card
				if(from_project_card_ID !== to_project_card_ID) {
					ui.item.prependTo('.project_card-content[data-statut="'+to_project_card_ID+'"]');
				}
				// otherwise, use default sort
			}
		}
	}).disableSelection();

	// toggleclass "active" pour l'étoile [Page:Dashboard]
	$(document).on('click', '.favorite_project', function(e) {
		e.preventDefault();
		// Three things must change : icon, data-favorite and position of the HTML element.
		var star_icon = $(this).find('i');
		var actual_card_ID = $(this).parents('.project_card-content').data('statut');
		if(star_icon.hasClass('las')) { // class "lar" = empty star; class "las" = full star;
			// removing favorite
			star_icon.removeClass('las').addClass('lar');
			$(this).parents('.project').data('favorite', 0).attr('data-favorite', 0);
			// move card below favorites
			let last_favorite_project = $('.project_card-content[data-statut="'+actual_card_ID+'"] .project[data-favorite="1"]').last();
			$(this).parents('.project').insertAfter(last_favorite_project);
		} else {
			// adding favorite
			star_icon.removeClass('lar').addClass('las');
			$(this).parents('.project').data('favorite', 1).attr('data-favorite', 1);
			// move card at the top of the list
			$(this).parents('.project').prependTo('.project_card-content[data-statut="'+actual_card_ID+'"]');
		}
	});

	// CUSTOM HEADER MENU
	$('#header_more_options').click(function(e) {
		e.preventDefault();
		$('#header_options_menu').toggleClass('active');
	});

	// CUSTOM PROFILE MENU
	$('#header_profile_picture').click(function(e) {
		e.preventDefault();
		$('#header_profil_menu').toggleClass('active');
	});

	// Dans la page "DASHBOARD", lorsqu'on clique sur l'icône "oeil" d'un projet
	$(document).on('click', '.view_project', function(e) {
		e.preventDefault();

		// Firstly, insert main data
		var project_ID = $(this).parents('.project').data('id');
		// projets[project_ID].projet_lie = ID du projet lié
		var linked_project_title = (projets[project_ID].projet_lie) ? projets[projets[project_ID].projet_lie].nom_projet : '';
		$('#project_modal #nom_projet').text(projets[project_ID].nom_projet);
		$('#project_modal #client').text(projets[project_ID].nom_client);
		$('#project_modal #code_projet').text(projets[project_ID].code_projet);
		$('#project_modal #date_livraison').text(projets[project_ID].date_livraison);
		$('#project_modal #projet_lie').text(linked_project_title); 
		$('#project_modal #statut').text(projets[project_ID].statut_string);
		$('#project_modal #contact').text(projets[project_ID].contact);
		$('#project_modal #telephone').text(projets[project_ID].telephone);
		$('#project_modal #email').text(projets[project_ID].email);

		// Secondly, create every "carte d'affaire"
		// Quick reset
		$('#project_modal-list').html('');
		// Create
		$.each(projets[project_ID].cartes_affaires, function(i, carte) {
			$('#project_modal-list').append("<div class='row carte_affaire'><div class='col-10 d-flex justify-content-start align-items-center'><p>"+carte.title+"</p></div><div class='col-2 d-flex justify-content-end align-items-center'><i class='las la-chevron-circle-down'></i></div><div class='col-12 carte_affaire-content'><p>"+carte.text+"</p></div></div>");
		});

		// Then, show up modal
		$('#project_modal').addClass('active');
		$('body').addClass('no_scroll');
	});

	$(document).on('click', '#close_project_modal', function(e) {
		e.preventDefault();
		$('#project_modal').removeClass('active');
		$('body').removeClass('no_scroll');
	});
		
	// Lorsqu'on clique sur une "carte d'affaire"
	$(document).on('click', '.carte_affaire', function(e) {
		e.preventDefault();
		$(this).find('.carte_affaire-content').slideToggle(200);
	});

	// Display selected item text in bootstrap's dropdown
	$(document).on('click', '.dropdown-menu a:not(.disable_text)', function(e) {
		e.preventDefault();
		let text = $(this).text();
		let icon = '<i class="las la-angle-down"></i>';
    $(this).parent().siblings('button').html(text+icon);
    $(this).parent().siblings('button').val(text);
	});

});