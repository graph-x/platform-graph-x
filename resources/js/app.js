import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import Toasted from 'vue-toasted';
import Multiselect from 'vue-multiselect';
import 'vue-multiselect/dist/vue-multiselect.min.css';
import VueNumeric from 'vue-numeric'

require('./bootstrap');
window.Vue = require('vue');

Vue.prototype.current_user = window.current_user; // ...authenticated user

Vue.use(VueNumeric);
Vue.use(VueSweetalert2);
Vue.use(Toasted, {
    theme: 'toasted-primary',
    iconPack: 'custom-class',
    position: 'bottom-left',
    duration: 5000,
    action: {
        text: 'Close',
        onClick: (e, toastObject) => {
            toastObject.goAway(0);
        },
    }
});

// Layouts
Vue.component('navigation-component', require('./components/Layouts/NavigationComponent.vue').default);
Vue.component('sidebar-container', require('./components/Sidebar/SidebarContainer.vue').default);

// Admin interface
Vue.component('dashboard-component', require('./components/admin-interface/Dashboard/DashboardComponent.vue').default);
Vue.component('projects-list', require('./components/admin-interface/Projects/ProjectsList.vue').default);
Vue.component('project-details', require('./components/admin-interface/Projects/ProjectDetails.vue').default);
Vue.component('clients-list', require('./components/admin-interface/Clients/ClientsList.vue').default);
Vue.component('client-details', require('./components/admin-interface/Clients/ClientDetails.vue').default);
Vue.component('suppliers-list', require('./components/admin-interface/Suppliers/SuppliersList.vue').default);
Vue.component('supplier-details', require('./components/admin-interface/Suppliers/SupplierDetails.vue').default);
Vue.component('admin-profile', require('./components/admin-interface/Profile/AdminProfile.vue').default);
Vue.component('users-list', require('./components/admin-interface/Users/UsersList.vue').default);
Vue.component('user-details', require('./components/admin-interface/Users/UserDetails.vue').default);

// Multiselect
Vue.component('multiselect', Multiselect);

// Header Component - Filters and Quick Search
Vue.component('header-component', require('./components/HeaderFilters/HeaderComponent.vue').default);

// Supplier interface
Vue.component('supplier-project-details', require('./components/supplier-interface/Projects/SupplierProjectDetails.vue').default);
Vue.component('supplier-profile', require('./components/supplier-interface/Profile/SupplierProfile.vue').default);

// Client interface
Vue.component('client-project-details', require('./components/client-interface/Projects/ClientProjectDetails.vue').default);
Vue.component('request-submissions', require('./components/client-interface/Projects/RequestSubmissions/RequestSubmissions.vue').default);
Vue.component('client-profile', require('./components/client-interface/Profile/ClientProfile.vue').default);

const app = new Vue({
    el: '#app',
});
