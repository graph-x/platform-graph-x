<?php

namespace Graphx\CustomSelect;

use Laravel\Nova\Fields\Field;

class CustomSelect extends Field
{
    /**
     * Indicates if the element should be shown on the index view.
     *
     * @var \Closure|bool
     */
    public $showOnIndex = false;

    /**
     * Indicates if the element should be shown on the detail view.
     *
     * @var \Closure|bool
     */
    public $showOnDetail = false;
    
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'custom-select';

    /**
     * The field's default create attribute button title.
     *
     * @var string
     */
    public $title = 'Create new';

    /**
     * The field's default create attribute path.
     *
     * @var string
     */
    public $path = null;

    /**
     * The field's default tab to redirect to.
     *
     * @var string
     */
    public $redirectToTab = null;

    /**
     * The field's default redirect from attribute.
     *
     * @var string
     */
    public $redirectFrom = 'Projects';

    /**
     * The path the field should make an ajax request to. 
     *
     * @var string
     */
    public function get($endpoint)
    {
        $this->withMeta(['endpoint' => $endpoint]);

        return $this;
    }

    /**
     * The parent attribute of the field.
     *
     * @var string
     */
    public function parent($attribute)
    {
        $this->withMeta(['parent_attribute' => $attribute]);

        return $this;
    }

    /**
     * Set the options for creating new attribute that may be selected by the field.
     *
     * @param  array|null $options
     * @return $this
     */
    public function withButton(array $options = null)
    {
        return $this->withMeta(['button' =>
            [
                'title' => $options['title'] ?? __($this->title),
                'path' => $options['path'] ?? $this->path,
                'redirectToTab' => $options['redirectToTab'] ?? $this->redirectToTab,
                'redirectFrom' => $options['redirectFrom'] ?? $this->redirectFrom
            ]
        ]);
    }
}
