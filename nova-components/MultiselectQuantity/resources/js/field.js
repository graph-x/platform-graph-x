Nova.booting((Vue, router, store) => {
  Vue.component('index-multiselect-quantity', require('./components/IndexField'))
  Vue.component('detail-multiselect-quantity', require('./components/DetailField'))
  Vue.component('form-multiselect-quantity', require('./components/FormField'))
})
