<?php

namespace GraphX\MultiselectQuantity;

use Laravel\Nova\Fields\Field;
use Laravel\Nova\Http\Requests\NovaRequest;
use Illuminate\Support\Facades\Log;

class MultiselectQuantity extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'multiselect-quantity';
}
