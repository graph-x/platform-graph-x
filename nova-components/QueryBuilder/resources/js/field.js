Nova.booting((Vue, router, store) => {
  Vue.component('index-query-builder', require('./components/IndexField'))
  Vue.component('detail-query-builder', require('./components/DetailField'))
  Vue.component('form-query-builder', require('./components/FormField'))
})
