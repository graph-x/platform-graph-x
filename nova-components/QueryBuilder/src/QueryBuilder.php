<?php

namespace Graphx\QueryBuilder;

use Laravel\Nova\Fields\Field;

class QueryBuilder extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'query-builder';

    /**
     * Hide field on index.
     *
     * @var bool
     */
    public $showOnIndex = false;

    /**
     * Hide field on detail.
     *
     * @var bool
     */
    public $showOnDetail = false;

    public function __construct($name, $attribute = null, callable $resolveCallback = null)
    {
        parent::__construct($name, $attribute, $resolveCallback);

        $this->withLabels([
            'addRule' => __('Add Rule'),
            'removeRule' => __('Remove Rule'),
            'addGroup' => __('Add Group'),
            'removeGroup' => __('Remove Group'),
            'textInputPlaceholder' => __('Value'),
            'matchType'  => __('Match Type'),
            'matchTypes' => [
                [ 'id' => 'all', 'label' => __('All') ],
                [ 'id' => 'any', 'label' => __('Any') ],
            ],
        ]);
    }

    /**
     *
     * @param array $labels
     * @return QueryBuilder
     */
    public function withLabels(array $labels)
    {
        return $this->withMeta(['labels' => $labels]);
    }

    /**
     * Set the query builder rules.
     *
     * @param array $rules
     * @return QueryBuilder
     */
    public  function withBuilderRules(array $rules)
    {
       return $this->withMeta(['rules' => $rules]);
    }

    /**
     * Set the query builder options.
     *
     * @param array $options
     * @return QueryBuilder
     */
    public function withBuilderOptions(array $options)
    {
        return $this->withMeta(['options' => $options]);
    }
}
