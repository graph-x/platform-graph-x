Nova.booting((Vue, router, store) => {
  Vue.component('index-multiselect-supplier', require('./components/IndexField'))
  Vue.component('detail-multiselect-supplier', require('./components/DetailField'))
  Vue.component('form-multiselect-supplier', require('./components/FormField'))
})
