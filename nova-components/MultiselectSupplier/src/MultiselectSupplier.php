<?php

namespace GraphX\MultiselectSupplier;

use Laravel\Nova\Fields\Field;

class MultiselectSupplier extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'multiselect-supplier';

    /**
     * The field's default create supplier button title.
     *
     * @var string
     */
    public $defaultCreateSupplierTitle = 'Create new supplier';

    /**
     * The field's default create supplier path.
     *
     * @var string
     */
    public $defaultCreateSupplierPath = '/resources/suppliers';

    /**
     * Set the options for creating new supplier that may be selected by the field.
     *
     * @param  array|null $options
     * @return $this
     */
    public function withCreateSupplierButton(array $options = null)
    {
        return $this->withMeta(['createSupplierButton' =>
            [
                'title' => $options['title'] ?? __($this->defaultCreateSupplierTitle),
                'path'  => $options['path'] ?? $this->defaultCreateSupplierPath
            ]
        ]);
    }
}
