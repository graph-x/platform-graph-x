<?php

namespace Graphx\CustomViews;

use Laravel\Nova\Nova;
use Laravel\Nova\Events\ServingNova;
use Illuminate\Support\Str;
use Illuminate\Support\ServiceProvider;

class CustomViewsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Nova::serving(function (ServingNova $event) {
            $this->provideToScript($this->attachCustomViews('client'));
            Nova::script('custom-views', __DIR__.'/../dist/js/views.js');
            Nova::style('custom-views', __DIR__.'/../dist/css/views.css');
        });
    }

    private function attachCustomViews ($resource) {
        $resourceName = Str::plural(Str::snake($resource, '-'));
        return [$resourceName => json_decode('{"index":{"route":"index","component":"Index","name":"client-index-view"},"lens":{"route":"lens","component":"Lens","name":"client-lens-view"},"detail":{"route":"detail","component":"Detail","name":"client-detail-view"},"create":{"route":"create","component":"Create","name":"client-create-view"},"edit":{"route":"edit","component":"Update","name":"client-edit-view"},"edit-attached":{"route":"edit-attached","component":"UpdateAttached","name":"client-edit-attached-view"},"attach":{"route":"attach","component":"Attach","name":"client-attach-view"}}', true)];
    }

    private function provideToScript($variables) {
        if (isset(Nova::$jsonVariables['novaCustomViews'])) {
            $variables = array_merge_recursive(Nova::$jsonVariables['novaCustomViews'], $variables);
        }
        Nova::provideToScript(['novaCustomViews' => $variables]);
    }
}
