let mix = require('laravel-mix')

mix.webpackConfig({
    resolve: {
        alias: {
            '@nova': path.resolve(__dirname, '../../nova/resources/js/')
        }
    }
});

mix
    .setPublicPath('dist')
    .setResourceRoot('../')
    .js('resources/js/views.js', 'js')
    .sass('resources/sass/views.scss', 'css')
