import routes from './routes';
import ClientsIndex from './views/clients/Index';
import OptionsIndex from './views/options/Index';
import ProcessesIndex from './views/processes/Index';
import ProjectsDetail from './views/projects/Detail';
import ProjectsIndex from './views/projects/Index';
import RulesIndex from './views/rules/Index';
import SuppliersIndex from './views/suppliers/Index';
import Plugin from './plugin';

import iFrameResize from 'iframe-resizer/js/iframeResizer';
import axios from 'axios';

Nova.booting((Vue, router) => {
    Vue.use(Plugin);

    let customViews = {
        'clients': {
            'index': ClientsIndex,
        },
        'options': {
            'index': OptionsIndex,
        },
        'processes': {
            'index': ProcessesIndex,
        },
        'projects': {
            'index': ProjectsIndex,
            'detail': ProjectsDetail,
        },
        'rules': {
            'index': RulesIndex,
        },
        'suppliers': {
            'index': SuppliersIndex,
        }
    };

    router.beforeEach((to, from, next) => {
        let name = to.name;
        let resource = to.params.resourceName;

        if ('nonav' in from.query) {
            to.query.nonav = from.query.nonav;

            if ('index' === to.name || 'custom-index' === to.name) {
                next({
                    name: 'create',
                    params: {resourceName: to.params.resourceName},
                    query: to.query,
                });

                return;
            }
        }

        if (resource in customViews && name in customViews[resource]) {
            next({
                name: `custom-${name}`,
                params: Object.assign({}, to.params, { component: customViews[resource][name] }),
                query: to.query
            });
        } else {
            next();
        }
    });

    router.addRoutes(routes);

    // Bind the resize directive.
    Vue.directive('resize', {
        bind: function (el) {
            el.addEventListener('load', () => {
                if (!el.hasAttribute('resize')) {
                    el.setAttribute('resize', 'true');
                    iFrameResize({
                        log: false,
                        onMessage: (event) => {
                            if ('type' in event.message) {
                                if ('created' === event.message.type || 'updated' === event.message.type || 'deleted' === event.message.type) {
                                    Nova.$emit('custom-views:event', event.message);
                                }
                            }
                        }
                    }, el);
                }
            })
        }
    });

    //  Fire custom-views:event events to the parent window if applicable.
    Nova.$on('custom-views:event', event => {
        if ('parentIFrame' in window) {
            window.parentIFrame.sendMessage(event, '*');
        }
    });

    // Intercept resource requests and fire events to listeners.
    Nova.request().interceptors.response.use(response => {
        if (200 === response.status || 201 === response.status) {
            const createExpr = /&editMode=create$/i;
            const updateExpr = /\/nova-api\/([^\/]+)\/(\d+)\?viaResource=([^&]*)&viaResourceId=([^&]*)&viaRelationship=([^&]*)&editing=true&editMode=update$/i;
            const deleteExpr = /\/nova-api\/([^\?]+)\?search=([^&]*)&trashed=([^&]*)&viaResource=([^&]*)&viaResourceId=([^&]*)&viaRelationship=([^&]*)&resources\[\]=(\d+)$/i;

    	    if (response.request.responseURL.match(createExpr)) {
                Nova.$emit('custom-views:event', {type: 'created', resource: response.data.resource});
	        }

            if (response.request.responseURL.match(updateExpr)) {
                Nova.$emit('custom-views:event', {type: 'updated', resource: response.data.resource});
            }

            if (response.request.responseURL.match(deleteExpr)) {
                Nova.$emit('custom-views:event', {type: 'deleted'});
            }
        }

        return response;
    });
});

