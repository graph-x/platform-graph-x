import Vuex from "vuex";

export default new Vuex.Store({
    state: {
        timestamp: -1,
    },
    mutations: {
        setTimestamp(state) {
            state.timestamp = new Date().getTime();
        }
    }
});

