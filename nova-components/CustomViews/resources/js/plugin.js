export default {
    install(Vue, options) {
        Vue.prototype.$attributeValue = function (resource, attribute) {
            let field = resource.fields.filter(i => attribute === i.attribute);
            return field.length > 0 ? field[0].value : '';
        }
    }
}
