Nova.booting((Vue, router, store) => {
  Vue.component('index-custom-options', require('./components/IndexField'))
  Vue.component('detail-custom-options', require('./components/DetailField'))
  Vue.component('form-custom-options', require('./components/FormField'))
})
