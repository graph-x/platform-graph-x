<?php

namespace Graphx\CustomOptions;

use Laravel\Nova\Fields\Field;

class CustomOptions extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'custom-options';
}
