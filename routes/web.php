<?php

use App\Http\Controllers\Admin\AcceptSubmissionController;
use App\Http\Controllers\Client\ClientProjectItemController;
use App\Http\Controllers\Admin\ApiProjectSubmissionController;
use App\Http\Controllers\Admin\DashboardController as AdminDashboardController;
use App\Http\Controllers\Admin\DeclineSubmissionController;
use App\Http\Controllers\Api\Admin\ClientAddressController;
use App\Http\Controllers\Api\Admin\ClientContactsController;
use App\Http\Controllers\Api\Admin\ClientController as AdminClientController;
use App\Http\Controllers\Api\Admin\ClientDivisionController;
use App\Http\Controllers\Api\Admin\GlobalSearchController;
use App\Http\Controllers\Api\Admin\ProcessController;
use App\Http\Controllers\Api\Admin\ProjectFavoriteController;
use App\Http\Controllers\Api\Admin\SupplierContactsController;
use App\Http\Controllers\Api\Admin\SupplierController as AdminSupplierController;
use App\Http\Controllers\Api\Admin\UserController;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\ApiProjectController;
use App\Http\Controllers\Api\ApiProjectItemController;
use App\Http\Controllers\Api\ApprovedItemController;
use App\Http\Controllers\Api\ApproveFileController;
use App\Http\Controllers\Api\ClientAddressTypeRoleController;
use App\Http\Controllers\Api\DashboardProjectsController;
use App\Http\Controllers\Api\DivisionContactRoleController;
use App\Http\Controllers\Api\OptionController;
use App\Http\Controllers\Api\ProjectItemMessagesController;
use App\Http\Controllers\Api\ProjectStatusController;
use App\Http\Controllers\Api\RejectFileController;
use App\Http\Controllers\Api\StatusController;
use App\Http\Controllers\Client\ClientProfileController;
use App\Http\Controllers\Client\ProjectsController as ClientProjectController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProjectItemFileController;
use App\Http\Controllers\ProjectsClientController;
use App\Http\Controllers\ProjectInvoiceController;
use App\Http\Controllers\Supplier\ProjectsController as SupplierProjectController;
use App\Http\Controllers\Supplier\SupplierProjectItemsController;
use App\Http\Controllers\Supplier\SupplierProjectItemsQuotesController;
use App\Http\Controllers\Supplier\SupplierProfileController;
use App\Http\Controllers\Client\ProjectSubmissionController;
use App\Http\Controllers\Supplier\SupplierApprovedItemController;
use App\Http\Controllers\Admin\AdminProfileController;
use App\Http\Controllers\Admin\ProjectInvoiceNotifyClientController;
use App\Http\Controllers\InvoicePdfController;
use App\Http\Controllers\Api\ClientContactRoleController;
use App\Http\Controllers\Api\AdminAcceptEstimateController;
use App\Http\Controllers\Api\AdminDeclineEstimateController;
use App\Http\Controllers\ApprovedQuoteFileController;
use App\Http\Controllers\Admin\AdminSoumissionController;
use App\Http\Controllers\Admin\ProjectPreInvoiceNotifyClientController;
use App\Http\Controllers\Client\ClientSubmissionTabController;
use App\Http\Controllers\Client\ProjectSuppliersTabController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::redirect('/register', 404);
Route::redirect('/home', '/');


Route::middleware('auth')->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('/dashboard', [AdminDashboardController::class, 'index'])->name('admin.dashboard');

    Route::get('/api/statuses', [StatusController::class, 'index']);

    //Admin only.
    Route::middleware('can:oversee_everything')->group(function () {
        // Global search
        Route::get('/api/search', [GlobalSearchController::class, 'index']);

        // Dashboard
        Route::get('/api/dashboard/projects', [DashboardProjectsController::class, 'index']);
        Route::patch('/api/dashboard/projects/{project}/status', [DashboardProjectsController::class, 'update']);
        Route::post('/api/dashboard/projects/{project}/favorite', [ProjectFavoriteController::class, 'store']);

        // Projects (Admin views)
        Route::get('/projects', function () {
            return view('projects.index');
        })->name('projects.index');

        Route::get('/projects/{project}', [ProjectsClientController::class, 'show'])->name('projects-client.show');

        Route::post('/api/projects/{project}/invoice/{invoice}/update', [ProjectInvoiceController::class, 'update'])->name('project.invoice.show');

        Route::post('/api/projects/{project}/pre-invoice/{invoice}/notify-client', [ProjectPreInvoiceNotifyClientController::class, 'store']);
        Route::post('/api/projects/{project}/invoice/{invoice}/notify-client', [ProjectInvoiceNotifyClientController::class, 'store'])->name('project.invoice.notify-client');

        Route::get('/api/projects', [ApiProjectController::class, 'index']);
        Route::post('/api/projects', [ApiProjectController::class, 'store']);
        Route::delete('/api/projects/{project}/delete', [ApiProjectController::class, 'destroy']);
        Route::patch('/api/projects/{project}/update-status', [ProjectStatusController::class, 'update']);

        Route::get('/admin/profile', [AdminProfileController::class, 'show'])->name('admin.profile.show');
        Route::patch('/admin/profile/update', [AdminProfileController::class, 'update'])->name('admin.profile.update');
    });

    Route::get('/api/projects/{project}/invoice', [ProjectInvoiceController::class, 'show'])->name('project.invoice.show');

    Route::get('/invoices/{unique_code}', [InvoicePdfController::class, 'show'])->name('invoice.pdf.show');

    Route::get('/api/projects/{project}/submissions', [ApiProjectSubmissionController::class, 'index']);
    Route::patch('/api/projects/{project}/update', [ApiProjectController::class, 'update']);

    // Quote submissions - fournisseurs tab on admin views / submissions tab on supplier
    Route::post(
        '/api/projects/{project}/suppliers/{supplier}/items/{item}/decline-submission',
        [DeclineSubmissionController::class, 'store']
    );

    Route::post(
        '/api/projects/{project}/suppliers/{supplier}/items/{item}/accept-submission',
        [AcceptSubmissionController::class, 'store']
    );

    // New stuff
    Route::post('/api/projects/{project}/quotes/{quote}/accept-submission', [AdminAcceptEstimateController::class, 'store']);
    Route::post('/api/projects/{project}/quotes/{quote}/decline-submission', [AdminDeclineEstimateController::class, 'store']);

    // Division contact roles (For creating new project)
    Route::get('/api/divisions/{division}/contacts/{role}', [DivisionContactRoleController::class, 'index']);
    Route::get('/api/clients/{client}/addresses/{type}', [ClientAddressTypeRoleController::class, 'index']);
    Route::get('/api/clients/{client}/contacts/{role}', [ClientContactRoleController::class, 'index']);

    //  Project items (Admin view)
    Route::get('/api/projects/{project}/items', [ApiProjectItemController::class, 'index'])->name('project-items.index');
    Route::post('/api/projects/{project}/items', [ApiProjectItemController::class, 'store'])->name('project-items.store');

    // Clients (Admin views)
    Route::get('/clients', function () {
        return view('clients.index');
    })->name('clients.index');

    Route::get('/api/clients', [AdminClientController::class, 'index']);
    Route::get('/api/clients/{client}', [AdminClientController::class, 'show']);
    Route::post('/api/clients', [AdminClientController::class, 'store']);
    Route::patch('/api/clients/{client}/update', [AdminClientController::class, 'update']);
    Route::get('/clients/{client}', [AdminClientController::class, 'show'])->name('clients.show');
    Route::delete('/api/clients/{client}/delete', [AdminClientController::class, 'destroy']);

    // Tab - Clients divisions (admin views)
    Route::get('/api/clients/{client}/divisions', [ClientDivisionController::class, 'index']);
    Route::post('/api/clients/{client}/divisions', [ClientDivisionController::class, 'store']);
    Route::patch('/api/clients/{client}/divisions/{division}', [ClientDivisionController::class, 'update']);
    Route::delete('/api/clients/{client}/divisions/{division}', [ClientDivisionController::class, 'destroy']);

    // Tab - Clients addresses (admin views)
    Route::get('/api/clients/{client}/addresses', [ClientAddressController::class, 'index']);
    Route::post('/api/clients/{client}/addresses', [ClientAddressController::class, 'store']);
    Route::patch('/api/clients/{client}/addresses/{address}/update', [ClientAddressController::class, 'update']);
    Route::delete('/api/clients/{client}/addresses/{address}', [ClientAddressController::class, 'destroy']);

    // Tab - Client contacts (admin views)
    Route::get('/api/clients/{client}/contacts', [ClientContactsController::class, 'index']);
    Route::post('/api/clients/{client}/contacts', [ClientContactsController::class, 'store']);
    Route::patch('/api/clients/{client}/contacts/{contact}/update', [ClientContactsController::class, 'update']);
    Route::delete('/api/clients/{client}/contacts/{contact}', [ClientContactsController::class, 'destroy']);

    // Users (Admin views)
    Route::get('/users', function () {
        return view('users.index');
    })->name('users.index');

    Route::get('/users/{user}', [UserController::class, 'show'])->name('users.show');
    Route::patch('/users/{user}/update', [UserController::class, 'update'])->name('users.update');
    Route::delete('/users/{user}/delete', [UserController::class, 'destroy'])->name('users.delete');


    // Suppliers (Admin views)
    Route::get('/suppliers', function () {
        return view('suppliers.index');
    })->name('suppliers.index');

    Route::get('/api/admin/suppliers', [AdminSupplierController::class, 'index']);
    Route::post('/api/suppliers', [AdminSupplierController::class, 'store']);
    Route::get('/suppliers/{supplier}', [AdminSupplierController::class, 'show'])->name('suppliers.show');
    Route::patch('/api/suppliers/{supplier}/update', [AdminSupplierController::class, 'update']);
    Route::delete('/api/admin/suppliers/{supplier}/delete', [AdminSupplierController::class, 'destroy']);

    // Tab - Supplier contacts (admin views)
    Route::get('/api/suppliers/{supplier}/contacts', [SupplierContactsController::class, 'index']);
    Route::post('/api/suppliers/{supplier}/contacts', [SupplierContactsController::class, 'store']);
    Route::patch('/api/suppliers/{supplier}/contacts/{contact}/update', [SupplierContactsController::class, 'update']);
    Route::delete('/api/suppliers/{supplier}/contacts/{contact}', [SupplierContactsController::class, 'destroy']);

    // Processes (Admin)
    Route::get('/api/processes', [ProcessController::class, 'index']);
    Route::post('/api/processes', [ProcessController::class, 'store']);

    // Options (Admin)
    Route::get('/api/options', [OptionController::class, 'index']);
    Route::post('/api/options', [OptionController::class, 'store']);

    // New
    Route::get('/api/users', [UserController::class, 'index']);
    Route::get('/api/available-users', [UserController::class, 'availableUsersForEntities']);
    Route::post('/api/users', [UserController::class, 'store']);

    Route::get('/api/client/{client}/divisions', [ApiController::class, 'clientDivisions'])->name('clientDivisions');
    Route::get('/api/client/{client}/addresses/{type?}', [ApiController::class, 'clientAddresses'])->name('clientAddresses');
    Route::get('/api/division/{division}/contacts/{role?}', [ApiController::class, 'divisionContacts'])->name('divisionContacts');
    Route::get('/api/{project}/division-buyer', [ApiController::class, 'projectDivisionBuyer']);

    // Messages
    Route::post('/api/projects/{project}/suppliers/{supplier}/items/{item}/messages', [
        ProjectItemMessagesController::class, 'store',
    ]);
    Route::delete('/api/projects/{project}/suppliers/{supplier}/items/{item}/messages/{message}', [
        ProjectItemMessagesController::class, 'destroy',
    ]);
    Route::patch('/api/projects/{project}/suppliers/{supplier}/items/{item}/messages/{message}', [
        ProjectItemMessagesController::class, 'update',
    ]);

    Route::prefix('client')->middleware('can:create_projects')->group(function () {
        Route::get('projects', [ClientProjectController::class, 'index'])->name('client.projects.index');
        Route::get('projects/create', [ClientProjectController::class, 'create'])->name('client.projects.create');
        Route::post('{client}/projects', [ClientProjectController::class, 'store'])->name('client.projects.store');
        Route::get('projects/{project}', [ClientProjectController::class, 'show'])->name('client.projects.show');

        // Profile
        Route::get('/profile', [ClientProfileController::class, 'show'])->name('client.profile.show');
        Route::patch('{client}/profile/update', [ClientProfileController::class, 'update'])->name('client.profile.update');

        // Submissions
        Route::get('/api/projects/{project}/submissions', [ProjectSuppliersTabController::class, 'index']);
        Route::post('/api/projects/{project}/accept-submission', [ClientSubmissionTabController::class, 'store']);
        Route::post('/api/projects/{project}/decline-submission', [ClientSubmissionTabController::class, 'destroy']);

        Route::post('/{client}/projects/{project}/items', [ClientProjectItemController::class, 'store'])->name('client.project-items.store');
    });

    Route::prefix('supplier')->middleware('can:oversee_supplier_activities')->group(function () {
        Route::get('projects', [SupplierProjectController::class, 'index'])->name('supplier.projects.index');
        Route::get('projects/{project}', [SupplierProjectController::class, 'show'])->name('supplier.projects.show');
        Route::get('projects/{project}/items', [SupplierProjectItemsController::class, 'index'])->name('supplier.project.items.index');
        Route::post('projects/{project}/quotes', [SupplierProjectItemsQuotesController::class, 'store'])->name('supplier.project.quotes.store');

        Route::get('/profile', [SupplierProfileController::class, 'show'])->name('supplier.profile.show');
        Route::patch('{supplier}/profile/update', [SupplierProfileController::class, 'update'])->name('supplier.profile.update');
        Route::get('{supplier}/projects/{project}/epreuves', [SupplierApprovedItemController::class, 'index'])->name('approved.item.index');
    });

    // Epreuves - admin views
    Route::get('/api/projects/{project}/epreuves', [ApprovedItemController::class, 'index'])->name('approved.item.index');
    // Route::get('/api/projects/{project}/epreuves', [ApprovedItemController::class, 'index'])->name('approved.item.index');

    Route::post('/quotes/{quote}/files/upload', [ApprovedQuoteFileController::class, 'store'])->name('quote.file.store');
    Route::post('/projects/{project}/items/{item}/files/upload', [ProjectItemFileController::class, 'store'])->name('project.item.file.store');

    Route::get('/files/{hash_name}/download', [ProjectItemFileController::class, 'download'])->name('file.download');
    Route::post('/files/{file}/reject', [RejectFileController::class, 'store'])->name('file.cancel');
    Route::post('/files/{file}/approve', [ApproveFileController::class, 'store'])->name('file.approve');
});

# Nova API routes
Route::middleware(\Laravel\Nova\Http\Middleware\Authenticate::class)->group(function () {
    Route::get('/api/client/{client}/divisions', 'Api\ApiController@clientDivisions')->name('clientDivisions');
    Route::get('/api/client/{client}/addresses/{type?}', 'Api\ApiController@clientAddresses')->name('clientAddresses');
    Route::get('/api/division/{division}/contacts/{role?}', 'Api\ApiController@divisionContacts')->name('divisionContacts');

    Route::get('/api/suppliers', 'Api\SupplierController@index')->name('getSuppliers');
    // Route::get('/api/projects/{project}/assigned-items', 'Api\SupplierProjectsController@assignedItems')->name('assignedItems');
    // Route::post('/api/projects/{project}/quotes', 'Api\SupplierProjectsController@quotes')->name('storeQuotes');
});


// Route::get('/api/projects/{project}/approved-estimates', [AdminSoumissionController::class, 'index']);
Route::get('/api/projects/{project}/pre-invoice', [AdminSoumissionController::class, 'index']);
Route::post('/api/projects/{project}/approved-estimates/update', [AdminSoumissionController::class, 'update']);