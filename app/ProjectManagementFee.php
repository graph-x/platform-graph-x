<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectManagementFee extends Model
{
    const DEFAULT_FEE = 00.00;
    const DEFAULT_TITLE = 'Project management fee';
    const DEFAULT_DESCRIPTION = 'Cost for managing the project.';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
}
