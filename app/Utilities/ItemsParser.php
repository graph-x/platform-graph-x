<?php

namespace App\Utilities;

use App\Project;
use App\Supplier;
use App\SupplierContact;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotifySupplierContacts;

class ItemsParser
{
    /**
     * @var \App\Project
     */
    protected $project;

    /**
     * ItemsParser constructor.
     *
     * @param  \App\Project $project
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * Handle the saved project items.
     *
     * @param  \App\Project $project
     */
    public static function handle(Project $project)
    {
        return (new self($project))->parse();
    }

    /**
     * Parse the JSON items of the project and save them for respective assigned suppliers.
     */
    private function parse()
    {
        $assignedSuppliers = $this->assignedSuppliers();

        $this->handleLooseSuppliers($assignedSuppliers);

        $assignedSuppliers->each(function ($assignedSupplier) {
            $supplier = Supplier::find($assignedSupplier['id']);

            # Find the existing suppliers items for this project
            $supplierProject = $supplier->projects()->where('project_id', $this->project->id);
            $supplierItems = $supplierProject->exists() ? $supplierProject->first()->pivot->items : null;

            $supplierItems = collect(json_decode($supplierItems, true));
            $projectItems = collect(json_decode($this->project->items, true));

            # Map over project items using supplier items and a supplier.
            $parsed = $projectItems->map(function ($projectItem) use ($supplierItems, $supplier) {
                # Check if the current item is assigned to supplier
                $matches = collect($this->itemSuppliers($projectItem))->filter(function ($assigned) use ($supplier) {
                    return $assigned['id'] === $supplier['id'];
                })->first();

                # If there are matches - item is assigned to the current supplier proceed with other inspections
                if (isset($matches)) {
                    return $this->inspectItem($supplierItems, $projectItem, $supplier);
                }

                return false;
            });

            # Rejecting the items that are not assigned to supplier.
            $parsed = $parsed->reject(false)->values();

            # Assigning parsed items (which preserve the existing quotes).
            if ($supplierProject->exists()) {
                return $supplier->projects()->updateExistingPivot($this->project, ['items' => json_encode($parsed)]);
            }

            return $supplier->projects()->attach($this->project, ['items' => json_encode($parsed)]);
        });
    }

    /**
     * Get unique assigned suppliers for this project.
     */
    private function assignedSuppliers(): \Illuminate\Support\Collection
    {
        return collect(json_decode($this->project->items, true))->flatMap(function ($item) {
            return $this->itemSuppliers($item);
        })->unique();
    }

    /**
     * @param $item
     * @return mixed
     */
    private function itemSuppliers($item)
    {
        return json_decode($item['attributes']['suppliers'], true);
    }

    /**
     * Checking if this project is attached to suppliers that are not present in project items.
     *
     * @param  \Illuminate\Support\Collection $assignedSuppliers
     */
    private function handleLooseSuppliers(\Illuminate\Support\Collection $assignedSuppliers): void
    {
        $loose = $this->project->suppliers->reject(function ($supplier) use ($assignedSuppliers) {
            return $assignedSuppliers->pluck('id')->contains($supplier->id);
        });

        $loose->each(function ($supplier) {
            $supplier->projects()->detach($this->project);
        });
    }

    /**
     * @param  \Illuminate\Support\Collection $supplierItems
     * @param                                 $projectItem
     * @param  \App\Supplier                  $supplier
     * @return array
     */
    private function inspectItem(\Illuminate\Support\Collection $supplierItems,
                                $projectItem,
                                Supplier $supplier): array
    {
        # Retrieving the matching item.
        $found = $supplierItems->filter(function ($supplierItem) use ($projectItem) {
            return $supplierItem['key'] === $projectItem['key'];
        })->first();

        # If the item is found compare it with project item.
        if (isset($found)) {
            $oldItemAttributes = collect($found['attributes']);
            $projectItemAttributes = collect($projectItem['attributes']);
            $oldItemQuantities = collect(json_decode($oldItemAttributes['quantity'], true));
            $projectItemQuantities = collect(json_decode($projectItemAttributes['quantity'], true));

            # Preserving the suppliers submitted quotes if the item quantity is still present in the project.
            $quantities = $projectItemQuantities->map(function ($newQuantity) use ($oldItemQuantities) {
                return $oldItemQuantities->filter(function ($oldQuantity) use ($newQuantity) {
                        return $oldQuantity['code'] === $newQuantity['code'];
                    })->first() ?? $newQuantity;
            });

            Arr::forget($projectItem, 'attributes.suppliers');
            Arr::set($projectItem, 'attributes.quantity', json_encode($quantities));
            return $projectItem;
        }

        # There are no matches, meaning the item is new.
        $this->notifyEstimator($supplier);
        Arr::forget($projectItem, 'attributes.suppliers');
        return $projectItem;
    }

    /**
     * Notify supplier's contacts with estimator role.
     *
     * @param  \App\Supplier $supplier
     */
    private function notifyEstimator(Supplier $supplier): void
    {
        $supplier->contacts->filter(function ($contact) {
            return collect($contact->roles)->contains(SupplierContact::ESTIMATOR);
        })->each(function ($contact) {
            Mail::to($contact->email)->queue(new NotifySupplierContacts());
        });
    }
}
