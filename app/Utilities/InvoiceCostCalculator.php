<?php

namespace App\Utilities;

use App\Invoice;

class InvoiceCostCalculator
{
    public $invoice;
    public $line_items;
    public $project_management;

    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
        $this->line_items = $invoice->line_items;
        $this->project_management = $invoice->project_management_fee;
    }

    public function subtotal()
    {
        $approved_prices = $this->line_items->sum('approved_price');
        $admin_profits = $this->line_items->sum('admin_profit');
        $project_management_fee = $this->project_management['fee'];
        $subtotal = $approved_prices + $admin_profits + $project_management_fee;

        return $subtotal;
    }

    public function profit()
    {
        $admin_profits = $this->line_items->sum('admin_profit');
        $project_management_fee = $this->project_management['fee'];

        $profit = $admin_profits + $project_management_fee;

        return $profit;
    }

    public function tpsAmountFrom($amount)
    {
        return Invoice::TPS_TAX_RATE * $amount;
    }

    public function tvqAmountFrom($amount)
    {
        return Invoice::TVQ_TAX_RATE * $amount;
    }
}
