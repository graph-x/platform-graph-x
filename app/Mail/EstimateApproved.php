<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Supplier;
use App\Item;
use Illuminate\Support\Facades\Log;

class EstimateApproved extends Mailable
{
    use Queueable, SerializesModels;

    public $supplier;
    public $item;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Supplier $supplier, Item $item)
    {
        $this->supplier = $supplier;
        $this->item = $item;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.projects.estimate-approved', [
            'url' => config('app.url') . "/supplier/projects/{$this->item->project_id}"
        ])->subject(__('Estimate approved.'));
    }
}
