<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Project;
use App\Client;
use App\Invoice;

class InvoiceGenerated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var \App\Project
     */
    public $project;

    /**
     * @var \App\Invoice
     */
    public $invoice;

    /**
     * @var \App\Client
     */
    public $client;

    /**
     * Create a new message instance.
     *
     * @param  \App\Project $project
     * @param  \App\Invoice $invoice
     * @param  \App\Client  $client
     */
    public function __construct(Project $project, Invoice $invoice, Client $client)
    {
        $this->project = $project;
        $this->invoice = $invoice;
        $this->client = $client;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.projects.invoices.notify-client', [
            'url' =>  config('app.url') . "/invoices/{$this->invoice->unique_code}"
        ])->subject(__('Invoice for project.'));
    }
}
