<?php

namespace App\Mail;

use App\Item;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifySupplierContacts extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var \App\Item
     */
    public $item;

    /**
     * Create a new message instance.
     *
     * @param  \App\Item $item
     */
    public function __construct(Item $item)
    {
        $this->item = $item;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.projects.notify-suppliers')->subject('Request for estimation.');
    }
}
