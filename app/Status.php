<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    public const QUOTE_REQUESTS_TO_BE_MADE = 'quote_requests_to_be_made';
    public const WORK_IN_PRODUCTION = 'tests_approved_work_in_production';
    public const WORK_COMPLETED_BILLING = 'work_completed_billing';

    /**
     * Attributes that are not mass assignable.
     */
    protected $guarded = [];

    /**
     * Predefined statuses, based on the provided design.
     */
    public static $predefined = [
        [
            'key' => 'quote_requests_to_be_made',
            'html_label' => 'Demandes de soumission à faire',
            'short_label' => 'Demandes de soumission à faire',
            'color' => '#7EB2BF',
        ],
        [
            'key' => 'awaiting_supplier_prices',
            'html_label' => 'En attente des prix fournisseurs',
            'short_label' => 'En attente des prix fournisseurs',
            'color' => '#A37FBF',
        ],
        [
            'key' => 'submissions_to_prepare',
            'html_label' => 'Soumissions à préparer',
            'short_label' => 'Soumissions à préparer',
            'color' => '#BFAD7F',
        ],
        [
            'key' => 'submissions_sent_awaiting_client_approval',
            'html_label' => "Soumissions expédiées <br>En attente d'approbation client",
            'short_label' => "Soumissions expédiées",
            'color' => '#BE7E7E',
        ],
        [
            'key' => 'submissions_approved_waiting_for_the_tests',
            'html_label' => "Soumissions approuvées <br>Projet en cours <br>En attente des épreuves",
            'short_label' => "Soumissions approuvées",
            'color' => '#8BBF7D',
        ],
        [
            'key' => 'tests_sent_awaiting_client_approval',
            'html_label' => "Épreuves expédiées <br>En attente d'approbation client",
            'short_label' => "Épreuves expédiées",
            'color' => '#BFAD7F',
        ],
        [
            'key' => 'tests_approved_work_in_production',
            'html_label' => "Épreuves approuvées <br>Travail en production",
            'short_label' => "Épreuves approuvées",
            'color' => '#BE7E7E',
        ],
        [
            'key' => 'partially_completed_work',
            'html_label' => "Travail partiellement complété",
            'short_label' => "Travail partiellement complété",
            'color' => '#8BBF7D',
        ],
        [
            'key' => 'work_completed_billing',
            'html_label' => "Travail complété <br>À facturer",
            'short_label' => "Travail complété",
            'color' => '#8BBF7D',
        ],
    ];
}
