<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * Fillable attributes.
     *
     * @var string[]
     */
    protected $fillable = ['name'];

    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }
}
