<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Item extends Model
{
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'is_linked' => 'boolean',
    ];

    /**
     *  The project that the item belongs to.
     */
    public function project(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    /**
     * The quantities that belong to the item.
     */
    public function quantities(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Quantity::class, 'item_quantity');
    }

    /**
     * The suppliers that are assigned to the item.
     */
    public function suppliers(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Supplier::class, 'item_supplier')->withPivot('notified_at', 'quotes_approved_at', 'quotes_declined_at');
    }

    /**
     * Quotes submitted for the item.
     */
    public function quotes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Quote::class, 'item_id');
    }

    /**
     * Parent item.
     */
    public function parent_item(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Item::class, 'parent_item_id');
    }

    /**
     * Children of the parent item.
     */
    public function linked_items(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Item::class, 'parent_item_id');
    }

    public function has_linked_items(): bool
    {
        return $this->linked_items()->exists();
    }

    /**
     * Query scope for Items that belong to given user with supplier role.
     *
     * @param            $query
     * @param  \App\User $user
     * @return mixed
     */
    public function scopeBelongingToSupplier($query, User $user)
    {
        return $query->whereHas('suppliers', function ($q) use ($user) {
            return $q->where('user_id', $user->id);
        });
    }

    /**
     * The delivery address that is assigned to the item.
     */
    public function address(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Address::class, 'address_id');
    }

    /**
     * The delivery contact that is assigned to the item.
     */
    public function contact(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(ClientContact::class, 'delivery_contact_id');
    }

    /**
     * The delivery address that is assigned to the item.
     */
    public function getDeliveryAddressAttribute(): array
    {
        return [
            'value' => $this->address->id,
            'display' => $this->address->address_line_1 . (!empty($this->address->address_line_2) ? ', ' . $this->address->address_line_2 : '') . ', ' . $this->address->city . ', ' . $this->address->province . ', ' . $this->address->country . ', ' . $this->address->postal_code,
        ];
    }

    /**
     * The delivery contact that is assigned to the item.
     */
    public function getDeliveryContactAttribute(): array
    {
        return [
            'value' => $this->delivery_contact_id,
            'display' => $this->contact->name,
        ];
    }

    /**
     * Files that are uploaded for item.
     */
    public function files(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(File::class, 'item_id');
    }

    /**
     * Approved files for the item.
     */
    public function approvedFiles()
    {
        return $this->files()->whereNotNull('approved_at');
    }

    public function optionGroups(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(OptionGroup::class, 'item_id');
    }
}
