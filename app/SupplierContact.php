<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierContact extends Model
{
    /**
     *  Supplier's contact roles.
     */
    const ESTIMATOR = 'estimator';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'roles' => 'array',
    ];

    /**
     * The number of resources to show per page via relationships.
     *
     * @var int
     */
    public static $perPageViaRelationship = 100;

    /**
     * The supplier that has the contact.
     */
    public function supplier()
    {
        return $this->belongsTo('App\Supplier');
    }
}