<?php

namespace App\Traits;

trait Filterable
{
    /**
     * Apply the filters for the model.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \App\Filters\ProjectFilters $filters
     * @return void
     */
    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }
}
