<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Dniccum\PhoneNumber\PhoneNumber;
use Fourstacks\NovaCheckboxes\Checkboxes;

class ClientContact extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\ClientContact';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
        'phone',
        'mobile',
        'email',
        'roles'
    ];

    /**
     * Indicates if the resource should be displayed in the sidebar.
     *
     * @var bool
     */
    public static $displayInNavigation = false;

    /**
     * The number of resources to show per page via relationships.
     *
     * @var int
     */
    public static $perPageViaRelationship = 100;

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $divisions = $request->findParentModel()->divisions()->get();

        return [
            BelongsTo::make('Client')
                ->onlyOnDetail(),

            Text::make('Nom', 'name')
                ->sortable()
                ->rules('required', 'max:255'),

            PhoneNumber::make('Téléphone', 'phone')
                ->countries(['US', 'CA'])
                ->sortable()
                ->format('(###) ###-#### ext ####')
                ->disableValidation()
                ->rules('max:255'),

            PhoneNumber::make('Mobile', 'mobile')
                ->countries(['US', 'CA'])
                ->disableValidation()
                ->rules('max:255'),

            Text::make('Email', 'email')
                ->sortable()
                ->rules('required', 'max:255', 'email'),

            Checkboxes::make('Roles', 'roles')
                ->options([
                    'buyer' => 'Acheteur',
                    'pdf_approval' => 'Approbation PDF',
                    'physical_approval' => 'Approbation physique',
                    'accounting' => 'Comptabilité',
                    'invoicing' => 'Facturation',
                    'delivery' => 'Livraison'
                ]),

            Checkboxes::make('Divisions', 'divisions')
                ->options($divisions->pluck('name', 'id'))

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Contacts';
    }

    /**
     * Determine where to redirect after create.
     *
     * @return string
     */
    public static function redirectAfterCreate(NovaRequest $request, $resource)
    {
        return '/resources/' . Client::uriKey() . '/' . $resource->client_id . '?tab=Contacts';
    }

    /**
     * Determine where to redirect after create.
     *
     * @return string
     */
    public static function redirectAfterUpdate(NovaRequest $request, $resource)
    {
        return '/resources/' . Client::uriKey() . '/' . $resource->client_id . '?tab=Contacts';
    }
}
