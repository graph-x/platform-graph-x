<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\BelongsTo;
use Dniccum\PhoneNumber\PhoneNumber;
use Fourstacks\NovaCheckboxes\Checkboxes;

class SupplierContact extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\SupplierContact';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
        'name',
        'phone',
        'mobile',
        'email',
        'roles'
    ];

    /**
     * Indicates if the resource should be displayed in the sidebar.
     *
     * @var bool
     */
    public static $displayInNavigation = false;

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            BelongsTo::make('Supplier')
                ->onlyOnDetail(),

            Text::make('Nom', 'name')
                ->sortable()
                ->rules('required', 'max:255'),

            PhoneNumber::make('Téléphone', 'phone')
                ->countries(['US', 'CA'])
                ->sortable()
                ->format('(###) ###-#### ext ####')
                ->disableValidation()
                ->rules('max:255'),

            PhoneNumber::make('Mobile', 'mobile')
                ->countries(['US', 'CA'])
                ->disableValidation()
                ->rules('max:255'),

            Text::make('Email', 'email')
                ->sortable()
                ->rules('required', 'max:255', 'email'),

            Checkboxes::make('Roles', 'roles')
                ->options([
                    'estimator' => 'Estimateur',
                    'expedition' => 'Expédition',
                    'infographics' => 'Infographie',
                    'installer' => 'Installateur',
                    'owner' => 'Propriétaire',
                    'salesrep' => 'Représentant',
                ])
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Contacts';
    }

    /**
     * Determine where to redirect after create.
     *
     * @return string
     */
    public static function redirectAfterCreate(NovaRequest $request, $resource)
    {
        return '/resources/' . Supplier::uriKey() . '/' . $resource->supplier_id . '?tab=Contacts';
    }

    /**
     * Determine where to redirect after create.
     *
     * @return string
     */
    public static function redirectAfterUpdate(NovaRequest $request, $resource)
    {
        return '/resources/' . Supplier::uriKey() . '/' . $resource->supplier_id . '?tab=Contacts';
    }
}
