<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Country;
use Laravel\Nova\Fields\BelongsTo;
use Illuminate\Validation\Rule;
use EmilianoTisato\GoogleAutocomplete\AddressMetadata;
use EmilianoTisato\GoogleAutocomplete\GoogleAutocomplete;
use Fourstacks\NovaCheckboxes\Checkboxes;

class Address extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Address';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'address_line_1',
        'address_line_2',
        'city',
        'province',
        'country',
        'postal_code'
    ];

    /**
     * Indicates if the resource should be displayed in the sidebar.
     *
     * @var bool
     */
    public static $displayInNavigation = false;

    /**
     * The number of resources to show per page via relationships.
     *
     * @var int
     */
    public static $perPageViaRelationship = 100;

    /**
     * Get the value that should be displayed to represent the resource.
     *
     * @return string
     */
    public function title()
    {
        return $this->address_line_1 . ', ' . $this->city;
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            BelongsTo::make('Client')
                ->onlyOnDetail(),

            GoogleAutocomplete::make('Adresse', 'address_line_1')
                ->countries(['US', 'CA'])
                ->withValues(['locality', 'administrative_area_level_1', 'country', 'postal_code'])
                ->sortable()
                ->rules('required', 'max:255'),

            AddressMetadata::make('Ville', 'city')
                ->fromValue('locality')
                ->disabled()
                ->hideFromIndex(),

            AddressMetadata::make('Province', 'province')
                ->fromValue('administrative_area_level_1')                    
                ->disabled()
                ->hideFromIndex(),

            AddressMetadata::make('Pays', 'country')
                ->fromValue('country')
                ->disabled()
                ->hideFromIndex(),

            AddressMetadata::make('Code Postal', 'postal_code')
                ->fromValue('postal_code')
                ->disabled()
                ->hideFromIndex(),

            Checkboxes::make('Types', 'types')
                ->options([
                    'physical_approval' => 'Approbation physique',
                    'delivery' => 'Livraison'
                ])
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Adresses';
    }

    /**
     * Get the displayable singular label of the resource.
     *
     * @return string
     */
    public static function singularLabel()
    {
        return 'Adresse';
    }

    /**
     * Determine where to redirect after create.
     *
     * @return string
     */
    public static function redirectAfterCreate(NovaRequest $request, $resource)
    {
        return '/resources/' . Client::uriKey() . '/' . $resource->client_id . '?tab=Adresses';
    }

    /**
     * Determine where to redirect after create.
     *
     * @return string
     */
    public static function redirectAfterUpdate(NovaRequest $request, $resource)
    {
        return '/resources/' . Client::uriKey() . '/' . $resource->client_id . '?tab=Adresses';
    }
}
