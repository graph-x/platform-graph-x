<?php

namespace App\Nova\Flexible\Layouts;

use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Trix;
use App\Option as OptionModel;
use Graphx\CustomSelect\CustomSelect;
use Graphx\QueryBuilder\QueryBuilder;
use Whitecube\NovaFlexibleContent\Layouts\Layout;
use GraphX\MultiselectQuantity\MultiselectQuantity;
use GraphX\MultiselectSupplier\MultiselectSupplier;

class ItemLayout extends Layout
{
    /**
     * The layout's unique identifier
     *
     * @var string
     */
    protected $name = 'item';

    /**
     * The displayed title
     *
     * @var string
     */
    protected $title = 'Item';

    /**
     * The attributes to cast to types.
     *
     * @var string
     */
    protected $casts = [
        'delivery_deadline' => 'date',
    ];

    /**
     * Get the fields displayed by the layout.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Trix::make(__('Description')),

            QueryBuilder::make(__('Options'), 'options')
                ->withBuilderRules(OptionModel::toQueryBuilderRules(false))
                ->withLabels($this->builderLabels())
                ->withBuilderOptions($this->builderOptions())
                ->hideFromDetail(),

            MultiselectQuantity::make(__('Quantity'), 'quantity'),

            MultiselectSupplier::make(__('Suppliers'), 'suppliers')
                ->withCreateSupplierButton([
                    'title' => __('Create new supplier'),
                    'path' => '/resources/suppliers',
                ]),

            CustomSelect::make(__('Delivery'), 'delivery_contact_id')
                ->get('/api/division/{division_id}/contacts/delivery')
                ->parent('division_id')
                ->rules('required')
                ->withButton([
                    'title' => __('Create new'),
                    'path' => '/resources/clients',
                    'redirectToTab' => 'Contacts',
                ]),

            CustomSelect::make(__('Physical Address'), 'address_id')
                ->get('/api/client/{client}/addresses/delivery')
                ->parent('client')
                ->rules('required')
                ->withButton([
                    'title' => __('Create new'),
                    'path' => '/resources/clients',
                    'redirectToTab' => 'Adresses',
                ]),

            Date::make(__('Delivery deadline'), 'delivery_deadline')
                ->sortable()
                ->rules('required', 'max:255')
        ];
    }

    private function builderLabels()
    {
        return [
            'addRule' => __('Add'),
            'removeRule' => __('Remove'),
            'addGroup' => __('Add Group'),
            'removeGroup' => __('Remove Group'),
            'textInputPlaceholder' => __('Value'),
            'noteInputPlaceholder' => __('Insert notes?'),
            'matchType' => __('Match Type'),
            'matchTypes' => [
                ['id' => 'all', 'label' => __('All')],
                ['id' => 'any', 'label' => __('Any')],
            ],
        ];
    }

    private function builderOptions()
    {
        return [
            'enableMatchType' => false,
            'enableGroups' => false,
            'enableOperators' => false,
        ];
    }
}
