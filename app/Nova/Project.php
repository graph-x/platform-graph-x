<?php

namespace App\Nova;

use App\Option as OptionModel;
use Graphx\QueryBuilder\QueryBuilder;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Trix;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use NovaAjaxSelect\AjaxSelect;
use Str;
use Whitecube\NovaFlexibleContent\Flexible;
use GraphX\MultiselectQuantity\MultiselectQuantity;
use GraphX\MultiselectSupplier\MultiselectSupplier;
use Epartment\NovaDependencyContainer\NovaDependencyContainer;
use Epartment\NovaDependencyContainer\HasDependencies;
use App\Nova\Flexible\Layouts\ItemLayout;

class Project extends Resource
{
    use HasDependencies;

    /**
     * @inheritDoc
     */
    protected static $indexDefaultField = 'title';

    /**
     * @inheritDoc
     */
    protected static $indexDefaultOrder = 'asc';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Project';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Opérations';

    /**
     * Build an "index" query for the given resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function indexQuery(NovaRequest $request, $query)
    {
        if ($request->user()->isAdmin()) {
            return $query;
        }

        return $query->assignedToAuthSupplier();
    }

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @throws \Exception
     * @return array
     */
    public function fields(Request $request)
    {
        if ($request->user()->isAdmin()) {
            return $this->adminView($request);
        }

        if ($request->user()->isClient()) {
            return $this->clientView($request);
        }

        return $this->supplierView($request);
    }

    /**
     * Get the fields displayed by the resource for the admin.
     *
     * @param  \Illuminate\Http\Request $request
     * @throws \Exception
     * @return array
     */
    public function adminView(Request $request)
    {
        return [
            BelongsTo::make(__('Client'), 'client'),

            AjaxSelect::make(__('Division'), 'division_id')
                ->get('/api/client/{client}/divisions')
                ->parent('client')
                ->rules('required'),

            AjaxSelect::make(__('Buyer'), 'buyer')
                ->get('/api/division/{division_id}/contacts/buyer')
                ->parent('division_id')
                ->rules('required'),

            AjaxSelect::make(__('Accounting'), 'accounting')
                ->get('/api/division/{division_id}/contacts/accounting')
                ->parent('division_id')
                ->rules('required'),

            Boolean::make(__('Require PDF Approval'), 'require_pdf'),

            AjaxSelect::make(__('PDF Approval'), 'pdf')
                ->get('/api/division/{division_id}/contacts/pdf_approval')
                ->parent('division_id'),

            /*NovaDependencyContainer::make([
                AjaxSelect::make(__('PDF Approval'), 'pdf_approval')
                    ->get('/api/division/{division_id}/contacts')
                    ->parent('division_id')
                    ->rules('required'),
            ])->dependsOn('require_pdf', 1),*/

            Boolean::make(__('Require Physical Approval'), 'require_physical'),

            AjaxSelect::make(__('Physical Approval'), 'physical')
                ->get('/api/division/{division_id}/contacts/physical_approval')
                ->parent('division_id'),

            AjaxSelect::make(__('Physical Address'), 'address_id')
                ->get('/api/client/{client}/addresses/physical_approval')
                ->parent('client'),

            Date::make(__('Start'), 'start')
                ->sortable()
                ->rules('required', 'max:255'),

            Date::make(__('End'), 'end')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make(__('Title'), 'title')
                ->sortable()
                ->rules('required', 'max:255'),

            Select::make(__('Status'), 'status')->options([
                'estimates' => __('Waiting for estimates'),
                'client' => __('Waiting for client approval'),
                'pdf' => __('Waiting for PDF approval'),
                'physical' => __('Waiting for physical approval'),
                'production' => __('In production'),
                'delivered' => __('Delivered'),
                'cancelled' => __('Cancelled'),
                'paused' => __('Paused')
                ])
                ->rules('required'),

            Text::make(__('PO Number'), 'po_number')
                ->sortable(),

            Flexible::make(__('Items'))
                ->fullWidth()
                ->button(__('Add Item'))
                ->addLayout(ItemLayout::class)
                ->hideFromIndex()
                ->hideFromDetail(),

            BelongsToMany::make('Suppliers')
                ->fields(function () {
                    return [
                        Flexible::make(__('Items'))
                            ->button(__('Add Item'))
                            ->addLayout(__('Item'), 'item', [
                                Trix::make(__('Description')),

                                QueryBuilder::make(__('Options'), 'options')
                                    ->withBuilderRules(OptionModel::toQueryBuilderRules(false))
                                    ->withLabels($this->builderLabels())
                                    ->withBuilderOptions($this->builderOptions())
                                    ->hideFromDetail(),

                                Number::make(__('Quantity'), 'quantity')->min(0)->max(9999)->step(1),
                            ])
                    ];
                })->hideFromDetail(),
        ];
    }

    /**
     * Get the fields displayed by the resource for the client.
     *
     * @param  \Illuminate\Http\Request $request
     * @throws \Exception
     * @return array
     */
    public function clientView(Request $request)
    {
        return [
            BelongsTo::make(__('Client'), 'client'),

            AjaxSelect::make(__('Division'), 'division_id')
                ->get('/api/client/{client}/divisions')
                ->parent('client')
                ->rules('required'),

            AjaxSelect::make(__('Buyer'), 'buyer')
                ->get('/api/division/{division_id}/contacts')
                ->parent('division_id')
                ->rules('required'),

            AjaxSelect::make(__('Accounting'), 'accounting')
                ->get('/api/division/{division_id}/contacts')
                ->parent('division_id')
                ->rules('required'),

            AjaxSelect::make(__('Delivery'), 'delivery')
                ->get('/api/division/{division_id}/contacts')
                ->parent('division_id')
                ->rules('required'),

            Date::make(__('Start'), 'start')
                ->sortable()
                ->rules('required', 'max:255'),

            Date::make(__('End'), 'end')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make(__('Title'), 'title')
                ->sortable()
                ->rules('required', 'max:255'),

            Trix::make(__('Description'), 'description')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make(__('PO Number'), 'po_number')
                ->sortable(),

            Flexible::make(__('Items'))
                ->button(__('Add Item / Group'))
                ->addLayout(__('Item'), 'item', [
                    Trix::make(__('Description')),

                    QueryBuilder::make(__('Options'), 'options')
                        ->withBuilderRules(OptionModel::toQueryBuilderRules(false))
                        ->withLabels($this->builderLabels())
                        ->withBuilderOptions($this->builderOptions())
                        ->hideFromDetail(),

                    Number::make(__('Quantity'), 'quantity')->min(0)->max(9999)->step(1)
                ])
                ->hideFromIndex()
                ->hideFromDetail(),

            BelongsToMany::make('Suppliers')
                ->fields(function () {
                    return [
                        Flexible::make(__('Items'))
                            ->button(__('Add Item'))
                            ->addLayout(__('Item'), 'item', [
                                Trix::make(__('Description')),

                                QueryBuilder::make(__('Options'), 'options')
                                    ->withBuilderRules(OptionModel::toQueryBuilderRules(false))
                                    ->withLabels($this->builderLabels())
                                    ->withBuilderOptions($this->builderOptions())
                                    ->hideFromDetail(),

                                Number::make(__('Quantity'), 'quantity')->min(0)->max(9999)->step(1),
                            ])
                    ];
                })->hideFromDetail(),
        ];
    }

    /**
     * Get the fields displayed by the resource for supplier.
     *
     * @param  \Illuminate\Http\Request $request
     * @throws \Exception
     * @return array
     */
    public function supplierView(Request $request)
    {
        return [
            Date::make(__('Start'), 'start')
                ->sortable()
                ->rules('required', 'max:255'),

            Date::make(__('End'), 'end')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make(__('Title'), 'title')
                ->sortable()
                ->rules('required', 'max:255'),

            Trix::make(__('Description'), 'description')
                ->sortable()
                ->rules('required', 'max:255'),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Projets';
    }

    private function builderLabels()
    {
        return [
            'addRule' => __('Add'),
            'removeRule' => __('Remove'),
            'addGroup' => __('Add Group'),
            'removeGroup' => __('Remove Group'),
            'textInputPlaceholder' => __('Value'),
            'noteInputPlaceholder' => __('Insert notes?'),
            'matchType'  => __('Match Type'),
            'matchTypes' => [
                [ 'id' => 'all', 'label' => __('All') ],
                [ 'id' => 'any', 'label' => __('Any') ],
            ],
        ];
    }

    private function builderOptions()
    {
        return [
            'enableMatchType' => false,
            'enableGroups' => false,
            'enableOperators' => false,
        ];
    }
}
