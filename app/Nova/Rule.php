<?php

namespace App\Nova;

use App\Option as OptionModel;
use Graphx\QueryBuilder\QueryBuilder;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Str;
use Fourstacks\NovaCheckboxes\Checkboxes;
use App\Process;

class Rule extends Resource
{
    /**
     * @inheritDoc
     */
    protected static $indexDefaultField = 'title';

    /**
     * @inheritDoc
     */
    protected static $indexDefaultOrder = 'asc';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Rule';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Système';

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $processes = Process::all();

        return [
            Text::make(__('Title'), 'title')
                ->sortable()
                ->rules('required', 'max:255'),

            BelongsTo::make('Option'),

            QueryBuilder::make('Query')
                ->withBuilderRules(OptionModel::toQueryBuilderRules())
                ->hideFromDetail()
                ->rules('required'),

            Checkboxes::make(__('Processes'), 'processes')
                ->options($processes->pluck('name', 'id')),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Règles';
    }
}
