<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Http\Requests\NovaRequest;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Country;
use Dniccum\PhoneNumber\PhoneNumber;
use Eminiarts\Tabs\Tabs;
use Laravel\Nova\Fields\HasMany;
use Fourstacks\NovaCheckboxes\Checkboxes;
use EmilianoTisato\GoogleAutocomplete\AddressMetadata;
use EmilianoTisato\GoogleAutocomplete\GoogleAutocomplete;
use Eminiarts\Tabs\TabsOnEdit;
use App\Process;

class Supplier extends Resource
{
    use TabsOnEdit;

    /**
     * @inheritDoc
     */
    protected static $indexDefaultField = 'name';

    /**
     * @inheritDoc
     */
    protected static $indexDefaultOrder = 'asc';

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Supplier';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'name',
        'phone',
        'address_line_1',
        'address_line_2',
        'city',
        'province',
        'country',
        'postal_code'
    ];

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Opérations';

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        $processes = Process::all();

        return [
            (new Tabs('Détails Fournisseur', [
                'Détails' => [
                    BelongsTo::make('User')->nullable()->hideFromIndex(),

                    Text::make('Nom', 'name')
                        ->sortable()
                        ->rules('required', 'max:255'),

                    PhoneNumber::make('Téléphone', 'phone')
                        ->countries(['US', 'CA'])
                        ->sortable()
                        ->rules('required', 'max:255'),

                    GoogleAutocomplete::make('Adresse', 'address_line_1')
                        ->countries(['US', 'CA'])
                        ->withValues(['locality', 'administrative_area_level_1', 'country', 'postal_code'])
                        ->sortable()
                        ->rules('required', 'max:255'),
        
                    AddressMetadata::make('Ville', 'city')
                        ->fromValue('locality')
                        ->disabled()
                        ->hideFromIndex(),
        
                    AddressMetadata::make('Province', 'province')
                        ->fromValue('administrative_area_level_1')                    
                        ->disabled()
                        ->hideFromIndex(),
        
                    AddressMetadata::make('Pays', 'country')
                        ->fromValue('country')
                        ->disabled()
                        ->hideFromIndex(),
        
                    AddressMetadata::make('Code Postal', 'postal_code')
                        ->fromValue('postal_code')
                        ->disabled()
                        ->hideFromIndex(),

                    Checkboxes::make(__('Processes'), 'processes')
                        ->options($processes->pluck('name', 'id')),

                ],
                'Contacts' => [
                    HasMany::make('Contacts', 'contacts', 'App\Nova\SupplierContact')
                ]
            ]))->withToolbar()
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }


    /**
     * Get the displayable label of the resource.
     *
     * @return string
     */
    public static function label()
    {
        return 'Fournisseurs';
    }
}
