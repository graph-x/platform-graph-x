<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Process;

class Rule extends Model
{
    protected $dispatchesEvents = [
        'saved' => \App\Events\RuleSaved::class,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'query' => 'array',
    ];

    /**
     * Accessor to retrieve activities.
     */
    public function getProcessesAttribute($value) {
        return $this->processes()->pluck('process_id')->toArray();
    }

    /**
     * Mutator to link activities.
     */
    public function setProcessesAttribute($value) {
        /**
        * We just assign the value to the object so it can be processed by the event listener
        * Using the mutator is required here as otherwise an array to string conversion error is thrown
        */
        $this->processes = $value;
    }

    /**
     * The activities that belongs to the supplier.
     */
    public function processes()
    {
        return $this->belongsToMany('App\Process');
    }

    public function option()
    {
        return $this->belongsTo(Option::class);
    }
}
