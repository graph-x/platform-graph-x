<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Observers\ItemObserver;
use App\Item;
use App\Observers\OptionGroupObserver;
use App\OptionGroup;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        \App\Events\SupplierSaved::class => [
            \App\Listeners\SupplierSaved::class,
        ],
        \App\Events\ClientContactSaved::class => [
            \App\Listeners\ClientContactSaved::class,
        ],
        \App\Events\RuleSaved::class => [
            \App\Listeners\RuleSaved::class,
        ],
        \App\Events\ProjectSaved::class => [
            \App\Listeners\ProjectSaved::class,
        ],

        'App\Events\RequireEstimates' => [
            'App\Listeners\SendRequireEstimatesNotification',
        ],

        'App\Events\ProjectUpdated' => [
            'App\Listeners\HandleProjectUpdated',
        ],

        'App\Events\ItemUpdated' => [
            'App\Listeners\HandleItemUpdated',
        ],

        'App\Events\ItemCreated' => [
            'App\Listeners\HandleItemCreated',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
