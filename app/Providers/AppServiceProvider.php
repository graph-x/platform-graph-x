<?php

namespace App\Providers;

use App\Client;
use App\Observers\ProjectObserver;
use App\Project;
use App\Supplier;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Project::observe(ProjectObserver::class);

        \View::composer('*', function($view) {
            $user = null;

            if (Auth::check()) {
                $connected_entity = null;
                $url = null;
                $type = 'Admin';
                $user = Auth::user();

                if ($user->role === 'client') {
                    $client = Client::where('user_id', $user->id);

                    if ($client->exists()) {
                        $connected_entity = $client->first();
                        $url = route('clients.show', $connected_entity);
                        $type = 'Client';
                    }
                }

                if ($user->role === 'supplier') {
                    $supplier = Supplier::where('user_id', $user->id);

                    if ($supplier->exists()) {
                        $connected_entity = $supplier->first();
                        $url = route('suppliers.show', $connected_entity);
                        $type = 'Fournisseur';
                    }
                }

                $user = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'role' => $user->role,
                    'connected_entity' => [
                        'object' => $connected_entity,
                        'type' => $type,
                        'url' => $url
                    ]
                ];
            }

            $view->with('current_user', $user);
        });
    }
}