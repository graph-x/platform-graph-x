<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     *  Defined types.
     */
    const DELIVERY = 'delivery';
    const PHYSICAL_APPROVAL = 'physical_approval';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'types' => 'array',
    ];

    /**
     * The client that has the address.
     */
    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    /**
     * The client that has the address.
     */
    public function project()
    {
        return $this->belongsToMany('App\Project');
    }
}
