<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Process extends Model
{
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The suppliers that belongs to the activity.
     */
    public function suppliers()
    {
        return $this->belongsToMany('App\Supplier');
    }
}