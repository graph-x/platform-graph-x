<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ADMIN = 'admin';
    const SUPPLIER = 'supplier';
    const CLIENT = 'client';

    /**
     * Fillable attributes.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'label',
    ];

    public function permissions()
    {
        return $this->belongsToMany(Permission::class)->withTimestamps();
    }

    public function grantPermissionTo($permission)
    {
        return $this->permissions()->syncWithoutDetaching($permission);
    }

    public function scopeAdmin($query)
    {
        return $query->firstWhere('name', self::ADMIN);
    }

    public function scopeClient($query)
    {
        return $query->firstWhere('name', self::CLIENT);
    }

    public function scopeSupplier($query)
    {
        return $query->firstWhere('name', self::SUPPLIER);
    }
}