<?php

namespace App;

use App\Traits\Filterable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Client extends Model implements Searchable
{
    use Filterable;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $with = ['contacts'];

    /**
     * The relationships to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['buyerRoleDetails'];

    /**
     * @var string
     */
    public $searchableType = 'Client';

    /**
     * @return \Spatie\Searchable\SearchResult
     */
    public function getSearchResult(): SearchResult
    {
        $url = route('clients.show', $this->id);
        return new SearchResult($this, $this->name, $url);
    }

    /**
     * The addresses that belongs to the client.
     */
    public function addresses()
    {
        return $this->hasMany('App\Address');
    }

    /**
     * The contacts that belongs to the client.
     */
    public function contacts()
    {
        return $this->hasMany('App\ClientContact');
    }

    /**
     * The divisions that belongs to the client.
     */
    public function divisions()
    {
        return $this->hasMany('App\Division');
    }

    /**
     * The projects that belongs to the client.
     */
    public function projects()
    {
        return $this->hasMany('App\Project');
    }

    /**
     * Defines the {@link Client} to {@link User} model relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the first clients contact with a role of buyer.
     */
    public function getBuyerRoleDetailsAttribute()
    {
        return $this->contacts->filter(function ($contact) {
            return collect($contact->roles)->contains('buyer');
        })->first();
    }

    /**
     * Get the Client addresses with the given type.
     *
     * @param string $type
     */
    public function addressWithType($type)
    {
        return $this->addresses()->orderBy('address_line_1')->get()->filter(function ($address) use ($type) {
            return in_array($type, $address->types);
        });
    }

    /**
     * Get the Client contacts with the given role.
     *
     * @param string $role
     */
    public function contactsWithRole($role)
    {
        return $this->contacts()->orderBy('name')->get()->filter(function ($contact) use ($role) {
            return in_array($role, $contact->roles);
        });
    }

    public function formattedAddress(): string
    {
        return $this->address_line_1 . (!empty($this->address_line_2) ? ', ' . $this->address_line_2 : '') . ', ' . $this->city . ', ' . $this->province . ', ' . $this->country . ', ' . $this->postal_code;
    }

    /**
     * Default division for the client
     *
     * @return \App\Division
     */
    public function default_division()
    {
        return $this->divisions()->firstWhere('name', Division::DEFAULT);
    }
}
