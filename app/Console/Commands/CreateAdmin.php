<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:admin {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a user with the ADMIN role.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            abort_if(User::whereEmail($this->argument('email'))->exists(), 422, 'User with that email already exists in the system.');

            $user = User::create([
                'name' => 'Admin',
                'email' => $this->argument('email'),
                'role' => User::ADMIN,
                'password' => bcrypt($this->argument('password'))
            ]);

            $user->assignRole('admin');

            $this->info('Admin created for the email ' . $this->argument('email'));
        } catch (\Exception $e) {
            $this->warn($e->getMessage());
        }
    }
}
