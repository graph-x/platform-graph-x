<?php

namespace App\Console\Commands;

use App\Project;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UpdateProjectsEstimatesEndDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'projects:update-estimates-end-date';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the estimates date of the existing projects.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projects = Project::all();

        foreach ($projects as $project) {
            $project->update([
                'estimates_end_date' => Carbon::parse($project->created_at)->addDays(Project::ESTIMATION_PERIOD_DAYS)
            ]);
        }
    }
}
