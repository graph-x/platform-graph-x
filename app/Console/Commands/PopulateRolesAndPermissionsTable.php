<?php

namespace App\Console\Commands;

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Console\Command;

class PopulateRolesAndPermissionsTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'populate:authorization';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populate roles and permissions.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $admin = Role::create(['name' => 'admin', 'label' => 'Administrateur']);
        $client = Role::create(['name' => 'client', 'label' => 'Client']);
        $supplier = Role::create(['name' => 'supplier', 'label' => 'Fournisseur']);

        $oversee_everything = Permission::create(['name' => 'oversee_everything']);
        $create_projects = Permission::create(['name' => 'create_projects']);
        $update_projects = Permission::create(['name' => 'update_project']);
        $submit_estimates = Permission::create(['name' => 'submit_estimates']);
        $oversee_supplier_activities = Permission::create(['name' => 'oversee_supplier_activities']);

        $admin->grantPermissionTo($oversee_everything['id']);
        $client->grantPermissionTo([$update_projects['id'], $create_projects['id']]);
        $supplier->grantPermissionTo([$submit_estimates['id'], $oversee_supplier_activities['id']]);
    }
}