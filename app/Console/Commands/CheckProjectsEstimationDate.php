<?php

namespace App\Console\Commands;

use App\Item;
use App\Project;
use App\Supplier;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CheckProjectsEstimationDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'estimates:check-dates';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if the estimation date on the projects is reached, and if so, handle it.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projects = Project::query()->estimatable()->get();

        // @TODO Refactor
        $projects->each(function ($project) {
            // If the estimate end date is reached
            if (now()->gt(Carbon::parse($project->estimates_end_date))) {
                $project->update([
                    'status' => 'submissions_to_prepare'
                ]);

                if ($project->choose_estimates_automatically) {
                    try {
                        $items = $project->projectItems->map(function ($item) use ($project) {
                            // Get the first item with lowest quotes per item (and its children) buy supplier
                            return $item->suppliers->map(function ($supplier) use ($item, $project) {
                                // Check if supplier has submitted quotes for every quantity of the item
                                // if item is parent
                                if ($item->parent_item_id === null) {
                                    if ($item->has_linked_items()) {
                                        $supplier_submitted_estimate_for_all = $this->checkIfEstimatesSubmittedForItemAndItsChildren($item, $supplier);

                                        $supplier_quotes = $this->getQuotesForItemAndItsChildren($supplier, $item, $project);

                                        return [
                                            'description' => 'this is parent with children',
                                            'item_id' => $item['id'],
                                            'is_linked' => $item['is_linked'],
                                            'parent_item_id' => $item['parent_item_id'],
                                            'supplier_id' => $supplier['id'],
                                            'project_id' => $project['id'],
                                            'supplier_submitted_estimate_for_all' => $supplier_submitted_estimate_for_all,
                                            'quotes_sum' => $supplier_quotes['sum'],
                                            'submitted_at' => $supplier_quotes['first_quote_submitted_at']
                                        ];
                                    }

                                    $supplier_submitted_estimate_for_all_quantities =
                                        $item->quotes()->where('supplier_id', $supplier['id'])->count() === $item->quantities()->count();

                                    $supplier_quotes = $supplier->quotes()->where([
                                        'item_id' => $item['id'],
                                        'project_id' => $project['id']
                                    ])->get();

                                    return [
                                        'description' => 'this is parent without children',
                                        'item_id' => $item['id'],
                                        'is_linked' => $item['is_linked'],
                                        'parent_item_id' => $item['parent_item_id'],
                                        'supplier_id' => $supplier['id'],
                                        'project_id' => $project['id'],
                                        'supplier_submitted_estimate_for_all' => $supplier_submitted_estimate_for_all_quantities,
                                        'quotes_sum' => $supplier_quotes->sum->value,
                                        'submitted_at' => $supplier_quotes->count() ? $supplier_quotes[0]->submitted_at : null
                                    ];
                                }

                                return null;
                            })->filter()->filter(function ($i) {
                                return $i['supplier_submitted_estimate_for_all'] === true;
                            })->sortBy(function ($item) {
                                return [$item['quotes_sum'], $item['submitted_at']];
                            })->first();
                        });

                        // Here we have only the items that the all quotes are submitted for,
                        // grouped by parent item where the item children estimates and parent estimates are summed
                        // if the parent has children of course
                        $chosenQuotes = $items->filter();

                        // Approve quotes.
                        $chosenQuotes->each(function ($quote) {
                            $supplier = Supplier::find($quote['supplier_id']);

                            // Approve items on items table
                            $supplier->items()->where('item_id', $quote['item_id'])->updateExistingPivot($quote['item_id'], [
                                'quotes_declined_at' => null,
                                'quotes_approved_at' => now(),
                            ]);

                            // Approve quotes on quote table for parent item
                            $supplier->quotes()
                                ->where('item_id', $quote['item_id'])
                                ->where('project_id', $quote['project_id'])
                                ->get()
                                ->each(function ($quote) {
                                    $quote->update([
                                        'approved_at' => now()
                                    ]);
                                });


                            $item = Item::find($quote['item_id']);
                            // Approve linked items of the project if there are any.
                            if ($item->has_linked_items()) {
                                $item->linked_items->each(function ($i) use ($supplier) {
                                    $supplier->items()->where('item_id', $i->id)->updateExistingPivot($i->id, [
                                    'quotes_declined_at' => null,
                                    'quotes_approved_at' => now(),
                                ]);

                                    // Approve quotes on quote table for linked items.
                                    $supplier->quotes()
                                    ->where('item_id', $i->id)
                                    ->get()
                                    ->each(function ($quote) {
                                        $quote->update([
                                            'approved_at' => now()
                                        ]);
                                    });
                                });
                            }
                        });
                    } catch (\Exception $e) {
                        Log::info($e);
                    }
                }
            }
        });
    }

    /**
     * @param $item
     * @param $supplier
     * @return bool
     */
    private function checkIfEstimatesSubmittedForItemAndItsChildren($item, $supplier): bool
    {
        $quotes_submitted_for_item = $item->quotes()->where('supplier_id', $supplier['id'])->count() === $item->quantities()->count();

        $quotes_submitted_for_all_children = $item->linked_items->reject(function ($child) use ($supplier) {
            return $child->quotes()->where('supplier_id', $supplier['id'])->count() === $child->quantities()->count();
        })->count() === 0;

        return $quotes_submitted_for_item && $quotes_submitted_for_all_children;
    }

    /**
     * @param $supplier
     * @param $item
     * @param $project
     * @return array
     */
    private function getQuotesForItemAndItsChildren($supplier, $item, $project): array
    {
        $parent_quotes = $item->quotes()->where('supplier_id', $supplier['id'])->get();
        $parent_quotes_value_with_all_quantities = $parent_quotes->sum->value;
        $children_value_with_all_quantities = $item->linked_items->map(function ($item) use ($supplier) {
            return $item->quotes()->where('supplier_id', $supplier['id'])->get()->sum->value;
        })->sum();

        return [
            'sum' => $parent_quotes_value_with_all_quantities + $children_value_with_all_quantities,
            'first_quote_submitted_at' => $parent_quotes->count() ? $parent_quotes[0]->submitted_at : null
        ];
    }
}
