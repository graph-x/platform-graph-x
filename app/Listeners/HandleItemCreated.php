<?php

namespace App\Listeners;

use App\Events\ItemCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Quote;

class HandleItemCreated
{
    /**
     * Handle the event.
     *
     * @param  ItemCreated  $event
     * @return void
     */
    public function handle(ItemCreated $event)
    {
        $quantities = $event->item->fresh()->quantities;
        $optionGroups = $event->item->fresh()->optionGroups;
        $suppliers = $event->item->suppliers->fresh();


        foreach ($quantities as $quantity) {
            foreach ($event->item->fresh()->optionGroups as $optionGroup) {
                foreach ($suppliers as $supplier) {
                        Quote::create([
                            'item_id' => $event->item->id,
                            'project_id' => $event->item->project_id,
                            'option_group_id' => $optionGroup->id,
                            'supplier_id' => $supplier->id,
                            'quantity_id' => $quantity->id
                        ]);
                    }
                }
        }
    }
}
