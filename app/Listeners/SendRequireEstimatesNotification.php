<?php

namespace App\Listeners;

use App\Events\RequireEstimates;
use App\Item;
use App\Mail\NotifySupplierContacts;
use App\Supplier;
use App\SupplierContact;
use Illuminate\Support\Facades\Mail;

class SendRequireEstimatesNotification
{
    /**
     * Handle the event.
     *
     * @param  RequireEstimates  $event
     * @return void
     */
    public function handle(RequireEstimates $event)
    {
        $this->sendEmailWhereNeccessary($event);
    }

    /**
     * Get the suppliers of the project that are not notified, and send them emails.
     *
     * @param $event
     */
    private function sendEmailWhereNeccessary($event): void
    {
        // Here we are getting suppliers that arent notified, from project items.
        $suppliersToNotify = collect();

        $event->project->fresh()->projectItems->each(function ($projectItem) use ($suppliersToNotify) {
            $unnotifiedSuppliers = $projectItem->suppliers()->withPivot('notified_at')->whereNull('notified_at')->get();

            if ($unnotifiedSuppliers->isNotEmpty()) {
                $supplierItemCollection = collect([
                    'suppliers_to_notify' => $unnotifiedSuppliers,
                    'assigned_item' => $projectItem,
                ]);

                $suppliersToNotify->push($supplierItemCollection);
            }
        });

        if ($suppliersToNotify->isNotEmpty()) {
            $suppliersToNotify->each(function ($group) use ($event) {
                $group['suppliers_to_notify']->each(function ($supplier) use ($group, $event) {
                    $this->notifyEstimator($supplier, $group['assigned_item'], $event);
                });
            });
        }
    }

    /**
     * Send email to supplier's contacts with estimator role.
     *
     * @param  \App\Supplier $supplier
     * @param  \App\Item     $item
     * @param                $event
     */
    private function notifyEstimator(Supplier $supplier, Item $item, $event): void
    {
        $supplier->contacts->filter(function ($contact) {
            return collect($contact->roles)->contains(SupplierContact::ESTIMATOR);
        })->each(function ($contact) use ($item) {
            Mail::to($contact->email)->send(new NotifySupplierContacts($item));
        });

        $this->markSupplierAsNotified($supplier, $event);
    }

    /**
     * @param  \App\Supplier $supplier
     * @param                $event
     */
    private function markSupplierAsNotified(Supplier $supplier, $event): void
    {
        $supplier
            ->items()
            ->where('project_id', $event->project->id)
            ->wherePivot('notified_at', null)
            ->each(function ($item) use ($supplier) {
                $item->suppliers()->updateExistingPivot($supplier->id, ['notified_at' => now()]);
            });

        // Envoye le - sent at
        $event->project->suppliers()
            ->where('supplier_id', $supplier->id)
            ->updateExistingPivot(
                $supplier->id,
                ['quotes_request_sent_at' => now()]
            );
    }
}
