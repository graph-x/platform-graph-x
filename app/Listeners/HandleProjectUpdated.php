<?php

namespace App\Listeners;

use App\Events\ProjectUpdated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Invoice;
use Illuminate\Support\Str;
use App\InvoiceLineItem;
use App\Status;
use Illuminate\Support\Facades\Log;
use App\ProjectManagementFee;
use App\Utilities\InvoiceCostCalculator;

class HandleProjectUpdated
{
    /**
     * Handle the event.
     *
     * @param  ProjectUpdated  $event
     * @return void
     */
    public function handle(ProjectUpdated $event)
    {
        if ($event->project->status === Status::WORK_COMPLETED_BILLING) {
            // if ($event->project->invoice()->exists()) {
            //     $event->project->invoice()->delete();
            // }
            $event->project->invoice()->update(['status' => 'open']);
            // $unique_invoice_id = Str::upper(Str::substr(Str::orderedUuid(), 0, 20));

            // $invoice = Invoice::create([
            //     'unique_code' => "IN-{$unique_invoice_id}",
            //     'project_id' => $event->project->id,
            //     'client_id' => $event->project->client->id,
            //     'client_name' => $event->project->client->name,
            //     'title' => "Invoice for {$event->project->title}",
            //     'description' => $event->project->short_description,
            //     'due_date' => now()->addMonth(),
            //     'paid' => false,
            //     'status' => Invoice::OPEN, // ['open', 'paid', 'draft']
            //     'currency' => Invoice::DEFAULT_CURRENCY,
            //     'tps_tax_rate' => Invoice::TPS_TAX_RATE, // in % TPS - taxes Goods and services tax 5%
            //     'tvq_tax_rate' => Invoice::TVQ_TAX_RATE, // in % TVQ, - taxes  9.975%
            //     'subtotal' => 0, // SOUS-TOTAL
            //     'tps_amount' => 0, // TPS amount should be calculated automatically from the line items
            //     'tvq_amount' => 0, // TVQ amount should be calculated from the line items
            //     'total_amount' => 0, // need to sum all of the InvoiceLineItems.
            //     'profit' => 0, // total profit includes project management fee + profit per item?
            // ]);

            // ProjectManagementFee::create([
            //     'invoice_id' => $invoice['id'],
            //     'fee' => 0,
            // ]);

            // foreach ($event->project->approvedQuotes() as $quote) {
            //     $invoice_line_item = InvoiceLineItem::create([
            //         'invoice_id' => $invoice->id,
            //         'project_id' => $quote->project_id,
            //         'item_id' => $quote->item_id,
            //         'supplier_id' => $quote->supplier_id, // because we need to get the approved/submission price.
            //         'supplier_name' => $quote->supplier->name,
            //         'title' => $quote->item->name . " " . $quote->quantity->value . "pts",
            //         'description' => $quote->item->description,
            //         'quantity' => $quote->quantity->value,
            //         'approved_price' => $quote->value, // PRIX APROUVE
            //         'cost_price' => $quote->value, // PRIX COÙTANT,
            //         'customer_price' => 0,
            //         'delivery_date' => $quote->item->delivery_deadline,
            //         'profit_type' => InvoiceLineItem::FIXED_AMOUNT_PROFIT,
            //         'admin_profit' => 0, // BÉNÉFICE PER ITEM / Profit per item for admin,
            //         'custom_made' => false // created from +
            //     ]);
            // }


            $costCalculator = new InvoiceCostCalculator($event->project->invoice->fresh());
            $subtotal = $costCalculator->subtotal(); // before taxes;
            $tps_tax_amount = $costCalculator->tpsAmountFrom($subtotal);
            $tvq_tax_amount = $costCalculator->tvqAmountFrom($subtotal);

            $total = $subtotal + $tps_tax_amount + $tvq_tax_amount;

            $event->project->invoice->update([
                'subtotal' => $subtotal,
                'tps_amount' => $tps_tax_amount,
                'tvq_amount' => $tvq_tax_amount,
                'total_amount' => $total
            ]);
        }
    }
}
