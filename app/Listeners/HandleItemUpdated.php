<?php

namespace App\Listeners;

use App\Events\ItemUpdated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use App\Quote;

class HandleItemUpdated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ItemUpdated  $event
     * @return void
     */
    public function handle(ItemUpdated $event)
    {
        $quotes = $event->item->quotes->fresh();
        $suppliers = $event->item->suppliers->fresh();
        $quantity_id = $event->item->quantities()->first()->id;
        $quantities = $event->item->fresh()->quantities;

        foreach ($quotes as $quote) {
            // means we removed the supplier from the item, and we need to delete the quote.
            if (!$suppliers->pluck('id')->contains($quote->supplier_id)) {
                $quote->delete();
            }
        }

        foreach ($quantities as $quantity) {
            foreach ($event->item->fresh()->optionGroups as $optionGroup) {
                foreach ($suppliers as $supplier) {
                        Quote::updateOrCreate([
                            'item_id' => $event->item->id,
                            'project_id' => $event->item->project_id,
                            'option_group_id' => $optionGroup->id,
                            'supplier_id' => $supplier->id,
                            'quantity_id' => $quantity->id
                        ]);
                    }
                }
        }
    }
}
