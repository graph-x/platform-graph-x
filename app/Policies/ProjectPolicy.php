<?php

namespace App\Policies;

use App\Project;
use App\User;

class ProjectPolicy extends BasePolicy
{
    /**
     * @inheritDoc
     */
    public function viewAny(User $user)
    {
        return $this->isAdmin($user) ||
            $this->isClient($user) ||
            $this->isSupplier($user);
    }

    /**
     * @inheritDoc
     */
    public function view(User $user, $model)
    {
        if ($this->isSupplier($user)) {
            if ($model instanceof Project) {
                return 0 !== $model->suppliers()->where('user_id', $user->id)->count();
            }
        }

        if ($this->isClient($user)) {
            if ($model instanceof Project) {
                return $model->client->user_id === $user->id;
            }
        }

        return $this->isAdmin($user);
    }

    /**
     * @inheritDoc
     */
    public function update(User $user, $model)
    {
        return $this->isAdmin($user);
    }
}
