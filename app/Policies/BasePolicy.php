<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

abstract class BasePolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return $this->isAdmin($user);
    }

    public function view(User $user, $model)
    {
        return $this->isAdmin($user);
    }

    public function create(User $user)
    {
        return $this->isAdmin($user);
    }

    public function update(User $user, $model)
    {
        return $this->isAdmin($user);
    }

    public function delete(User $user, $model)
    {
        return $this->isAdmin($user);
    }

    public function restore(User $user, $model)
    {
        return $this->isAdmin($user);
    }

    public function forceDelete(User $user, $model)
    {
        return $this->isAdmin($user);
    }

    protected function isAdmin(User $user)
    {
        return 'admin' === $user->role;
    }

    protected function isClient(User $user)
    {
        return 'client' === $user->role;
    }
    
    protected function isSupplier(User $user)
    {
        return 'supplier' === $user->role;
    }
}
