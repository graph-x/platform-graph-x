<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceLineItem extends Model
{
    const FIXED_AMOUNT_PROFIT = 'fixed_amount';

    public static $profit_types = [
        [
            'key' => self::FIXED_AMOUNT_PROFIT,
            'name' => 'Montant fixe'
        ]
    ];

    protected $guarded = [];

    protected $casts = [
        'custom_made' => 'boolean'
    ];

    /**
     * Function description
     *
     * @param
     * @return
     */
    public function quote()
    {
        return $this->belongsTo(Quote::class, 'quote_id');
    }
}
