<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    const DEFAULT = 'clients_default_division';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The client that has the division.
     */
    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    /**
     * The client contacts that belongs to the division.
     */
    public function contacts()
    {
        return $this->belongsToMany('App\ClientContact');
    }

    /**
     * Get the division contacts with the given role.
     *
     * @param string $role
     */
    public function contactsWithRole($role)
    {
        return $this->contacts()->orderBy('name')->get()->filter(function ($contact) use ($role) {
            return in_array($role, $contact->roles);
        });
    }
}
