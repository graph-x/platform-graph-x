<?php

namespace App\Http\Middleware;

use Closure;

class RemoveNavigation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $removeNavigation = filter_var($request->get('nonav', 'false'), FILTER_VALIDATE_BOOLEAN);

        view()->share('removeNavigation', $removeNavigation);

        return $next($request);
    }
}

