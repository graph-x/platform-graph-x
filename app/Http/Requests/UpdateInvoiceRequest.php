<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\MustBeInteger;
use App\Rules\FirstLineMax255;

class UpdateInvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'invoice.line_items.*.admin_profit' => ['nullable'],
            'invoice.line_items.*.description' => ['required', 'max:500', new FirstLineMax255()],
            'invoice.line_items.*.supplier_id' => ['required'],
            'invoice.line_items.*.approved_price' => ['nullable'],
            'invoice.line_items.*.customer_price' => ['nullable'],
            'invoice.line_items.*.cost_price' => ['nullable'],
            'invoice.project_management.title' => ['nullable'],
            'invoice.project_management.description' => ['nullable'],
            'invoice.project_management.fee' => ['nullable'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'invoice.line_items.*.admin_profit.required' => 'Le champ quantité est obligatoire.',
            'invoice.line_items.*.description.required' => 'Le champ description est obligatoire.',
            'invoice.line_items.*.supplier_id.required' => 'Le champ fournisseurs est obligatoire.',
            'invoice.project_management.title.required' => 'Le champ titre est obligatoire.',
            'invoice.project_management.description.required' => 'Le champ description est obligatoire.',
        ];
    }
}
