<?php

namespace App\Http\Requests;

use App\Rules\PhoneNumber;
use App\Rules\AtleastOneChecked;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SupplierContactsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['nullable', new PhoneNumber],
            'mobile' => ['nullable', new PhoneNumber],
            'email' => ['required', 'email', Rule::unique('App\SupplierContact')->ignore($this->id)],
            'roles' => [new AtleastOneChecked],
        ];
    }
}
