<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProjectItemsAsClient extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'items.*.description' => ['required', 'max:500'],
            'items.*.quantities' => ['required'],
            'items.*.delivery_deadline' => ['required'],
            'items.*.deliveryAddress' => ['required'],
            'items.*.deliveryContact' => ['required'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'items.*.quantities.required' => 'Le champ quantité est obligatoire.',
            'items.*.suppliers.required' => 'Le champ fournisseurs est obligatoire.',
            'items.*.delivery_deadline.required' => 'Le champ date limite livraison est obligatoire.',
            'items.*.deliveryAddress.required' => 'Le champ addresse est obligatoire.',
            'items.*.deliveryContact.required' => 'Le champ contact est obligatoire.',
        ];
    }
}
