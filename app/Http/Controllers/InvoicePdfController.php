<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use LaravelDaily\Invoices\Invoice;
use Illuminate\Support\Facades\Auth;
use LaravelDaily\Invoices\Classes\Buyer;
use LaravelDaily\Invoices\Classes\InvoiceItem;
use LaravelDaily\Invoices\Classes\Party;


class InvoicePdfController extends Controller
{
    public function show($invoice_unique_code)
    {
        // Check if project has generated invoice
        $app_invoice = \App\Invoice::where('unique_code', $invoice_unique_code)->firstOrFail();

        // show the invoice
        $project = $app_invoice->project;
        $client = $project->client;
        $client_name = $client->name;
        $client_email = $client->user->email;
        $client_address = $client->formattedAddress();
        $client_phone = $client->phone;
        $invoice_code = $invoice_unique_code;
        $invoice_created_at = $project->invoice->created_at;
        $line_items = $app_invoice->line_items;
        $project_management = $app_invoice->project_management_fee;
        $tps_rate =  $app_invoice->tps_tax_rate * 100;
        $tvq_rate =  $app_invoice->tvq_tax_rate * 100;
        $total_tax_rates = $tps_rate + $tvq_rate;
        $tps_amount = $app_invoice->tps_amount;
        $tvq_amount = $app_invoice->tvq_amount;
        $total_taxes = $tps_amount + $tvq_amount;


        // Seller
        $graphx = new Party([
            'name'          => 'Graph X',
            'phone'         => '(123) 456-789',
        ]);

        // Buyer
        $client = new Party([
            'name'          => $client_name,
            'address'       => $client_address,
            'phone'          => $client_phone,
            'email' => $client_email,
            'custom_fields' => [
                'order number' => $invoice_code,
            ],
        ]);

        $items = collect();

        foreach ($line_items as $line_item) {
            $item_subtotal = $line_item->cost_price + $line_item->admin_profit;

            $items->push((new InvoiceItem())
                ->title($line_item->title)
                ->pricePerUnit($item_subtotal / $line_item->quantity)
                ->quantity($line_item->quantity)
                ->subTotalPrice($item_subtotal));
        }

        $items->push((new InvoiceItem())
            ->title($project_management->title ?? 'Gestion du projet')
            ->pricePerUnit($project_management->fee)
            ->quantity(1)
            ->subTotalPrice($project_management->fee));



        $items = $items->toArray();

        $formatted_tps = "TPS TAX:" . " $" . $tps_amount .  " (" . $tps_rate . "%)";
        $formatted_tvq = "TVQ TAX:" . " $" . $tvq_amount . " (" .$tvq_rate . "%)";

        $notes = [
            '',
            $formatted_tps,
            $formatted_tvq,
        ];

        $notes = implode("<br>", $notes);

        $name = $project->status === 'work_completed_billing' || $project->status === 'partially_completed_work' ? "Invoice for " : "Preinvoice for ";

        $invoice = Invoice::make($name . $project->title )
            ->seller($graphx)
            ->buyer($client)
            ->date($invoice_created_at)
            ->dateFormat('m/d/Y')
//            ->payUntilDays(now()->addWeeks(2))
            ->currencySymbol('$')
            ->currencyCode('CAD')
            ->currencyFormat('{SYMBOL}{VALUE}')
            ->currencyThousandsSeparator('.')
            ->currencyDecimalPoint(',')
            ->filename($invoice_unique_code)
            ->addItems($items)
            ->notes($notes)
            ->taxRate($total_tax_rates)
            // You can additionally save generated invoice to configured disk
            ->save('public');

        $link = $invoice->url();
        // Then send email to party with link

        // And return invoice itself to browser or have a different view
        return $invoice->stream();
    }
}
