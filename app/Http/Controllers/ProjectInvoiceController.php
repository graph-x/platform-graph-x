<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\ProjectInvoiceResource;
use App\Http\Requests\UpdateInvoiceRequest;
use App\Invoice;
use App\InvoiceLineItem;
use App\Utilities\InvoiceCostCalculator;
use Illuminate\Support\Facades\Mail;
use App\Mail\ClientSubmissionApprovalRequest;

class ProjectInvoiceController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        abort_unless(Auth::user()->isAdmin() || $project->client->user_id === Auth::id(), 403);

        return new ProjectInvoiceResource($project->invoice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInvoiceRequest $request, Project $project, Invoice $invoice)
    {
        abort_unless(Auth::user()->isAdmin(), 403);

        // Get all the line items
        $line_items = $this->clearLooseItems($invoice, $request);

        $project_management = $request['invoice']['project_management'];

        $line_items->each(function ($line_item) use ($invoice) {
            $existingItem = InvoiceLineItem::whereId($line_item['id'])->first();

            if (is_null($existingItem)) {
                return $this->createNew($line_item, $invoice);
            }

            return $this->updateExisting($existingItem, $line_item, $invoice);
        });

        // And update the profit
        $invoice->project_management_fee()->update([
            'title' => $project_management['title'],
            'description' => $project_management['description'],
            'fee' => $project_management['fee'],
        ]);

        $costCalculator = new InvoiceCostCalculator($invoice->fresh());
        $subtotal = $costCalculator->subtotal();
        $tps_amount = $costCalculator->tpsAmountFrom($subtotal);
        $tvq_amount = $costCalculator->tvqAmountFrom($subtotal);
        $profit = $costCalculator->profit();
        $total_amount = $subtotal + $tps_amount + $tvq_amount;

        // Update invoice
        $invoice->update([
            'subtotal' =>  $subtotal,
            'tps_amount' => $tps_amount,
            'tvq_amount' => $tvq_amount,
            'profit' => $profit,
            'total_amount' => $total_amount,
        ]);

        $responseMessage = __('Invoice updated.');

        if ($request['sendForClientApproval']) {

            $project->quotes->each->update([
                'approved_by_client' => false,
                'declined_by_client_at' => null
            ]);

            $client = $project->client;
            $invoicingRole = $client->contactsWithRole('invoicing')->first();

            $invoicing = $invoicingRole->exists() ? $invoicingRole->email : $client->user->email;

            Mail::to($invoicing)->send(new ClientSubmissionApprovalRequest($project, $invoice->fresh(), $client));

            $invoice->notified_at = now();

            $invoice->save();

            $project->update([
                'status' => 'submissions_sent_awaiting_client_approval'
            ]);

            $responseMessage = __('Quote approval request sent.');
        }

        return response(['message' => $responseMessage], 200);
    }

    private function createNew($line_item, $invoice)
    {
        $invoice->line_items()->create([
            'project_id' => $invoice['project_id'],
            'supplier_id' => $line_item['supplier_id'],
            'supplier_name' => $line_item['supplier_name'],
            'title' => explode("\n", $line_item['description'])[0],
            'description' => $line_item['description'],
            'quantity' => $line_item['quantity'],
            'approved_price' => $line_item['approved_price'],
            'cost_price' => $line_item['cost_price'],
            'customer_price' => $line_item['approved_price'],
            'delivery_date' => $line_item['delivery_date'],
            'profit_type' => $line_item['profit_type']['key'],
            'admin_profit' => $line_item['admin_profit'],
            'custom_made' => $line_item['custom_made'],
        ]);
    }

    private function updateExisting($existingItem, $line_item, $invoice)
    {
        if ($existingItem->custom_made) {
            return $existingItem->update([
                'supplier_id' => $line_item['supplier_id'],
                'supplier_name' => $line_item['supplier_name'],
                'title' => explode("\n", $line_item['description'])[0],
                'description' => $line_item['description'],
                'quantity' => $line_item['quantity'],
                'approved_price' => $line_item['approved_price'],
                'cost_price' => $line_item['cost_price'],
                'customer_price' => $line_item['customer_price'],
                'delivery_date' => $line_item['delivery_date'],
                'profit_type' => $line_item['profit_type']['key'],
                'admin_profit' => $line_item['admin_profit'],
            ]);
        }

        $existingItem->update([
            'admin_profit' => number_format($line_item['admin_profit'], 2),
            'profit_type' => $line_item['profit_type']['key'],
            'customer_price' => $line_item['customer_price']
        ]);
    }

    /**
     *  If some project items are present in DB, but are not present in request items data
     *  that means that the user deleted them on the front end - so we remove them from database.
     *
     * @param  \App\Invoice                         $project
     * @param  \App\Http\Requests\UpdateInvoiceRequest $request
     * @return \Illuminate\Support\Collection
     */
    private function clearLooseItems(Invoice $invoice, UpdateInvoiceRequest $request): \Illuminate\Support\Collection
    {
        $items = collect($request['invoice']['line_items']);

        foreach ($invoice->line_items as $item) {
            if (!$items->pluck('id')->contains($item->id)) {
                $item->delete();
            }
        }

        return $items;
    }
}
