<?php

namespace App\Http\Controllers\Api\Admin;

use App\Address;
use App\Client;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaveAddressRequest;
use App\Http\Resources\ClientAddressResource;
use Illuminate\Http\Request;

class ClientAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Client $client
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Client $client)
    {
        return ClientAddressResource::collection($client->addresses()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Client                           $client
     * @param  \App\Http\Requests\SaveAddressRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Client $client, SaveAddressRequest $request)
    {
        abort_unless(
            auth()->user()->isAdmin() ||
            auth()->user()->isClient() ||
            $client->user_id === auth()->id(),
            403
        );

        $address = $client->addresses()->create($this->arrayFrom($request));

        $addressForClientTab = [
            'id' => $address->id,
            'name' => $this->formatAddress($address),
        ];

        $addressForItemTab = [
            'value' => $address->id,
            'display' => $this->formatAddress($address),
        ];

        return response([
            "message" => __("Address created for ") . $client->name,
            "address" => $addressForClientTab,
            "addressForItemTab" => $addressForItemTab,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Client                           $client
     * @param  \App\Address                          $address
     * @param  \App\Http\Requests\SaveAddressRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(Client $client, Address $address, SaveAddressRequest $request)
    {
        abort_unless(
            auth()->user()->isAdmin() ||
            $client->user_id === auth()->id(),
            403
        );

        $address->update($this->arrayFrom($request));

        return response(['message' => __('Address updated.')], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @param  \App\Address $address
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client, Address $address)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $address->delete();

        return response(['message' => __('Address deleted.')], 200);
    }

    /**
     * @param $request
     * @return array
     */
    private function arrayFrom($request)
    {
        return [
            'address_line_1' => $request['address_line_1'],
            'city' => $request['city'],
            'province' => $request['province'],
            'country' => $request['country'],
            'postal_code' => $request['postal_code'],
            'types' => collect($request['types'])->reject(false)->keys()->toArray(),
        ];
    }

    private function formatAddress($address)
    {
        return $address->address_line_1 . (!empty($address->address_line_2) ? ', ' . $address->address_line_2 : '') . ', ' . $address->city . ', ' . $address->province . ', ' . $address->country . ', ' . $address->postal_code;
    }
}
