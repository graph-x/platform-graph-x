<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Filters\UserFilters;
use Illuminate\Validation\Rule;
use App\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(UserFilters $filters)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        return UserResource::collection(User::filter($filters)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Request $request)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        $user = User::create([
            'name' => $data['name'],
            'role' => User::DEFAULT,
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        return response([
            'message' => 'User created.',
            'user' => $user,
            'redirect_route' => route('users.index'),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        return view('users.show', [
            'user' => json_encode(new UserResource($user)),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $validated = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->id)],
            'password' => ['sometimes', 'nullable', 'confirmed', 'min:8', 'max:255'],
        ]);

        $user->update([
            'name' => $request['name'],
            'email' => $request['email'] ?? $user->email,
            'password' =>  is_null($request['password']) ? $user->password : Hash::make($request['password']),
        ]);

        return response(['message' => __('User updated!')], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        if ($user->isAdmin()) {
            return response([
                'message' => __('Administrators cannot be deleted.'),
                'redirect_route' => route('users.index'),
            ], 200);
        }

        $user->delete();

        return response([
            'message' => __('User deleted.'),
            'redirect_route' => route('users.index'),
        ], 200);
    }

    /**
     * Get the available users for connecting with other entities (Supplier or Client)
     */
    public function availableUsersForEntities()
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        return UserResource::collection(User::query()->available()->get()->sortBy('name'));
    }
}