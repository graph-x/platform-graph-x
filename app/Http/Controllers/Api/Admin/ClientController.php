<?php

namespace App\Http\Controllers\Api\Admin;

use App\Role;
use App\User;
use App\Client;
use App\Rules\PhoneNumber;
use Illuminate\Http\Request;
use App\Filters\ClientFilters;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Http\Resources\ClientResource;
use App\Rules\NotAssignedToOtherEntities;
use App\Division;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Filters\ClientFilters $filters
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(ClientFilters $filters)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        return ClientResource::collection(
            Client::filter($filters)->with([
                'projects', 'addresses', 'divisions', 'user',
            ])->get()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'user.id' => ['required', 'exists:users,id', new NotAssignedToOtherEntities('client')],
            'phone' => ['required', new PhoneNumber],
            'address' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'province' => ['required', 'string', 'max:255'],
            'country' => ['required', 'string', 'max:255'],
            'postal_code' => ['required', 'string', 'max:255'],
        ]);

        try {
            $client = Client::create([
                'name' => $request['name'],
                'user_id' => $request['user']['id'],
                'phone' => $request['phone'],
                'address_line_1' => $request['address'],
                'city' => $request['city'],
                'province' => $request['province'],
                'country' => $request['country'],
                'postal_code' => $request['postal_code'],
            ]);

            $client->divisions()->create([
                'name' => Division::DEFAULT
            ]);

            User::find($request['user']['id'])->assignEntity(Role::CLIENT, $client);

            return response([
                'message' => __('Client Created!'),
                'redirect_route' => route('clients.index'),
                'client' => $client,
            ], 200);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client $client
     * @return \App\Http\Resources\ClientResource|\Illuminate\View\View
     */
    public function show(Client $client)
    {
        abort_unless(
            auth()->user()->isAdmin() ||
            auth()->id() === $client['user_id'],
            403
        );

        if (request()->wantsJson()) {
            return new ClientResource($client);
        }

        return view('clients.show', [
            'client' => json_encode(new ClientResource($client)),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Client              $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'user.id' => ['required', 'exists:users,id', new NotAssignedToOtherEntities('client', null, $client)],
            'phone' => ['required', new PhoneNumber],
            'address_line_1' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'province' => ['required', 'string', 'max:255'],
            'country' => ['required', 'string', 'max:255'],
            'postal_code' => ['required', 'string', 'max:255'],
        ]);

        /** If the user is changed */
        if ($client->user_id !== (int) $request['user']['id']) {
            $user = User::find($client->user_id);
            if ($user) {
                $user->clearConnectedEntity();
            }

            User::find((int) $request['user']['id'])->assignEntity(Role::CLIENT, $client);
        }

        $client->update([
            'name' => $request['name'],
            'user_id' => $request['user']['id'],
            'phone' => $request['phone'],
            'address_line_1' => $request['address_line_1'],
            'city' => $request['city'],
            'province' => $request['province'],
            'country' => $request['country'],
            'postal_code' => $request['postal_code'],
        ]);

        return response(['message' => __('Client updated.')], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client $client
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $client->delete();

        return response([
            'message' => __('Client deleted.'),
            'redirect_route' => route('clients.index'),
        ], 200);
    }
}
