<?php

namespace App\Http\Controllers\Api\Admin;

use App\Filters\SupplierFilters;
use App\Http\Controllers\Controller;
use App\Http\Resources\SupplierResource;
use App\Rules\NotAssignedToOtherEntities;
use App\Rules\PhoneNumber;
use App\Supplier;
use Illuminate\Http\Request;
use App\Role;
use App\User;
use Illuminate\Support\Facades\Hash;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Filters\SupplierFilters $filters
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(SupplierFilters $filters)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        return SupplierResource::collection(
            Supplier::filter($filters)->with(['contacts', 'projects'])->get()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'user.id' => ['required', 'exists:users,id', new NotAssignedToOtherEntities('supplier')],
            'phone' => [new PhoneNumber],
            'address' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'province' => ['required', 'string', 'max:255'],
            'country' => ['required', 'string', 'max:255'],
            'postal_code' => ['required', 'string', 'max:255'],
        ]);

        $supplier = Supplier::create([
            'name' => $request['name'],
            'user_id' => $request['user']['id'],
            'phone' => $request['phone'],
            'address_line_1' => $request['address'],
            'city' => $request['city'],
            'province' => $request['province'],
            'country' => $request['country'],
            'postal_code' => $request['postal_code'],
        ]);

        try {
            User::find($request['user']['id'])->assignEntity(Role::SUPPLIER, $supplier);

            if ($request['processes']) {
                $supplier->processes()->sync(collect($request['processes'])->filter->checked->pluck('id'));
            }

            return response([
                'message' => __('Supplier Created.'),
                'redirect_route' => route('suppliers.index'),
                'supplier' => $supplier->only(['id', 'name']),
            ], 200);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\View\View
     */
    public function show(Supplier $supplier)
    {
        return view('suppliers.show', [
            'supplier' => json_encode(new SupplierResource($supplier)),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Supplier $supplier, Request $request)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $validated = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'user.id' => ['sometimes', 'exists:users,id', new NotAssignedToOtherEntities('supplier', $supplier, null)],
            'phone' => [new PhoneNumber],
            'address_line_1' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'province' => ['required', 'string', 'max:255'],
            'country' => ['required', 'string', 'max:255'],
            'postal_code' => ['required', 'string', 'max:255'],
        ]);

        /** If the user is changed */
        if ($supplier->user_id !== (int) $request['user']['id']) {
            $user = User::find($supplier->user_id);
            if ($user) {
                $user->clearConnectedEntity();
            }

            User::find((int) $request['user']['id'])->assignEntity(Role::SUPPLIER, $supplier);
        }

        $supplier->update([
            'name' => $request['name'],
            'user_id' => $request['user']['id'],
            'phone' => $request['phone'],
            'address_line_1' => $request['address_line_1'],
            'city' => $request['city'],
            'province' => $request['province'],
            'country' => $request['country'],
            'postal_code' => $request['postal_code'],
        ]);

        $supplier->processes()->sync(collect($request['processes'])->filter->checked->pluck('id'));

        return response(['message' => __('Supplier updated!')], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier $supplier
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $supplier->delete();

        return response([
            'message' => __('Supplier deleted.'),
            'redirect_route' => route('suppliers.index'),
        ], 200);
    }
}
