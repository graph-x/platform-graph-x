<?php

namespace App\Http\Controllers\Api\Admin;

use App\Client;
use App\Http\Controllers\Controller;
use App\Project;
use App\Supplier;
use Illuminate\Http\Request;
use Spatie\Searchable\ModelSearchAspect;
use Spatie\Searchable\Search;

class GlobalSearchController extends Controller
{
    /**
     * Display a listing of the resource.
     * Returns results for partial matches on given fields.
     *
     * @param  \Illuminate\Http\Request     $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $results = (new Search())
            ->registerModel(Project::class, function (ModelSearchAspect $modelSearchAspect) {
                $modelSearchAspect
                    ->addSearchableAttribute('title')
                    ->addSearchableAttribute('short_description')
                    ->addSearchableAttribute('unique_code')
                    ->take(5);
            })
            ->registerModel(Client::class, function (ModelSearchAspect $modelSearchAspect) {
                $modelSearchAspect
                    ->addSearchableAttribute('name')
                    ->addSearchableAttribute('phone')
                    ->addSearchableAttribute('address_line_1')
                    ->addSearchableAttribute('city')
                    ->addSearchableAttribute('province')
                    ->addSearchableAttribute('country')
                    ->take(5);
            })
            ->registerModel(Supplier::class, function (ModelSearchAspect $modelSearchAspect) {
                $modelSearchAspect
                    ->addSearchableAttribute('name')
                    ->addSearchableAttribute('phone')
                    ->addSearchableAttribute('address_line_1')
                    ->addSearchableAttribute('city')
                    ->addSearchableAttribute('province')
                    ->addSearchableAttribute('country')
                    ->take(5);
            })
            ->search($request['query']);

        return response()->json($results);
    }
}