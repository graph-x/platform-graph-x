<?php

namespace App\Http\Controllers\Api\Admin;

use App\Client;
use App\ClientContact;
use App\Http\Controllers\Controller;
use App\Http\Requests\ClientContactsRequest;
use App\Http\Resources\ClientContactsResource;
use App\Division;

class ClientContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Client $client
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Client $client)
    {
        abort_unless(
            auth()->user()->isAdmin() ||
            auth()->user()->isClient() ||
            auth()->id() === $client->user_id,
            403
        );

        return ClientContactsResource::collection(
            $client->contacts()->with('divisionsRelation')->get()
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Client                                   $client
     * @param  \App\Http\Requests\ClientContactsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Client $client, ClientContactsRequest $request)
    {
        abort_unless(
            auth()->user()->isAdmin() ||
            auth()->user()->isClient() ||
            auth()->id() === $client->user_id,
            403
        );

        $contact = $client->contacts()->create([
            'name' => $request['name'],
            'phone' => $request['phone'],
            'mobile' => $request['mobile'],
            'email' => $request['email'],
            'roles' => collect($request['roles'])->filter(function ($role) {
                return $role['checked'];
            })->keys()->toArray(),
        ]);

        $divisions = collect($request['divisions'])->filter(function ($division) {
            return isset($division['checked']) && $division['checked'] === true;
        })->pluck('id');

        if ($client->divisions()->whereName(Division::DEFAULT)->exists()) {
            $default_division = $client->divisions()->whereName(Division::DEFAULT)->first();
            $divisions = $divisions->push($default_division['id']);
        }

        $divisions = $divisions->filter()->all();

        $contact->divisions()->sync($divisions);

        // When contact is created from Item tab..
        $contact_for_item = [
            'value' => $contact->id,
            'display' => $contact->name,
        ];

        return response([
            'message' => __('Contact created for ') . $client->name,
            'contact' => $contact,
            'contact_for_item' => $contact_for_item,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Client                              $client
     * @param  \App\ClientContact                       $contact
     * @param  \App\Http\Requests\ClientContactsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(Client $client, ClientContact $contact, ClientContactsRequest $request)
    {
        abort_unless(
            auth()->user()->isAdmin() ||
            auth()->id() === $client->user_id,
            403
        );

        $contact->update([
            'name' => $request['name'],
            'phone' => $request['phone'],
            'mobile' => $request['mobile'],
            'email' => $request['email'],
            'roles' => collect($request['roles'])->filter(function ($role) {
                return $role['checked'];
            })->pluck('name')->toArray(),
        ]);

        $divisions = collect($request['divisions'])->filter(function ($division) {
            return isset($division['checked']) && $division['checked'] === true;
        })->pluck('id');

        if ($client->divisions()->whereName(Division::DEFAULT)->exists()) {
            $default_division = $client->divisions()->whereName(Division::DEFAULT)->first();
            $divisions = $divisions->push($default_division['id']);
        }

        $divisions = $divisions->filter()->all();

        $contact->divisions()->sync($divisions);

        return response(['message' => __('Contact updated.')], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client        $client
     * @param  \App\ClientContact $contact
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client, ClientContact $contact)
    {
        abort_unless(
            auth()->user()->isAdmin() ||
            auth()->id() === $client->user_id,
            403
        );

        $contact->delete();

        return response(['message' => __('Contact deleted'), 200]);
    }
}
