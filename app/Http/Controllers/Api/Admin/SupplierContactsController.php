<?php

namespace App\Http\Controllers\Api\Admin;

use App\Supplier;
use App\SupplierContact;
use App\Http\Controllers\Controller;
use App\Http\Requests\SupplierContactsRequest;
use App\Http\Resources\SupplierContactResource;


class SupplierContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Supplier $supplier
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Supplier $supplier)
    {
        return SupplierContactResource::collection($supplier->contacts()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Supplier                              $supplier
     * @param  \App\Http\Requests\SupplierContactsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Supplier $supplier, SupplierContactsRequest $request)
    {
        abort_unless(
            auth()->user()->isAdmin() ||
            auth()->id() === $supplier['user_id'],
            403
        );

        $supplier->contacts()->create([
            'name' => $request['name'],
            'phone' => $request['phone'],
            'mobile' => $request['mobile'],
            'email' => $request['email'],
            'roles' => collect($request['roles'])->filter->checked->keys()->all(),
        ]);

        return response(['message' => __('Contact created for ') . $supplier->name], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Supplier                              $supplier
     * @param  \App\SupplierContact                       $contact
     * @param  \App\Http\Requests\SupplierContactsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(Supplier $supplier, SupplierContact $contact, SupplierContactsRequest $request)
    {
        abort_unless(
            auth()->user()->isAdmin() ||
            auth()->id() === $supplier['user_id'],
            403
        );

        $contact->update([
            'name' => $request['name'],
            'phone' => $request['phone'],
            'mobile' => $request['mobile'],
            'email' => $request['email'],
            'roles' => collect($request['roles'])->filter(function ($role) {
                return $role['checked'];
            })->pluck('name')->toArray(),
        ]);

        return response(['message' => __('Contact updated.')], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Supplier        $supplier
     * @param  \App\SupplierContact $contact
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier, SupplierContact $contact)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $contact->delete();

        return response(['message' => __('Contact deleted')], 200);
    }
}
