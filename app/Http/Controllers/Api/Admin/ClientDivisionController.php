<?php

namespace App\Http\Controllers\Api\Admin;

use App\Client;
use App\Division;
use App\Http\Controllers\Controller;
use App\Http\Resources\ClientDivisionResource;
use Illuminate\Http\Request;

class ClientDivisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Client $client
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Client $client)
    {
        return ClientDivisionResource::collection($client->divisions()->where('name', '!=', Division::DEFAULT)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Client              $client
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Client $client, Request $request)
    {
        abort_unless(
            auth()->user()->isAdmin() ||
            auth()->user()->isClient() ||
            auth()->id() === $client->user_id,
            403
        );

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
        ]);

        $division = $client->divisions()->create([
            'name' => $request['name'],
        ]);

        return response([
            'message' => __("New division added for ") . $client->name,
            'division' => $division,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Client              $client
     * @param  \App\Division            $division
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Client $client, Division $division, Request $request)
    {
        abort_unless(
            auth()->user()->isAdmin() ||
            $client->user_id === auth()->id(),
            403
        );

        $request->validate([
            'name' => ['required', 'string'],
        ]);

        $division->update([
            'name' => $request['name'],
        ]);

        return response(['message' => __('Division updated successfully.'), 200]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client   $client
     * @param  \App\Division $division
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client, Division $division)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $division->delete();

        return response(['message' => __('Division deleted.')], 200);
    }
}
