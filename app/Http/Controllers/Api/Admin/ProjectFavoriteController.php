<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Project;
use Illuminate\Http\Request;

class ProjectFavoriteController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Project             $project
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Project $project)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $project->toggleFavorite();

        return response(200);
    }
}
