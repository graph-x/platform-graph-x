<?php

namespace App\Http\Controllers\Api;

use App\Events\RequireEstimates;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProjectItems;
use App\Http\Resources\ItemResource;
use App\Item;
use App\Project;
use App\Quantity;
use App\Quote;
use App\Events\CheckOptionGroups;
use App\Events\ItemUpdated;
use App\Events\ItemCreated;
use App\OptionGroup;

class ApiProjectItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Project $project)
    {
        return ItemResource::collection($project->projectItems);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Project                         $project
     * @param  \App\Http\Requests\StoreProjectItems $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, StoreProjectItems $request)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        if (empty($request['items']) && $project->hasItems()) {
            $project->projectItems()->delete();

            return response(['message' => __('Project items removed successfully.')], 200);
        }

        if (empty($request['items']) && !$project->hasItems()) {
            return response(['message' => __('Nothing to do here.')], 200);
        }

        $items = $this->clearLooseItems($project, $request);

        // Create or Update item
        $items->each(function ($item) use ($project) {
            $existingItem = Item::whereId($item['id'])->first();

            if (is_null($existingItem)) {
                $createdItem = $this->createNew($item, $project);

                // Assign default option group without OPTIONS.
                $createdItem->fresh()->optionGroups()->create([
                    'project_id' => $createdItem->project_id,
                    'options' => json_encode([]),
                    'type' => OptionGroup::DEFAULT
                ]);

                foreach ($item['option_groups'] as $group) {
                    $optionGroup = $createdItem->fresh()->optionGroups()->create([
                        'project_id' => $createdItem->project_id,
                        'options' => json_encode($group['options']),
                    ]);
                }

                return ItemCreated::dispatch($createdItem->fresh());
            }

            $this->handleOptionGroupsOnUpdate($item, $existingItem, $project);

            $this->updateExisting($existingItem, $item, $project);

            ItemUpdated::dispatch($existingItem->fresh());
        });

        // Attach suppliers to project (supplier_project table)
        $this->syncAssignedSuppliers($items, $project);

        $responseMessage = 'Items saved successfully.';

        // Warn the user if suppliers without contacts (and if his contact has not estimator role)
        if ($request['notifySuppliers']) {
            $suppliers = $project->suppliers->map(function ($supplier) {
                return [
                    'supplier_name' => $supplier['name'],
                    'supplier_id' => $supplier['id'],
                    'contacts_roles' => $supplier->contacts()->pluck('roles')->flatten()->all()
                ];
            });

            $suppliersWithoutEstimator = $suppliers->filter(function ($supplier) {
                return empty($supplier['contacts_roles']) || ! in_array('estimator', $supplier['contacts_roles']);
            });

            if ($suppliersWithoutEstimator->count()) {
                $warning = $suppliersWithoutEstimator->map(function ($item) {
                    return ['name' => $item['supplier_name']];
                })->implode('name', ', ');

                $warning .= " don't have contacts with estimator role. Please create an estimator role for supplier before trying to notify them.";

                return response(['message' => $warning], 200);
            }
        }

        if ($request['notifySuppliers'] && $project->fresh()->hasUnnotifiedSuppliers) {
            RequireEstimates::dispatch($project);

            $project->update([
                'quote_requests_sent_at' => now(),
                'status' => 'awaiting_supplier_prices'
            ]);

            $responseMessage = 'Items saved and emails dispatched.';
        }

        return response(['message' => $responseMessage], 200);
    }

    /**
     * @param $item
     * @return array
     */
    private function getArr($item): array
    {
        if (! is_null($item['parent_item_id'])) {
            $id = Item::whereId($item['parent_item_id'])
                    ->orWhere('hash_id', $item['parent_item_hash_id'])
                    ->first()
                    ->id;
        }
        return [
            'name' => explode("\n", $item['description'])[0],
            'hash_id' => $item['hash_id'],
            'is_linked' => $item['is_linked'],
            'parent_item_id' => $id ?? null,
            'description' => $item['description'],
            'options' => json_encode($item['options']),
            'delivery_deadline' => $item['delivery_deadline'],
            'address_id' => $item['deliveryAddress']['value'],
            'delivery_contact_id' => $item['deliveryContact']['value'],
        ];
    }

    /**
     *  If some project items are present in DB, but are not present in request items data
     *  that means that the user deleted them on the front end - so we remove them from database.
     *
     * @param  \App\Project                         $project
     * @param  \App\Http\Requests\StoreProjectItems $request
     * @return \Illuminate\Support\Collection
     */
    private function clearLooseItems(Project $project, StoreProjectItems $request): \Illuminate\Support\Collection
    {
        $items = collect($request['items']);

        foreach ($project->projectItems as $item) {
            if (!$items->pluck('id')->contains($item->id)) {
                // if item is parent, get their children and remove them.
                if ($item->linked_items()->count()) {
                    $linked_items = $item->linked_items;
                    // Remove linked items from $request['items'] if there are any present.
                    foreach ($linked_items as $linked_item) {
                        $items = $items->reject(function ($i) use ($linked_item) {
                            Log::info($i, ['herere']);
                            return $i['id'] === $linked_item['id'];
                        });
                    }

                    $linked_items->each->delete();
                }

                $item->delete();
            }
        }

        return $items;
    }

    /**
     * If there are new quantities present in the item create them,
     * otherwise just update the old ones.
     *
     * @param $item
     * @return \Illuminate\Support\Collection
     */
    private function updateOrCreateQuantities($item): \Illuminate\Support\Collection
    {
        $quantities = collect();

        collect($item['quantities'])->each(function ($quantity) use ($quantities) {
            $quantity = Quantity::updateOrCreate([
                'value' => $quantity['value'],
            ]);

            $quantities->push($quantity);
        });

        return $quantities;
    }

    /**
     * @param               $item
     * @param  \App\Project $project
     */
    private function createNew($item, $project)
    {
        $createdItem = $project->projectItems()->create($this->getArr($item));

        $quantities = $this->updateOrCreateQuantities($item);

        $createdItem->quantities()->sync($quantities->pluck('id'));


        // If item is linked
        if ($item['is_linked']) {
            $parent_item = Item::whereId($item['parent_item_id'])->orWhere('hash_id', $item['parent_item_hash_id'])->first();

            $parent_item->suppliers->each(function ($supplier) use ($createdItem) {
                $createdItem->suppliers()->attach([
                    $supplier['id'] => ['item_data' => json_encode($createdItem->quantities)],
                ]);
            });

            return $createdItem;;
        }

        collect($item['suppliers'])->each(function ($supplier) use ($createdItem) {
            // Here we attach the item and assigned suppliers, along with the item quantities,
            // and quote_data that the supplier will update.
            $createdItem->suppliers()->attach([
                $supplier['id'] => ['item_data' => json_encode($createdItem->quantities)],
            ]);
        });

        return $createdItem;
    }

    /**
     * @param $existingItem
     * @param $item
     */
    private function updateExisting($existingItem, $item): void
    {
        $existingItem->update($this->getArr($item));

        $quantities = $this->updateOrCreateQuantities($item);

        $existingItem->quantities()->sync($quantities->pluck('id'));

        if ($item['is_linked']) {
            $parent_item = Item::whereId($item['parent_item_id'])->orWhere('hash_id', $item['parent_item_hash_id'])->first();
            $existingItem->suppliers()->sync($parent_item->suppliers->pluck('id'));

            collect($parent_item->suppliers)->each(function ($supplier) use ($existingItem) {
                $quantities = $existingItem->quantities;

                $existingItem->suppliers()->updateExistingPivot(
                    $supplier['id'],
                    ['item_data' => json_encode($quantities)]
                );
            });

            return;
        }

        $supplierIds = collect($item['suppliers'])->pluck('id');

        $existingItem->suppliers()->sync($supplierIds->toArray());

        // Here we sync the item and assigned suppliers, along with the item quantities
        // We then compare the new quantity and the old (quote) data,
        // and if they are not the same we sync them.
        collect($item['suppliers'])->each(function ($supplier) use ($existingItem) {
            $quantities = $existingItem->quantities;

            $existingItem->suppliers()->updateExistingPivot(
                $supplier['id'],
                ['item_data' => json_encode($quantities)]
            );
        });
    }

    private function syncAssignedSuppliers($items, $project)
    {
        $assignedSuppliers = $items->flatMap(function ($item) {
            return collect($item['suppliers'])->pluck('id');
        })->unique()->toArray();

        $project->suppliers()->sync($assignedSuppliers);
    }

    private function handleOptionGroupsOnUpdate($item, $existingItem, $project)
    {
        if (empty($item['option_groups'])) {
            // Assign option group without OPTIONS.
            $existingItem->fresh()->optionGroups()->create([
                'project_id' => $project->id,
                'options' => json_encode([]),
                'type' => OptionGroup::DEFAULT
            ]);

            // clean up, remove old groups
            if ($existingItem->fresh()->optionGroups()->exists()) {
                $existingItem->fresh()->optionGroups->each->delete();
            }

            return;
        }

        $optionGroups = $this->clearLooseOptionGroups($item['option_groups'], $existingItem);

        foreach ($optionGroups as $group) {
            $existingItem->fresh()->optionGroups()->updateOrCreate(
                ['id' => $group['id'], 'project_id' => $project->id],
                ['options' => json_encode($group['options'])]
            );
        }
    }

    private function clearLooseOptionGroups($option_groups,$existingItem)
    {
        $option_groups_from_request = collect($option_groups);

        foreach ($existingItem->optionGroups as $group) {
            if (!$option_groups_from_request->pluck('id')->contains($group->id)) {
                $group->delete();
            }
        }

        return $option_groups_from_request;
    }
}
