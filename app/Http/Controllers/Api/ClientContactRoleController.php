<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\ClientContactRoleResource;
use App\Client;

class ClientContactRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Client $client, $role)
    {
        return ClientContactRoleResource::collection($client->contactsWithRole($role));
    }
}
