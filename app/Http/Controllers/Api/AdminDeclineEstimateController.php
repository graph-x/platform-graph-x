<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Project;
use App\Quote;

class AdminDeclineEstimateController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Quote $quote, Request $request)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        // if ($project->choose_estimates_automatically) {
        //     return response(['message' => __('You cannot do this, automatic choosing of estimates is chosen.')]);
        // }

        if ($quote->declined_at !== null) {
            return response([
                'message' => 'Quote is already declined.',
                'quote' => $quote->fresh()
            ]);
        }

        $quote->update([
            'declined_at' => now(),
            'approved_at' => null,
        ]);

        if ($quote->invoice_line_item()->exists()) {
            $quote->invoice_line_item->delete();
        }

        return response([
            'message' => "Quote submission declined",
            'quote' => $quote->fresh()
        ], 200);
    }
}
