<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Project;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function clientDivisions(Request $request, $client)
    {
        return $client->divisions()->orderBy('name')->get()->map(function ($division) {
            return [
                'value' => $division->id,
                'display' => $division->name,
            ];
        })->values();
    }

    public function clientAddresses($client, $type = false)
    {
        return $client->addresses()->orderBy('address_line_1')->get()->filter(function ($address) use ($type) {
            if (!empty($type)) {
                return in_array($type, $address->types);
            } else {
                return true;
            }
        })->map(function ($address) {
            return [
                'value' => $address->id,
                'display' => $address->address_line_1 . (!empty($address->address_line_2) ? ', ' . $address->address_line_2 : '') . ', ' . $address->city . ', ' . $address->province . ', ' . $address->country . ', ' . $address->postal_code,
            ];
        })->values();
    }

    /**
     * Get the contact with buyer role assigned to a project.
     *
     * @param
     * @return
     */
    public function projectDivisionBuyer(Project $project)
    {
        $buyer = $project->division->contacts()->orderBy('name')->get()->filter(function ($contact) {
            return in_array('buyer', $contact->roles);
        })->first();

        return $buyer;
    }

    public function divisionContacts($division, $role = false)
    {
        return $division->contacts()->orderBy('name')->get()->filter(function ($contact) use ($role) {
            if (!empty($role)) {
                return in_array($role, $contact->roles);
            } else {
                return true;
            }
        })->map(function ($contact) {
            return [
                'value' => $contact->id,
                'display' => $contact->name,
            ];
        })->values();
    }
}