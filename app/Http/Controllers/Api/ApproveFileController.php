<?php

namespace App\Http\Controllers\Api;

use App\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApproveFileController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\File                $file
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(File $file, Request $request)
    {
        $file->approve();

        return response(['message' => "File approved."], 200);
    }
}
