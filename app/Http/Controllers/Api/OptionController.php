<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Option::toQueryBuilderRules(false);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $validated = $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'type' => ['required'],
            'values' => ['required', 'string', 'max:255'],
        ]);

        $createdOption = Option::create([
            'title' => $validated['title'],
            'type' => $validated['type']['value'],
            'values' => $validated['values'],
        ]);

        /** Return the transformed newly created option, so the frontend dropdown can be automatically populated */
        $options = Option::toQueryBuilderRules(false);
        $formattedOption = collect($options)->filter(function ($option) use ($createdOption) {
            return $option['id'] === Str::slug($createdOption->title, '_');
        })->first();

        return response([
            'message' => 'Option created.',
            'option' => $formattedOption,
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}