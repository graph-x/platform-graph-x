<?php

namespace App\Http\Controllers\Api;

use App\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \App\Supplier[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection
     */
    public function index()
    {
        return Supplier::all()->map(function ($supplier) {
            return [
                'id' => $supplier->id,
                'name' => $supplier->name,
            ];
        });
    }
}
