<?php

namespace App\Http\Controllers\Api;

use App\Division;
use App\Http\Controllers\Controller;
use App\Http\Resources\DivisionContactRoleResource;

class DivisionContactRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Division $division, $role)
    {
        return DivisionContactRoleResource::collection($division->contactsWithRole($role));
    }
}