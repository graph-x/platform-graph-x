<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Project;
use Illuminate\Http\Request;

class ProjectStatusController extends Controller
{
    /**
     * Update the status of the specified project.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $project->update(['status' => $request['status']['key']]);

        return response(['message' => 'Status updated!', 'project' => $project], 200);
    }
}