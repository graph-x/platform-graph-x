<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Item;
use App\Message;
use App\Project;
use App\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectItemMessagesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Project             $project
     * @param  \App\Supplier            $supplier
     * @param  \App\Item                $item
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Supplier $supplier, Item $item, Request $request)
    {
        $request->validate([
            'message' => ['required', 'string'],
        ]);

        $message = Message::create([
            'project_id' => $project->id,
            'supplier_id' => $supplier->id,
            'item_id' => $item->id,
            'user_id' => Auth::user()->id,
            'option_group_id' => $request['option_group_id'] ?? null,
            'quote_id' => $request['quote_id'] ?? null,
            'text' => $request['message'],
            'tab' => $request['from_tab'] ?? Message::SUPPLIER_TAB,
        ]);

        return response([
            'message' => 'Message saved.', 'msg_object' => $message,
            'message_object' => $message,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Project             $project
     * @param  \App\Supplier            $supplier
     * @param  \App\Item                $item
     * @param  \App\Message             $message
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project, Supplier $supplier, Item $item, Message $message, Request $request)
    {
        abort_unless(auth()->user()->isAdmin() || auth()->id() === $message['user_id'], 403);

        $message->update([
            'text' => $request['text'] ?? '',
        ]);

        return response(['message' => "Message mis à jour."], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project             $project
     * @param  \App\Supplier            $supplier
     * @param  \App\Item                $item
     * @param  \App\Message             $message
     * @param  \Illuminate\Http\Request $request
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project, Supplier $supplier, Item $item, Message $message, Request $request)
    {
        abort_unless(auth()->user()->isAdmin() || auth()->id() === $message['user_id'], 403);

        $message->delete();

        return response(['message' => "Message supprimé."], 200);
    }
}
