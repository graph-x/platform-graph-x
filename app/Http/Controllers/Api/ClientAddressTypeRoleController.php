<?php

namespace App\Http\Controllers\Api;

use App\Client;
use App\Http\Controllers\Controller;
use App\Http\Resources\ClientAddressTypeResource;

class ClientAddressTypeRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Client $client, $type)
    {
        return ClientAddressTypeResource::collection($client->addressWithType($type));
    }
}