<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use App\Filters\ProjectFilters;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Status;
use App\Client;
use App\ProjectManagementFee;
use App\Invoice;

class ApiProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request    $request
     * @param  \App\Filters\ProjectFilters $filters
     * @return \App\Project[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Http\Response
     */
    public function index(Request $request, ProjectFilters $filters)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $projects = Project::filter($filters)->get()->sortBy('client.name');

        if ($request->name === 'desc') {
            $projects = $projects->sortByDesc('client.name');
        };

        return $projects->values()->toArray();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $request->validate([
            'title' => ['required', 'max:100'],
            'short_description' => ['required', 'max:255'],
            'client' => ['required'],
            'division' => ['nullable'],
            'buyer' => ['required'],
            'accounting' => ['required'],
            'require_physical' => ['nullable', 'boolean'],
            'physical_approval' => ['nullable'],
            'physical_address' => ['nullable'],
            'start_date' => ['required', 'date'],
            'end_date' => ['required', 'date'],
            'po_number' => ['required', 'max:20'],
            'estimates_end_date' => ['nullable', 'date'],
            'choose_estimates_automatically' => ['nullable', 'boolean'],
        ]);

        $project = Project::create([
            'unique_code' => Str::upper(Str::substr(Str::orderedUuid(), 0, 20)),
            'title' => $request['title'],
            'short_description' => $request['short_description'],
            'client_id' => $request['client']['id'],
            'division_id' => $request['division']['id'] ?? Client::firstWhere('id', $request['client']['id'])->default_division()->id, // maybe attach default here?
            'address_id' => $request['physical_address'] ? $request['physical_address']['id'] : null,
            'start' => Carbon::parse($request['start_date']),
            'end' => Carbon::parse($request['end_date']),
            'po_number' => $request['po_number'],
            'require_pdf' => $request['requires_pdf_approval'],
            'require_physical' => $request['requires_physical_approval'],
            'status' => Status::QUOTE_REQUESTS_TO_BE_MADE,
            'parent_id' => $request['parent_project'],
            'estimates_end_date' => $request['estimates_end_date'] ?? now()->addDays(Project::ESTIMATION_PERIOD_DAYS),
            'choose_estimates_automatically' => $request['choose_estimates_automatically']
        ]);

        $unique_invoice_id = Str::upper(Str::substr(Str::orderedUuid(), 0, 20));

        $invoice = Invoice::create([
            'unique_code' => "IN-{$unique_invoice_id}",
            'project_id' => $project->id,
            'client_id' => $project->client->id,
            'client_name' => $project->client->name,
            'title' => "Invoice for {$project->title}",
            'description' => $project->short_description,
            'due_date' => now()->addMonth(),
            'paid' => false,
            'status' => Invoice::PRE_INVOICE, // ['open', 'paid', 'draft', 'pre_invoice']
            'currency' => Invoice::DEFAULT_CURRENCY,
            'tps_tax_rate' => Invoice::TPS_TAX_RATE, // in % TPS - taxes Goods and services tax 5%
            'tvq_tax_rate' => Invoice::TVQ_TAX_RATE, // in % TVQ, - taxes  9.975%
            'subtotal' => 0, // SOUS-TOTAL
            'tps_amount' => 0, // TPS amount should be calculated automatically from the line items
            'tvq_amount' => 0, // TVQ amount should be calculated from the line items
            'total_amount' => 0, // need to sum all of the InvoiceLineItems.
            'profit' => 0, // total profit includes project management fee + profit per item?
        ]);

        ProjectManagementFee::create([
            'invoice_id' => $invoice['id'],
            'project_id' => $project['id'],
            'fee' => ProjectManagementFee::DEFAULT_FEE,
            'title' => ProjectManagementFee::DEFAULT_TITLE,
            'description' => ProjectManagementFee::DEFAULT_DESCRIPTION,
        ]);

        // Attach buyer role
        $project->contacts()->attach($request['buyer']['id'], [
            'role' => 'buyer',
        ]);

        // Attach accounting role
        $project->contacts()->attach($request['accounting']['id'], [
            'role' => 'accounting',
        ]);

        // Attach physical_approval role
        if ($request['requires_physical_approval'] && $request['physical_approval']) {
            $project->contacts()->attach($request['physical_approval']['id'], [
                'role' => 'physical_approval',
            ]);
        }

        return response([
            'message' => __('Project created.'),
            'redirect_route' => route('projects-client.show', [
                'project' => $project->fresh()->load('management_fee'),
                'client' => $project->fresh()->client,
            ]),
        ], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        abort_unless(
            auth()->user()->isAdmin() ||
            auth()->id() === $request['client']['user_id'],
            403
        );

        $request->validate([
            'title' => ['required', 'max:100'],
            'short_description' => ['required', 'max:255'],
            'client' => ['required'],
            'division' => ['required'],
            'buyer' => ['required'],
            'accounting' => ['required'],
            'requires_physical_approval' => ['boolean', 'nullable'],
            'physical_approval' => ['required_if:requires_physical_approval, 1', 'nullable'],
            'physical_address' => ['required_if:requires_physical_approval, 1', 'nullable'],
            'start_date' => ['required', 'date'],
            'end_date' => ['required', 'date'],
            'po_number' => ['required', 'max:20'],
            'estimates_end_date' => ['nullable', 'date'],
            'choose_estimates_automatically' => ['boolean'],
            'notes' => ['nullable', 'string', 'max:500']
        ]);

        $project->update([
            'title' => $request['title'],
            'short_description' => $request['short_description'],
            'client_id' => $request['client']['id'],
            'division_id' => $request['division']['id'],
            'address_id' => $request['requires_physical_approval'] ? $request['physical_address']['id'] : null,
            'start' => Carbon::parse($request['start_date']),
            'end' => Carbon::parse($request['end_date']),
            'po_number' => $request['po_number'],
            'require_pdf' => $request['requires_pdf_approval'],
            'require_physical' => $request['requires_physical_approval'],
            'status' => $request['status']['key'] ?? $project->status,
            'notes' => $request['notes'],
            'estimates_end_date' => $request['estimates_end_date'],
            'choose_estimates_automatically' => $request['choose_estimates_automatically']
        ]);

        // Attach buyer role
        $project->belongsToMany('App\ClientContact')->wherePivot('role', 'buyer')->detach();
        $project->contacts()->attach($request['buyer']['id'], [
            'role' => 'buyer',
        ]);

        // Attach accounting role
        $project->belongsToMany('App\ClientContact')->wherePivot('role', 'accounting')->detach();
        $project->contacts()->attach($request['accounting']['id'], [
            'role' => 'accounting',
        ]);

        // Attach physical_approval role

        if ($request['requires_physical_approval'] === true && $request['physical_approval']) {
            $project->belongsToMany('App\ClientContact')->wherePivot('role', 'physical_approval')->detach();
            $project->contacts()->attach($request['physical_approval']['id'], [
                'role' => 'physical_approval',
            ]);
        }

        if ($request['requires_physical_approval'] === false) {
            $project->belongsToMany('App\ClientContact')->wherePivot('role', 'physical_approval')->detach();
        }

        return response(['message' => __('Project updated.')], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project $project
     * @throws \Exception
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        abort_unless(Auth::user()->isAdmin(), 403);

        $project->delete();

        return response([
            'message' => __('Project deleted.'),
            'redirect_route' => route('projects.index'),
        ], 200);
    }
}
