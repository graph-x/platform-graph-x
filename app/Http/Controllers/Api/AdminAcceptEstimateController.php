<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Project;
use App\Quote;
use Illuminate\Support\Facades\Mail;
use App\Mail\EstimateApproved;
use App\InvoiceLineItem;

class AdminAcceptEstimateController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Quote $quote, Request $request)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        if ($project->choose_estimates_automatically) {
            return response(['message' => __('You cannot do this, automatic choosing of estimates is chosen.')]);
        }

        if (Quote::query()
            ->where('item_id', $quote['item_id'])
            ->where('option_group_id', $quote['option_group_id'])
            ->where('quantity_id', $quote['quantity_id'])
            ->where('approved_at', '!=', null)->exists()) {
            return response([
                'message' => 'A quote has already been approved for this item (item group)',
                'quote' => $quote->fresh()
            ], 200);
        }

        if ($quote->approved_at !== null) {
            return response([
                'message' => 'Quote is already approved.',
                'quote' => $quote->fresh()
            ], 200);
        }

        $quote->update([
            'approved_at' => now(),
            'declined_at' => null,
        ]);

        InvoiceLineItem::create([
            'invoice_id' => $quote->project->invoice->id,
            'project_id' => $quote->project_id,
            'quote_id' => $quote->id,
            'item_id' => $quote->item_id,
            'supplier_id' => $quote->supplier_id, // because we need to get the approved/submission price.
            'supplier_name' => $quote->supplier->name,
            'title' => $quote->item->name . " " . $quote->quantity->value . "pts",
            'description' => $quote->item->description,
            'quantity' => $quote->quantity->value,
            'approved_price' => $quote->value, // PRIX APROUVE
            'cost_price' => $quote->value, // PRIX COÙTANT,
            'customer_price' => 0,
            'delivery_date' => $quote->item->delivery_deadline,
            'profit_type' => InvoiceLineItem::FIXED_AMOUNT_PROFIT,
            'admin_profit' => 0, // BÉNÉFICE PER ITEM / Profit per item for admin,
            'custom_made' => false // created from +
        ]);

        $supplier = $quote->supplier;
        Mail::to($supplier->user->email)->queue(new EstimateApproved($supplier, $quote->item));

        return response([
            'message' => "Quote submission approved for {$supplier->name}.",
            'quote' => $quote->fresh()
        ], 200);
    }
}
