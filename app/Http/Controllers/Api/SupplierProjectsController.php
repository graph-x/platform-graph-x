<?php

namespace App\Http\Controllers\Api;

use App\Project;
use App\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SupplierProjectsController extends Controller
{
    /**
     * Get the project items assigned to supplier.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Project             $project
     * @return mixed
     */
    public function assignedItems(Request $request, Project $project)
    {
        $supplier = Supplier::where('user_id', $request->user()->id)->first();

        return json_decode($supplier->projects()->firstWhere('project_id', $project->id)->items);
    }

    /**
     * Store the quotes for the items assigned to supplier.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Project             $project
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function quotes(Request $request, Project $project)
    {
        $supplier = Supplier::where('user_id', $request->user()->id)->first();

        $supplier->projects()->detach($project);
        $supplier->projects()->attach($project, ['items' => json_encode($request->items)]);

        return response(__('Success.'), 200);
    }
}
