<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\EpreuvesResource;
use App\Project;
use Illuminate\Http\Request;
use App\Http\Resources\ApprovedQuotesForEpreuvesTabResource;

class ApprovedItemController extends Controller
{
    /**
     * Display a listing of the resource.
     * Items to show in the Epreuves tab.
     *
     * @param  \App\Project             $project
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Project $project, Request $request)
    {
        return ApprovedQuotesForEpreuvesTabResource::collection($project->approvedQuotes());

        // return EpreuvesResource::collection($project->approvedQuotes());
        // return EpreuvesResource::collection($project->approvedItems());
    }
}
