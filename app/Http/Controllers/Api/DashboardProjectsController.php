<?php

namespace App\Http\Controllers\Api;

use App\Filters\ProjectFilters;
use App\Http\Controllers\Controller;
use App\Project;
use App\Status;
use Illuminate\Http\Request;

class DashboardProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Filters\ProjectFilters $filters
     * @return \Illuminate\Support\Collection
     */
    public function index(Request $request, ProjectFilters $filters)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $projects = $this->getProjects($request, $filters);

        return Status::all()->map(function ($status) use ($projects, $request) {
            /** Assign projects to status group based on their status (this is to show in a single card) */
            $filteredProjects = $projects->filter(function ($project) use ($status) {
                return $project->status === $status['key'];
            })->sortBy(function ($project) {
                return [!$project->favorite, $project->client->name];
            });

            if ($request->name === 'desc') {
                $filteredProjects = $filteredProjects->sortByDesc(function ($project) {
                    return [$project->favorite, $project->client->name];
                });
            }

            return [
                'id' => $status->id,
                'key' => $status->key,
                'html_label' => $status->html_label,
                'short_label' => $status->short_label,
                'hex_color' => $status->hex_color,
                'projects' => $filteredProjects->values()->toArray(),
            ];
        });
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Project             $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        $project->update([
            'status' => $request['status'],
        ]);

        if ($project->hasChildren()) {
            $project->children->each->update(['status' => $request['status']]);
        }

        return response(['message' => __('Project status updated.')], 200);
    }

    private function getProjects($request, $filters)
    {
        return Project::filter($filters)
            ->parentsOnly()
            ->with('children')
            ->get();
    }
}