<?php

namespace App\Http\Controllers\Admin;

use App\Item;
use App\Project;
use App\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\EstimateApproved;
use Illuminate\Support\Facades\Mail;

class AcceptSubmissionController extends Controller
{
    /**
     * @param  \App\Project             $project
     * @param  \App\Supplier            $supplier
     * @param  \App\Item                $item
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Supplier $supplier, Item $item, Request $request)
    {
        abort_unless(auth()->user()->isAdmin(), 403);

        if ($project->choose_estimates_automatically) {
            return response(['message' => __('You cannot do this, automatic choosing of estimates is chosen.')]);
        }

        if ($item->suppliers()->wherePivot('quotes_approved_at', '!=', null)->exists()) {
            return response(['message' => 'Quotes are already approved for this item.']);
        }

        if ($supplier->items()->where('item_id', $item['id'])->first()->pivot->quotes_approved_at !== null) {
            return response(['message' => "Quotes are already approved."], 200);
        }

        $supplier->items()->where('item_id', $item['id'])->updateExistingPivot($item['id'], [
            'quotes_declined_at' => null,
            'quotes_approved_at' => now(),
        ]);

        $supplier->quotes()
            ->where('item_id', $item['id'])
            ->get()
            ->each(function ($quote) {
                $quote->update([
                    'approved_at' => now()
                ]);
            });


        // Update linked items of the project if there are any.
        if ($item->has_linked_items()) {
            $item->linked_items()->each(function ($i) use ($supplier) {
                $supplier->items()->where('item_id', $i->id)->updateExistingPivot($i->id, [
                    'quotes_declined_at' => null,
                    'quotes_approved_at' => now(),
                ]);

                // Update linked items quotes table
                $supplier->quotes()
                    ->where('item_id', $i->id)
                    ->get()
                    ->each(function ($quote) {
                        $quote->update([
                            'approved_at' => now()
                        ]);
                    });
            });
        }

        // Notify supplier that his estimate has been approved.
        Mail::to($supplier->user->email)->queue(new EstimateApproved($supplier, $item));

        return response(['message' => "Quote submission approved for {$supplier->name}."], 200);
    }
}
