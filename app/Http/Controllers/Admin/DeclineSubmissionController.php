<?php

namespace App\Http\Controllers\Admin;

use App\Item;
use App\Project;
use App\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeclineSubmissionController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Project             $project
     * @param  \App\Supplier            $supplier
     * @param  \App\Item                $item
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, Supplier $supplier, Item $item, Request $request)
    {
        abort_unless(
            auth()->user()->isAdmin() ||
            auth()->id() === $project->client->user_id,
            403
        );

        if ($project->choose_estimates_automatically) {
            return response(['message' => __('You cannot do this, automatic choosing of estimates is chosen.')]);
        }

        if ($supplier->items()->where('item_id', $item['id'])->first()->pivot->quotes_declined_at !== null) {
            return response(['message' => "Quotes are already declined for {$supplier->name}."], 200);
        }

        $supplier->items()->where('item_id', $item['id'])->updateExistingPivot($item['id'], [
            'quotes_declined_at' => now(),
            'quotes_approved_at' => null,
        ]);


        $supplier->quotes()
            ->where('item_id', $item['id'])
            ->get()
            ->each(function ($quote) {
                $quote->update([
                    'approved_at' => null
                ]);
            });


        if ($item->has_linked_items()) {
            $item->linked_items()->each(function ($i) use ($supplier) {
                $supplier->items()->where('item_id', $i->id)->updateExistingPivot($i->id, [
                    'quotes_declined_at' => now(),
                    'quotes_approved_at' => null,
                ]);

                $supplier->quotes()
                    ->where('item_id', $i->id)
                    ->get()
                    ->each(function ($quote) {
                        $quote->update([
                            'approved_at' => null
                        ]);
                    });
            });
        }

        return response(['message' => "Quote submission declined for {$supplier->name}."], 200);
    }
}
