<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\AdminSoumissionTabResource;
use App\Project;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\ProjectInvoiceResource;

class AdminSoumissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project)
    {
        abort_unless(Auth::user()->isAdmin(), 403);

        return new ProjectInvoiceResource($project->invoice);

        // return [
        //     'project' => $project->load('management_fee'),
        //     'profit_types' => Project::$profit_types,
        //     'data' => AdminSoumissionTabResource::collection($project->quotesApprovedByAdmin())
        // ];
    }

    /**
     * Save submissions. (Send if notify client);
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project, Request $request)
    {
        abort_unless(Auth::user()->isAdmin(), 403);

        // Get all the line items
        $this->clearLooseItems($project, $request);

        $quotes_for_client = $request['preinvoiceData'];

        $project_management = $request['project_management'];

        $quotes_for_client->each(function ($quote) use ($project) {
            $existingItem = Quote::whereId($quote['id'])->first();

            if (is_null($existingItem)) {
                return $this->createNew($quote, $project);
            }

            return $this->updateExisting($existingItem, $quote, $project);
        });

        // And update the profit
        $project->management_fee()->update([
            'title' => $project_management['title'],
            'description' => $project_management['description'],
            'fee' => $project_management['fee'],
        ]);

        // $costCalculator = new InvoiceCostCalculator($invoice->fresh());
        // $subtotal = $costCalculator->subtotal();
        // $tps_amount = $costCalculator->tpsAmountFrom($subtotal);
        // $tvq_amount = $costCalculator->tvqAmountFrom($subtotal);
        // $profit = $costCalculator->profit();
        // $total_amount = $subtotal + $tps_amount + $tvq_amount;

        // // Update invoice
        // $invoice->update([
        //     'subtotal' =>  $subtotal,
        //     'tps_amount' => $tps_amount,
        //     'tvq_amount' => $tvq_amount,
        //     'profit' => $profit,
        //     'total_amount' => $total_amount,
        // ]);

        return response(['message' => __('Invoice updated.')], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function updateExisting($existingItem, $line_item, $invoice)
    {
        if ($existingItem->custom_made) {
            return $existingItem->update([
                'supplier_id' => $line_item['supplier_id'],
                'supplier_name' => $line_item['supplier_name'],
                'title' => explode("\n", $line_item['description'])[0],
                'description' => $line_item['description'],
                'quantity' => $line_item['quantity'],
                'approved_price' => $line_item['approved_price'],
                'cost_price' => $line_item['cost_price'],
                'customer_price' => $line_item['customer_price'],
                'delivery_date' => $line_item['delivery_date'],
                'profit_type' => $line_item['profit_type']['key'],
                'admin_profit' => $line_item['admin_profit'],
            ]);
        }

        $existingItem->update([
            'admin_profit' => number_format($line_item['admin_profit'], 2),
            'profit_type' => $line_item['profit_type']['key'],
            'customer_price' => $line_item['customer_price']
        ]);
    }

    /**
     *  If some project items are present in DB, but are not present in request items data
     *  that means that the user deleted them on the front end - so we remove them from database.
     *
     * @param  \App\Invoice                         $project
     * @param  \App\Http\Requests\UpdateInvoiceRequest $request
     */
    private function clearLooseItems($project, $request)
    {
        $quotes = collect($request['preinvoiceData'])->filter(function ($quote) {
            return $quote['custom_made'] === true;
        });

        if ($quotes->count()) {
            foreach ($project->quotes()->where('custom_made', true)->get() as $quote) {
                if (!$quotes->pluck('quote_id')->contains($quote->id)) {
                    $quote->delete();
                }
            }
        }
    }
}
