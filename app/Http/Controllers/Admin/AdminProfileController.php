<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\AdminUserResource;
use Illuminate\Support\Facades\Hash;


class AdminProfileController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Http\Resources\ClientResource|\Illuminate\View\View
     */
    public function show(Request $request)
    {
        abort_unless(Auth::user()->isAdmin(), 403);

        if (request()->wantsJson()) {
            return new AdminUserResource(Auth::user());
        }

        return view('admin.profile.show', [
            'admin' => new AdminUserResource(Auth::user()),
        ]);
    }

    /**
     * Update the admin.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        abort_unless(Auth::user()->isAdmin(), 403);

        $admin = Auth::user();

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,'.auth()->id()],
            'password' => ['sometimes', 'nullable', 'string', 'min:8', 'confirmed'],
        ]);

        $admin->update([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => is_null($request['password']) ? $admin->password : Hash::make($request['password'])
        ]);

        return response(['message' => 'Profil mis à jour.'], 200);
    }
}
