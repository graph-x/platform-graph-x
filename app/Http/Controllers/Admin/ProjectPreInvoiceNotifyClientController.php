<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Invoice;
use App\Project;
use Illuminate\Support\Facades\Mail;
use App\Mail\InvoiceGenerated;
use Illuminate\Support\Facades\Auth;
use App\Client;
use App\Mail\ClientSubmissionApprovalRequest;

class ProjectPreInvoiceNotifyClientController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Project             $project
     * @param  \App\Invoice             $invoice
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Project $project, Invoice $invoice)
    {
        abort_unless(Auth::user()->isAdmin(), 403);

        $client = $project->client;

        $invoicingRole = $client->contactsWithRole('invoicing')->first();

        $invoicing = $invoicingRole->exists() ? $invoicingRole->email : $client->user->email;

        Mail::to($invoicing)->send(new ClientSubmissionApprovalRequest($project, $invoice, $client));

        $invoice->notified_at = now();

        $invoice->save();

        $project->update([
            'status' => 'submissions_sent_awaiting_client_approval'
        ]);

        return response(['message' => __('Quote approval request sent.')], 200);
    }
}
