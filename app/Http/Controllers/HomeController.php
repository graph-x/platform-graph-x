<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index()
    {
        switch (Auth::user()->role) {
            case User::ADMIN:
                return redirect('/dashboard');
                break;
            case User::SUPPLIER:
                return redirect()->route('supplier.projects.index');
                break;
            case User::CLIENT:
                return redirect()->route('client.projects.index');
                break;
            case User::DEFAULT:
                Auth::logout();
                return response('No entity has been assigned to this user. Please assign an entity (Supplier or Client) before loging in as this user.');
                break;

            default:
                return redirect('/dashboard');
                break;
        }
    }
}
