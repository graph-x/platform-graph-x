<?php

namespace App\Http\Controllers;

use App\Client;
use App\Project;
use App\Status;

class ProjectsClientController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function show(Project $project)
    {
        $client = Client::with('divisions')->whereId($project->client->id)->first();
        $invoice_amount = $project->invoice()->exists() ? $project->invoice->total_amount : '0.00';
        $adminApprovedQuotesCount = $project->fresh()->quotes()->whereNotNull('approved_at')->count();

        return view('projects.show', [
            'project' => $project->load('division'),
            'client' => $client,
            'statuses' => Status::all(),
            'invoice_count' => $project->invoice()->count(),
            'invoice_amount' => $invoice_amount,
            'admin_approved_quotes_count' => $adminApprovedQuotesCount
        ]);
    }
}
