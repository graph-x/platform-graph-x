<?php

namespace App\Http\Controllers\Supplier;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreQuotesRequest;
use App\Project;
use App\Quote;
use App\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SupplierProjectItemsQuotesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Project                          $project
     * @param  \App\Http\Requests\StoreQuotesRequest $request
     * @return void
     */
    public function store(Project $project, StoreQuotesRequest $request)
    {
        abort_if($project->isInProduction(), 403, __('Project is in production, cannot submit estimates.'));

        abort_unless(Carbon::parse($project->estimates_end_date)->gte(now()), 403, __('Cannot submit estimates. Estimation date is over.'));

        $supplier = Supplier::where('user_id', Auth::user()->id)->first();

        collect($request['items'])->each(function ($i) use ($project, $supplier) {
            $quote = Quote::where('id', $i['quote_id'])->first();

            return $quote->update([
                'value' => (float) $i['value'],
                'submitted_at' => now(),
            ]);
        });

        // Soumission recu - oui
        $project->
            suppliers()
            ->where('supplier_id', $supplier->id)
            ->updateExistingPivot($supplier->id, ['quotes_submitted_at' => now()]);

        // Do other stuff here, notify admin of submissions, etc.
        return response(['message' => 'Soumissions enregistrées.'], 200);
    }
}
