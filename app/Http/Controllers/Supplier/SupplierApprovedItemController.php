<?php

namespace App\Http\Controllers\Supplier;

use App\Project;
use App\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Resources\EpreuvesResource;
use App\Http\Resources\ApprovedQuotesForEpreuvesTabResource;

class SupplierApprovedItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Supplier            $supplier
     * @param  \App\Project             $project
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Supplier $supplier, Project $project, Request $request)
    {
        abort_unless($supplier['user_id'] === Auth::id(), 403);

        return ApprovedQuotesForEpreuvesTabResource::collection($project->approvedQuotesForSupplier($supplier)->get());
        // return EpreuvesResource::collection($project->approvedItemsForSupplier($supplier));
    }
}
