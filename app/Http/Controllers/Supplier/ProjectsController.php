<?php

namespace App\Http\Controllers\Supplier;

use App\Client;
use App\Status;
use App\Project;
use App\Supplier;
use Illuminate\Http\Request;
use App\Filters\ProjectFilters;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request    $request
     * @param  \App\Filters\ProjectFilters $filters
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, ProjectFilters $filters)
    {
        if ($request->wantsJson()) {
            $projects = Project::query()
                ->filter($filters)
                ->belongingToSupplier(Auth::user())
                ->get()
                ->sortBy('client.name');
            return response($projects, 200);
        }

        return view('supplier.projects.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Project $project)
    {
        abort_if($project->suppliers()->where('user_id', Auth::user()->id)->count() === 0, 404);

        $client = Client::with('divisions')->whereId($project->client->id)->first();
        $supplier = Supplier::where('user_id', Auth::id())->firstOrFail();
        $approved_estimates_count = $project->approvedQuotesForSupplier($supplier)->count();

        return view('supplier.projects.show', [
            'project' => $project->load('division'),
            'client' => $client,
            'supplier' => $supplier,
            'statuses' => Status::all(),
            'approved_estimates_count' => $approved_estimates_count,
        ]);
    }
}
