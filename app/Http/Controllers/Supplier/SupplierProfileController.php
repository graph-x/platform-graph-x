<?php

namespace App\Http\Controllers\Supplier;

use App\Supplier;
use App\Rules\PhoneNumber;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\SupplierResource;

class SupplierProfileController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $supplier = Supplier::query()->where('user_id', Auth::id())->firstOrFail();

        if (request()->wantsJson()) {
            return new SupplierResource($supplier);
        }

        return view('supplier.profile.show', [
            'supplier' => new SupplierResource($supplier),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function update(Supplier $supplier, Request $request)
    {
        abort_unless(auth()->id() === $supplier['user_id'], 403);

        $validated = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'phone' => [new PhoneNumber],
            'address_line_1' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'province' => ['required', 'string', 'max:255'],
            'country' => ['required', 'string', 'max:255'],
            'postal_code' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'unique:users,email,'.auth()->id(), 'max:255'],
            'password' => ['sometimes', 'nullable', 'confirmed', 'min:8', 'max:255'],
        ]);

        $supplier->update([
            'name' => $request['name'],
            'phone' => $request['phone'],
            'address_line_1' => $request['address_line_1'],
            'city' => $request['city'],
            'province' => $request['province'],
            'country' => $request['country'],
            'postal_code' => $request['postal_code'],
        ]);

        $supplier->processes()->sync(collect($request['processes'])->filter->checked->pluck('id'));

        $supplier->user->update([
            'email' => $request['email'] ?? $supplier->user->email,
            'password' =>  is_null($request['password']) ? $supplier->user->password : Hash::make($request['password']),
        ]);

        return response(['message' => __('Profile updated!')], 200);
    }
}
