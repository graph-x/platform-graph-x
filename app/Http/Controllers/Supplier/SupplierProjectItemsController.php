<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use App\Http\Resources\ItemResource;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\SupplierItemResource;
use App\Supplier;
use App\Http\Resources\SupplierItemQuotes;

class SupplierProjectItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Project $project)
    {
        return SupplierItemQuotes::collection(
            $project->quotes()
                ->belongingToSupplier(Supplier::where('user_id', Auth::user()->id)->first()->id)
                ->oldest()
                ->get()
        );
    }
}
