<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Project;
use App\Mail\DeclinedSubmission;
use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class ClientSubmissionTabController extends Controller
{
    /**
     * Function description
     *
     * @param
     * @return
     */
    public function store(Project $project, Request $request)
    {
        abort_unless(Auth::user()->isClient(), 403);

        if (! $project->quotes()->where('approved_by_client', false)->exists()) {
           return response([
               'message' => 'Quotes already approved.'
           ], 200);
        }

        $project->quotesApprovedByAdmin()->each->update([
            'approved_by_client' => true,
            'declined_by_client_at' => null
        ]);

        $project->update([
            'status' => 'submissions_approved_waiting_for_the_tests'
        ]);

        return response([
            'message' => 'Quotes approved.'
        ], 200);
    }

    /**
     * Function description
     *
     * @param
     * @return
     */
    public function destroy(Project $project)
    {
        if (! $project->quotes()->where('approved_by_client', true)->exists() &&
              $project->quotes()->whereNotNull('declined_by_client_at')->exists()) {
            return response([
                'message' => 'Quotes already declined.'
            ], 200);
         }

        $project->quotesApprovedByAdmin()->each->update([
            'approved_by_client' => false,
            'declined_by_client_at' => now()
        ]);

        $project->update([
            'status' => 'awaiting_supplier_prices'
        ]);

        // Notify administrator
        Mail::to(User::where('role', User::ADMIN)->first()->email)->send(new DeclinedSubmission($project));

        return response([
            'message' => 'Quotes declined.'
        ], 200);
    }
}
