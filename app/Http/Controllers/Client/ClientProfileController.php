<?php

namespace App\Http\Controllers\Client;

use App\Client;
use App\Rules\PhoneNumber;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\ClientResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClientProfileController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \App\Http\Resources\ClientResource|\Illuminate\View\View
     */
    public function show(Request $request)
    {
        $client = Client::query()->where('user_id', Auth::id())->firstOrFail();

        if (request()->wantsJson()) {
            return new ClientResource($client);
        }

        return view('client.profile.show', [
            'client' => new ClientResource($client),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Client              $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        abort_unless(auth()->id() === $client['user_id'], 403);

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', new PhoneNumber],
            'address_line_1' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'unique:users,email,' . auth()->id(), 'max:255'],
            'password' => ['sometimes', 'nullable', 'confirmed', 'min:8'],
        ]);

        $client->update([
            'name' => $request['name'],
            'phone' => $request['phone'],
            'address_line_1' => $request['address_line_1'],
            'city' => $request['city'] ?? "",
            'province' => $request['province'] ?? "",
            'country' => $request['country'] ?? "",
            'postal_code' => $request['postal_code'] ?? "",
        ]);

        $client->user->update([
            'email' => $request['email'] ?? $client->user->email,
            'password' => is_null($request['password']) ? $client->user->password : Hash::make($request['password']),
        ]);

        return response(['message' => __('Profile updated.')], 200);
    }
}
