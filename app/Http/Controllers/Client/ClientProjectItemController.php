<?php

namespace App\Http\Controllers\Client;

use App\Item;
use App\Client;
use App\Project;
use App\Quantity;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProjectItemsAsClient;
use App\OptionGroup;

class ClientProjectItemController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Client                                  $client
     * @param  \App\Project                                 $project
     * @param  \App\Item                                    $item
     * @param  \App\Http\Requests\StoreProjectItemsAsClient $request
     * @return void
     */
    public function store(Client $client, Project $project, Item $item, StoreProjectItemsAsClient $request)
    {
        abort_unless(auth()->id() === $client['user_id'], 403);

        if (empty($request['items']) && $project->hasItems()) {
            $project->projectItems()->delete();

            return response(['message' => 'Project items removed successfully.'], 200);
        }

        if (empty($request['items']) && !$project->hasItems()) {
            return response(['message' => 'Nothing to do here.'], 200);
        }

        $this->clearLooseItems($project, $request);

        $items = collect($request['items']);

        // Create or Update item
        $items->each(function ($item) use ($project) {
            $existingItem = Item::whereId($item['id'])->first();

            if (is_null($existingItem)) {
                $createdItem = $this->createNew($item, $project);

                // Assign default option group if none is provided.
                $createdItem->fresh()->optionGroups()->create([
                    'project_id' => $createdItem->project_id,
                    'options' => json_encode([]),
                    'type' => OptionGroup::DEFAULT
                ]);

                foreach ($item['option_groups'] as $group) {
                    $optionGroup = $createdItem->fresh()->optionGroups()->create([
                        'project_id' => $createdItem->project_id,
                        'options' => json_encode($group['options']),
                    ]);
                }
                return;
            }

            $this->handleOptionGroupsOnUpdate($item, $existingItem, $project);

            return $this->updateExisting($existingItem, $item, $project);
        });

        return response(['message' => 'Items updated.'], 200);
    }

    /**
     *  If some project items are present in DB, but are not present in request items data
     *  that means that the user deleted them on the front end - so we remove them from database.
     *
     * @param  \App\Project                                 $project
     * @param  \App\Http\Requests\StoreProjectItemsAsClient $request
     */
    private function clearLooseItems(Project $project, StoreProjectItemsAsClient $request): void
    {
        foreach ($project->projectItems as $item) {
            if (!collect($request->items)->pluck('id')->contains($item->id)) {
                $item->delete();
            }
        }
    }

    /**
     * @param               $item
     * @param  \App\Project $project
     */
    private function createNew($item, $project)
    {
        $createdItem = $project->projectItems()->create($this->getArr($item));

        $quantities = $this->updateOrCreateQuantities($item);

        $createdItem->quantities()->sync($quantities->pluck('id'));

        return $createdItem;
    }

    /**
     * @param $existingItem
     * @param $item
     */
    private function updateExisting($existingItem, $item): void
    {
        $existingItem->update($this->getArr($item));

        $quantities = $this->updateOrCreateQuantities($item);

        $existingItem->quantities()->sync($quantities->pluck('id'));
    }

    /**
     * If there are new quantities present in the item create them,
     * otherwise just update the old ones.
     *
     * @param $item
     * @return \Illuminate\Support\Collection
     */
    private function updateOrCreateQuantities($item): \Illuminate\Support\Collection
    {
        $quantities = collect();

        collect($item['quantities'])->each(function ($quantity) use ($quantities) {
            $quantity = Quantity::updateOrCreate([
                'value' => $quantity['value'],
            ]);

            $quantities->push($quantity);
        });

        return $quantities;
    }

    /**
     * @param $item
     * @return array
     */
    private function getArr($item): array
    {
        return [
            'name' => explode("\n", $item['description'])[0],
            'description' => $item['description'],
            'options' => json_encode($item['options']),
            'delivery_deadline' => $item['delivery_deadline'],
            'address_id' => $item['deliveryAddress']['value'],
            'delivery_contact_id' => $item['deliveryContact']['value'],
        ];
    }

    private function handleOptionGroupsOnUpdate($item, $existingItem, $project)
    {
        if (empty($item['option_groups'])) {
            // clean up, remove old groups
            if ($existingItem->fresh()->optionGroups()->exists()) {
                $existingItem->fresh()->optionGroups->each->delete();
            }

            // Assign default option group if none is provided.
            $existingItem->fresh()->optionGroups()->create([
                'project_id' => $project->id,
                'options' => json_encode([]),
                'type' => OptionGroup::DEFAULT
            ]);

            return;
        }

        $optionGroups = $this->clearLooseOptionGroups($item['option_groups'], $existingItem);

        foreach ($optionGroups as $group) {
            $existingItem->fresh()->optionGroups()->updateOrCreate(
                ['id' => $group['id'], 'project_id' => $project->id],
                ['options' => json_encode($group['options'])]
            );
        }
    }

    private function clearLooseOptionGroups($option_groups,$existingItem)
    {
        $option_groups_from_request = collect($option_groups);

        foreach ($existingItem->optionGroups as $group) {
            if (!$option_groups_from_request->pluck('id')->contains($group->id)) {
                $group->delete();
            }
        }

        return $option_groups_from_request;
    }
}
