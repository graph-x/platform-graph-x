<?php

namespace App\Http\Controllers\Client;

use App\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectSubmissionsResource;
use App\Http\Resources\ProjectEstimatesResource;
use App\Http\Resources\ClientFournisseursTabResource;
use App\Http\Resources\NewFournisseursTabResource;
use App\OptionGroup;
use App\Quantity;
use Carbon\Carbon;
use App\Http\Resources\NewProjectEstimatesResource;

class ProjectSuppliersTabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Project $project)
    {
        abort_unless(auth()->id() === $project->client->user_id, 403);


        $data = $project->quotes->groupBy(function ($item) {
            return $item['option_group_id'] . "_" . $item['quantity_id'];
        });

        $formatted = $data->map(function ($value, $key) {
            // split the key
            $ids = explode("_", $key);
            $option_group_id = (int) $ids[0];
            $quantity_id = $ids[1];
            $option_group = OptionGroup::find($option_group_id);
            $quantity = Quantity::find($quantity_id);
            $quotes = $option_group->quotes()->where('quantity_id', $quantity_id)->whereNotNull('value')->whereNotNull('approved_at')->orderByRaw('CONVERT(value, SIGNED) asc')->with([
                'supplier', 'quantity', 'fournisseur_tab_messages'
            ])->get();
            $has_submitted_estimates = count($quotes) > 0;

            return [
                'key' => $key,
                'option_group_id' => $option_group_id,
                'options' => $option_group->transformOptions(),
                'item_id' => $option_group->item_id,
                'item' => $option_group->item,
                'quantity' => $quantity->value,
                'project_id' => $option_group->project_id,
                'quotes' => $quotes,
                'quotes_request_sent' => $option_group->project->quote_requests_sent_at !== null,
                'quotes_request_sent_at' => Carbon::parse($option_group->project->quote_requests_sent_at)->format('Y-m-d | G:i'),
                'quotes_submitted' => $has_submitted_estimates,
            ];
        })->sort()->values();

        return NewProjectEstimatesResource::collection($formatted);
    }
}
