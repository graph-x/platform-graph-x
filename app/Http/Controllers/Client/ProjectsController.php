<?php

namespace App\Http\Controllers\Client;

use App\Client;
use App\Project;
use App\Status;
use App\Quantity;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Rules\MustBeInteger;
use App\Rules\FirstLineMax255;
use App\Filters\ProjectFilters;
use App\Rules\AtleastOneInArray;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Invoice;
use App\ProjectManagementFee;
use App\OptionGroup;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request    $request
     * @param  \App\Filters\ProjectFilters $filters
     * @return \Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index(Request $request, ProjectFilters $filters)
    {
        if ($request->wantsJson()) {
            $projects = Project::query()
                ->belongingToClient(Auth::user())
                ->filter($filters)
                ->get();
            return response($projects, 200);
        }

        return view('client.projects.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        abort_unless(Auth::user()->isClient(), 404);

        $client = Client::query()->where('user_id', Auth::id())->with('divisions')->first();

        return view('client.projects.create', [
            'client' => $client,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Client              $client
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Client $client, Request $request)
    {
        abort_unless(
            Auth::user()->isClient()
            && $client->user_id === Auth::id(),
            403
        );

        $request->validate([
            'title' => ['required', 'max:100'],
            'short_description' => ['required', 'max:255'],
            'client' => ['required'],
            'division' => ['nullable'],
            'buyer' => ['required'],
            'accounting' => ['required'],
            'require_physical' => ['nullable', 'boolean'],
            'physical_approval' => ['nullable'],
            'physical_address' => ['nullable'],
            'start_date' => ['required', 'date'],
            'end_date' => ['required', 'date'],
            'po_number' => ['required', 'max:20'],
            'notes' => ['nullable', 'string', 'max:500'],
            'items' => [new AtleastOneInArray],
            'items.*.description' => ['required', 'max:500', new FirstLineMax255],
            'items.*.quantities' => ['required', new MustBeInteger()],
            'items.*.delivery_deadline' => ['required'],
            'items.*.deliveryAddress' => ['required'],
            'items.*.deliveryContact' => ['required'],
        ]);

        $project = Project::create([
            'unique_code' => Str::upper(Str::substr(Str::orderedUuid(), 0, 20)),
            'title' => $request['title'],
            'short_description' => $request['short_description'],
            'client_id' => $request['client']['id'],
            'division_id' => $request['division']['id'] ?? $client->default_division()->id,
            'address_id' => $request['physical_address']['id'] ?? null,
            'start' => $request['start_date'],
            'end' => $request['end_date'],
            'po_number' => $request['po_number'],
            'require_pdf' => $request['requires_pdf_approval'],
            'require_physical' => $request['requires_physical_approval'],
            'status' => Status::QUOTE_REQUESTS_TO_BE_MADE,
            'parent_id' => null,
            'notes' => $request['notes'] ?? null,
            'estimates_end_date' => now()->addDays(Project::ESTIMATION_PERIOD_DAYS),
        ]);

        $unique_invoice_id = Str::upper(Str::substr(Str::orderedUuid(), 0, 20));

        $invoice = Invoice::create([
            'unique_code' => "IN-{$unique_invoice_id}",
            'project_id' => $project->id,
            'client_id' => $project->client->id,
            'client_name' => $project->client->name,
            'title' => "Invoice for {$project->title}",
            'description' => $project->short_description,
            'due_date' => now()->addMonth(),
            'paid' => false,
            'status' => Invoice::PRE_INVOICE, // ['open', 'paid', 'draft', 'pre_invoice']
            'currency' => Invoice::DEFAULT_CURRENCY,
            'tps_tax_rate' => Invoice::TPS_TAX_RATE, // in % TPS - taxes Goods and services tax 5%
            'tvq_tax_rate' => Invoice::TVQ_TAX_RATE, // in % TVQ, - taxes  9.975%
            'subtotal' => 0, // SOUS-TOTAL
            'tps_amount' => 0, // TPS amount should be calculated automatically from the line items
            'tvq_amount' => 0, // TVQ amount should be calculated from the line items
            'total_amount' => 0, // need to sum all of the InvoiceLineItems.
            'profit' => 0, // total profit includes project management fee + profit per item?
        ]);

        ProjectManagementFee::create([
            'invoice_id' => $invoice['id'],
            'project_id' => $project['id'],
            'fee' => ProjectManagementFee::DEFAULT_FEE,
            'title' => ProjectManagementFee::DEFAULT_TITLE,
            'description' => ProjectManagementFee::DEFAULT_DESCRIPTION,
        ]);

        // Attach buyer role
        $project->contacts()->attach($request['buyer']['id'], [
            'role' => 'buyer',
        ]);

        // Attach accounting role
        $project->contacts()->attach($request['accounting']['id'], [
            'role' => 'accounting',
        ]);

        // Attach physical_approval role
        if ($request['requires_physical_approval'] && $request['physical_approval']) {
            $project->contacts()->attach($request['physical_approval']['id'], [
                'role' => 'physical_approval',
            ]);
        }

        // Create the items for the project.
        foreach ($request['items'] as $item) {
            $createdItem = $project->projectItems()->create([
                'name' => explode("\n", $item['description'])[0],
                'description' => $item['description'],
                'options' => json_encode($item['options']),
                'delivery_deadline' => $item['delivery_deadline'],
                'address_id' => $item['deliveryAddress']['id'],
                'delivery_contact_id' => $item['deliveryContact']['id'],
            ]);

            $quantities = $this->updateOrCreateQuantities($item);

            $createdItem->quantities()->sync($quantities->pluck('id'));

            // Assign default option group.
            $createdItem->fresh()->optionGroups()->create([
                'project_id' => $project->id,
                'options' => json_encode([]),
                'type' => OptionGroup::DEFAULT
            ]);

            foreach ($item['option_groups'] as $group) {
                $createdItem->fresh()->optionGroups()->create([
                    'project_id' => $project->id,
                    'options' => json_encode($group['options']),
                ]);
            }
        }

        // Response
        return response([
            'message' => __('Project created.'),
            'redirect_route' => route('client.projects.show', [
                'project' => $project->fresh(),
            ]),
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        abort_if($project->client->user_id !== Auth::user()->id, 404);

        $client = Client::with('divisions')->whereId($project->client->id)->first();

        $approved_submissions_count = $project->approvedItems()->count();
        $invoice_amount = $project->invoice()->exists() ? $project->invoice->total_amount : '0.00';

        return view('client.projects.show', [
            'project' => $project->load('division'),
            'client' => $client,
            'statuses' => Status::all(),
            'approved_submissions_count' => $approved_submissions_count,
            'invoice_count' => $project->invoice()->count(),
            'invoice_amount' => $invoice_amount,
        ]);
    }

    /**
     * If there are new quantities present in the item create them,
     * otherwise just update the old ones.
     *
     * @param $item
     * @return \Illuminate\Support\Collection
     */
    private function updateOrCreateQuantities($item): \Illuminate\Support\Collection
    {
        $quantities = collect();

        collect($item['quantities'])->each(function ($quantity) use ($quantities) {
            $quantity = Quantity::updateOrCreate([
                'value' => $quantity['value'],
            ]);

            $quantities->push($quantity);
        });

        return $quantities;
    }
}
