<?php

namespace App\Http\Controllers;

use App\File;
use App\Item;
use App\Project;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProjectItemFileController extends Controller
{
    /**
     * Upload a file for the given item.
     *
     * @param  \App\Project             $project
     * @param  \App\Item                $item
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Project $project, Item $item, Request $request)
    {
        abort_if($item->files()->count() === 20, 400, 'Maximum number of files uploaded.');

        try {
            $file = request('file');
            $fileName = $file->getClientOriginalName();
            $timestamp = now()->timestamp;
            $hashName = "{$timestamp}_{$fileName}";
            $path = Storage::putFileAs('files', $file, $hashName);

            $file = File::create([
                'name' => $fileName,
                'hash_name' => $hashName,
                'path' => $path,
                'project_id' => $project['id'],
                'item_id' => $item['id'],
                'user_id' => Auth::user()->id,
                'approved_at' => null,
                'rejected_at' => null,
            ]);

        } catch (Exception $e) {
            throw $e->getMessage();
        }

        return response()->json([
            'success' => 'You have successfully uploaded file.',
            'file' => $file,
        ], 200);
    }

    /**
     * Download a file by the given hash name.
     *
     * @param $hashName
     * @return mixed
     */
    public function download($hashName)
    {
        return Storage::download("files/{$hashName}");
    }
}
