<?php

namespace App\Http\Resources;

use App\Quote;
use App\Message;
use Illuminate\Http\Resources\Json\JsonResource;

class SupplierItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $item_id = $this->id;
        $project_id = $this->project_id;
        $supplier = $this->suppliers()->where('user_id', auth()->id())->first();

        return [
            'id' => $item_id,
            'project_id' => $project_id,
            'name' => $this->name,
            'description' => $this->description,
            'options' => $this->options ? json_decode($this->options, true) : [],
            'delivery_deadline' => $this->delivery_deadline,
            'deliveryAddress' => $this->deliveryAddress,
            'deliveryContact' => $this->deliveryContact,
            'quantities' => $this->transformQuantities($item_id, $project_id, $supplier['id']),
            'messages' => $this->transformMessages($item_id, $project_id, $supplier['id']),
            'supplier' => $supplier,
            'approved' => $supplier->items()->firstWhere('item_id', $item_id)->pivot->quotes_approved_at !== null,
            'declined' => $supplier->items()->firstWhere('item_id', $item_id)->pivot->quotes_declined_at !== null,
        ];
    }

    /**
     * @param $item_id
     * @param $project_id
     * @param $supplier_id
     * @return mixed
     */
    private function transformQuantities($item_id, $project_id, $supplier_id)
    {
        return $this->quantities->map(function ($quantity) use ($item_id, $project_id, $supplier_id) {
            $quote = Quote::where([
                'project_id' => $project_id,
                'item_id' => $item_id,
                'quantity_id' => $quantity['id'],
                'supplier_id' => $supplier_id,
            ])->first();

            $quote['value'] = $quote['value'] ?? null;

            $quantity['quote'] = $quote;

            return $quantity;
        });
    }

    /**
     * @param $item_id
     * @param $project_id
     * @param $supplier_id
     * @return mixed
     */
    private function transformMessages($item_id, $project_id, $supplier_id)
    {
        return Message::where([
            'item_id' => $item_id,
            'project_id' => $project_id,
            'supplier_id' => $supplier_id,
            'tab' => Message::SUPPLIER_TAB
        ])->get();
    }

}
