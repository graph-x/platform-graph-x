<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StatusResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'key' => $this->key,
          'html_label' => $this->html_label,
          'short_label' => $this->short_label,
          'name' => $this->short_label,
          'hex_color' => $this->hex_color
        ];
    }
}
