<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientAddressTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->formattedAddress(),
        ];
    }

    private function formattedAddress()
    {
        return $this->address_line_1 . (!empty($this->address_line_2) ? ', ' . $this->address_line_2 : '') . ', ' . $this->city . ', ' . $this->province . ', ' . $this->country . ', ' . $this->postal_code;
    }
}