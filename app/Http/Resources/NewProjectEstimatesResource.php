<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NewProjectEstimatesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'key' => $this['key'],
            'option_group_id' => $this['option_group_id'],
            'options' => $this['options'],
            'item_id' => $this['item_id'],
            'item' => $this['item'],
            'quantity' => $this['quantity'],
            'project_id' => $this['project_id'],
            'quotes' => $this['quotes'],
            'quotes_request_sent' => $this['quotes_request_sent'],
            'quotes_request_sent_at' => $this['quotes_request_sent_at'],
            'quotes_submitted' => $this['quotes_submitted']
        ];
    }
}
