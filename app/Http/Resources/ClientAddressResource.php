<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ClientAddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'address_line_1' => $this->address_line_1,
            'city' => $this->city,
            'province' => $this->province,
            'country' => $this->country,
            'postal_code' => $this->postal_code,
            'types' => $this->formatTypes($this->types),
        ];
    }

    /**
     * @param $types
     * @return false[]|\Illuminate\Support\Collection
     */
    private function formatTypes($types)
    {
        if ($types) {
            return collect($types)->flatMap(function ($type) {
                return [
                    $type => true,
                ];
            });
        }

        return [
            'physical_approval' => false,
            'delivery' => false,
        ];
    }
}
