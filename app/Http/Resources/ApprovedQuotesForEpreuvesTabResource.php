<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Message;

class ApprovedQuotesForEpreuvesTabResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // this = quote
        $files = $this->files;

        return [
            'item_id' => $this->item_id,
            'project_id' => $this->project_id,
            'name' => $this->item->name,
            'description' => $this->item->description,
            'options' => $this->optionGroup->transformOptions(),
            'quantity' => $this->quantity->value,
            'files' => $files,
            'quote_id' => $this->id,
            'supplier_id' => $this->supplier_id,
            'option_group_id' => $this->option_group_id,
            'item' => $this->item,
            'supplier' => $this->supplier,
            'is_approved' => $this->isApproved($files),

            'messages' => $this->messages()->where('tab', Message::EPREUVES_TAB)->get()
        ];
    }

    private function isApproved($files)
    {
        $files = $files->reject(function ($file) {
            return $file->rejected_at !== null;
        });

        if ($files->count()) {
            return $files->filter(function ($file) {
                return $file->approved_at === null;
            })->count() === 0;
        }

        return false;
    }
}
