<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Division;

class ClientContactsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'client_id' => $this->client_id,
            'name' => $this->name,
            'phone' => $this->phone,
            'mobile' => $this->mobile,
            'email' => $this->email,
            'roles' => $this->formattedRoles(),
            'divisions' => $this->formattedDivisions($request->client),
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function formattedRoles()
    {
        $roles = collect($this->roles)->map(function ($role) {
            switch ($role) {
                case 'buyer':
                    return [
                        'name' => 'buyer',
                        'checked' => true,
                        'translation' => "Acheteur",
                    ];
                    break;
                case 'pdf_approval':
                    return [
                        'name' => 'pdf_approval',
                        'checked' => true,
                        'translation' => "Approbation PDF",
                    ];
                    break;
                case 'physical_approval':
                    return [
                        'name' => 'physical_approval',
                        'checked' => true,
                        'translation' => "Approbation physique",
                    ];
                    break;
                case 'accounting':
                    return [
                        'name' => 'accounting',
                        'checked' => true,
                        'translation' => "Comptabilité",
                    ];
                    break;
                case 'invoicing':
                    return [
                        'name' => 'invoicing',
                        'checked' => true,
                        'translation' => "Facturation",
                    ];
                    break;
                case 'delivery':
                    return [
                        'name' => 'delivery',
                        'checked' => true,
                        'translation' => "Livraison",
                    ];
                    break;

                default:
                    break;
            }
        });

        return $this->preformattedRoles()->merge($roles->keyBy('name'))->values();
    }

    /**
     * @param Client $client
     * @return mixed
     */
    private function formattedDivisions($client)
    {
        $contactDivisions = $this->divisionsRelation->map(function ($division) {
            return [
                'id' => $division->id,
                'name' => $division->name,
                'checked' => true,
            ];
        })->keyBy(function ($i) {
            return "{$i['id']}_{$i['name']}";
        });

        $clientDivisions = $client->divisions->map(function ($division) {
            return [
                'id' => $division->id,
                'name' => $division->name,
                'checked' => false,
            ];
        })->keyBy(function ($i) {
            return "{$i['id']}_{$i['name']}";
        });

        /*
         * Return all clients divisions merged with contact divisions.
         */
        return $clientDivisions->merge($contactDivisions)->reject(function ($division) {
            return $division['name'] === Division::DEFAULT;
        })->all();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function preformattedRoles()
    {
        return collect([
            [
                'name' => 'buyer',
                'checked' => false,
                'translation' => "Acheteur",
            ],
            [
                'name' => 'pdf_approval',
                'checked' => false,
                'translation' => "Approbation PDF",
            ],
            [
                'name' => 'physical_approval',
                'checked' => false,
                'translation' => "Approbation physique",
            ],
            [
                'name' => 'accounting',
                'checked' => false,
                'translation' => "Comptabilité",
            ],
            [
                'name' => 'invoicing',
                'checked' => false,
                'translation' => "Facturation",
            ],
            [
                'name' => 'delivery',
                'checked' => false,
                'translation' => "Livraison",
            ],
        ])->keyBy('name');
    }
}
