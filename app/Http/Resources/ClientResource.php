<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Division;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->user->email,
            'password' => null,
            'password_confirmation' => null,
            'phone' => $this->phone,
            'address_line_1' => $this->address_line_1,
            'city' => $this->city,
            'province' => $this->province,
            'country' => $this->country,
            'postal_code' => $this->postal_code,
            'user' => ($user = $this->user) ? $user->only(['id', 'name']) : null,
            'divisions' => ($divisions = $this->divisions) ? $divisions->map(function ($division) {
                return [
                    'id' => $division->id,
                    'name' => $division->name,
                    'new_name' => $division->name,
                ];
            })->filter(function ($division) {
                return $division['name'] !== Division::DEFAULT;
            })->toArray() : null,
            'addresses' => ($addresses = $this->addresses) ? $addresses->map(function ($address) {
                return $this->formatAddress($address);
            }) : null,
            'buyer_role_details' => $this->buyerRoleDetails ?? null,
            'contacts_count' => $this->contacts->count(),
            'projects_count' => $this->projects->count(),
            'invoices_count' => 0, // default 0 for now, until the implementation of facturation.
        ];
    }

    private function formatAddress($address)
    {
        {
            return [
                'id' => $address->id,
                'address_line_1' => $address->address_line_1,
                'city' => $address->city,
                'province' => $address->province,
                'country' => $address->country,
                'postal_code' => $address->postal_code,
                'types' => $this->formatTypes($address->types),
            ];
        }
    }

    private function formatTypes($types)
    {
        if ($types) {
            return collect($types)->flatMap(function ($type) {
                return [
                    $type => true,
                ];
            });
        }

        return [
            'physical_approval' => false,
            'delivery' => false,
        ];
    }
}
