<?php

namespace App\Http\Resources;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'project_id' => $this->project_id,
            'name' => $this->name,
            'description' => $this->description,
            'options' => $this->options ? json_decode($this->options, true) : [],
            'delivery_deadline' => $this->delivery_deadline,
            'deliveryAddress' => $this->deliveryAddress,
            'deliveryContact' => $this->deliveryContact,
            'quantities' => $this->quantities,
            'suppliers' => Auth::user()->isClient() ? [] : $this->suppliers,
            'is_linked' => $this->is_linked,
            'parent_item_id' => $this->parent_item_id,
            'parent_item_hash_id' => $this->parent_item ? $this->parent_item->hash_id : null,
            'parent_item' => $this->parent_item ?? null,
            'linked_items' => $this->linked_items ?? [],
            'hash_id' => $this->hash_id ?? null,
            'option_groups' => $this->optionGroups ? $this->optionGroups()->oldest()->get()->map(function ($group) {
                $group['options'] = json_decode($group['options'], true);

                return $group;
            }) : []
        ];
    }
}
