<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SupplierResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => ($user = $this->user) ? $user->only(['id', 'name']) : null,
            'email' => ($user = $this->user) ? $user->email : null,
            'password' => null,
            'password_confirmation' => null,
            'name' => $this->name,
            'phone' => $this->phone,
            'address_line_1' => $this->address_line_1,
            'city' => $this->city,
            'province' => $this->province,
            'country' => $this->country,
            'postal_code' => $this->postal_code,
            'processes' => ($processes = $this->processesRelation) ? $processes->map(function ($process) {
                return [
                    'id' => $process->id,
                    'name' => $process->name,
                    'checked' => true,
                ];
            }) : null,
            'contacts_count' => $this->contacts->count(),
            'projects_count' => $this->projects->count(),
            'archive_count' => 0, // 0 for now, until implementation of archives.
            'factures_count' => 0, // 0 for now, until implementation of factures.
        ];
    }
}
