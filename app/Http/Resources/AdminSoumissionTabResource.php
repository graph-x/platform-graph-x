<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\InvoiceLineItem;

class AdminSoumissionTabResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // Quote that is approved by admin
        $quote_id = $this->id;
        $item = $this->item;
        $option_group = $this->option_group;
        $estimate_value = $this->value;
        $client_price = $this->value_for_client_approval;
        $supplier = $this->supplier;

        return [
            'quote_id' => $quote_id,
            'item' => $item,
            'option_group' => $option_group,
            'estimate_value' => number_format($estimate_value, 2, '.', ''),
            'client_price' => number_format($client_price, 2, '.', ''),
            'admin_profit' => $this->admin_profit ?? number_format(0, 2, '.', ''),
            'supplier' => $supplier,
            'profit_types' => InvoiceLineItem::$profit_types,
            'custom_made' => $this->custom_made,
        ];
    }
}
