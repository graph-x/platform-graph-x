<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class ClientFournisseursTabResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // this = option group
        $option_group_id = $this->id;
        $item_id = $this->item_id;
        $project_id = $this->project_id;
        $quotes = $this->quotes()->whereNotNull('value')->whereNotNull('approved_at')->orderByRaw('CONVERT(value, SIGNED) asc')->with([
            'supplier', 'quantity', 'fournisseur_tab_messages'
        ])->get();

        $has_submitted_estimates = $this->quotes()->whereNotNull('submitted_at')->exists();

        return [
            'option_group_id' => $option_group_id,
            'options' => $this->transformOptions(),
            'item_id' => $item_id,
            'item' => $this->item,
            'quantity' => $this->item->quantities()->first()->value,
            'project_id' => $project_id,
            'quotes' => $quotes,
            'quotes_request_sent' => $this->project->quote_requests_sent_at !== null,
            'quotes_request_sent_at' => Carbon::parse($this->project->quote_requests_sent_at)->format('Y-m-d | G:i'), // hardcoded
            'quotes_submitted' => $has_submitted_estimates, // hardoced
        ];
    }
}
