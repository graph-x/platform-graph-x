<?php

namespace App\Http\Resources;

use App\Quote;
use App\Message;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ProjectSubmissionsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $supplier_id = $this->id;
        $project_id = $this->pivot->project_id;
        $items = $this->transformItems($this->items, $supplier_id, $project_id);

        return [
            'supplier_id' => $supplier_id,
            'supplier_name' => $this->name,
            'items' => $items,
            'quotes_request_sent' => $this->pivot->quotes_request_sent_at !== null, // this is when admin sends items to supplier
            'quotes_request_sent_at' => Carbon::parse($this->pivot->quotes_request_sent_at)->format('Y-m-d | G:i'), // this is when admin sends items to supplier
            //            'quotes_submitted' => $this->pivot->quotes_submitted_at !== null, // this is when supplier submits quotes
            'quotes_submitted' => $this->checkIfQuotesSubmitted($project_id), // this is when supplier submits quotes
            'quotes_submitted_at' => $this->pivot->quotes_submitted_at, // this is when supplier submits quotes
        ];
    }

    /**
     * @param $items
     * @param $supplierId
     * @param $projectId
     * @return mixed
     */
    private function transformItems($items, $supplierId, $projectId)
    {
        return $items->map(function ($item) use ($supplierId, $projectId) {
            $key_part = $item['parent_item_id'] ?? $item['id'];
            $item['key'] = "{$key_part}_{$item['id']}";
            $item['quantities'] = $this->transformItemQuantities($item, $supplierId, $projectId);
            $item['options'] = $this->transformOptions($item['options']);
            $item['parent_item'] = $item->parent_item()->exists() ? $item->parent_item()->get()->pluck('name') : null;

            $item['messages'] = Message::where([
                'item_id' => $item['id'],
                'supplier_id' => $supplierId,
                'project_id' => $projectId,
                'tab' => Message::SUPPLIER_TAB,
            ])->get();

            return $item;
        })->sortBy('key')->all();
    }

    /**
     * @param $item
     * @param $supplierId
     * @param $projectId
     * @return mixed
     */
    private function transformItemQuantities($item, $supplierId, $projectId)
    {
        return $item->quantities->map(function ($quantity) use ($item, $projectId, $supplierId) {
            $quote = Quote::where([
                'project_id' => $projectId,
                'item_id' => $item->id,
                'quantity_id' => $quantity['id'],
                'supplier_id' => $supplierId,
            ])->first();

            $quote['value'] = $quote['value'] ?? null;

            $quantity['quote'] = $quote;

            return $quantity;
        });
    }

    /**
     * @param $options
     * @return \Illuminate\Support\Collection
     */
    private function transformOptions($options)
    {
        return collect(json_decode($options, true))->map(function ($option) {
            return collect([
                $option['label'],
                $option['value'] ?? null,
                $option['textValue'] ?? null,
                $option['numericValue'] ?? null,
                // isset($option['choices']) ? $this->transformChoices($option['choices']) : null,
                isset($option['choices']) ? collect($option['checked'])->implode(",") : null,
                $option['notes'] ?? null,
            ])->filter()->values()->implode(" - ");
        });
    }

    /**
     * @param $choices
     * @return string
     */
    public function transformChoices($choices)
    {
        return collect($choices)->pluck('label')->implode(",");
    }

    /**
     * @param $project_id
     * @return bool
     */
    private function checkIfQuotesSubmitted($project_id): bool
    {
        $supplier = $this;

        return $supplier->items()
            ->where('project_id', $project_id)
            ->withCount([
                'quantities',
                'quotes as supplier_quotes_count' => function ($query) use ($supplier) {
                    $query->where('supplier_id', $supplier['id']);
                }
            ])
            ->get()
            ->reject(function ($item) {
                return $item->supplier_quotes_count === $item->quantities_count;
            })->count() === 0;
    }
}
