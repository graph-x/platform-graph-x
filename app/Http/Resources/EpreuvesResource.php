<?php

namespace App\Http\Resources;

use App\Message;
use Illuminate\Http\Resources\Json\JsonResource;

class EpreuvesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $item_id = $this->id;
        $project_id = $this->project_id;
        $files = $this->files;

        return [
            'id' => $item_id,
            'project_id' => $this->project_id,
            'name' => $this->name,
            'description' => $this->description,
            // 'options' => $this->transformOptions($this->options),
            'files' => $files,
            'is_approved' => $this->isApproved($files),
            'supplier' => $this->suppliers()->whereNotNull('item_supplier.quotes_approved_at')->first(),

            // Return only the messages that belong to epreuves tab.
            'messages' => Message::where([
                'item_id' => $item_id,
                'project_id' => $project_id,
                'tab' => Message::EPREUVES_TAB,
            ])->get(),
        ];
    }

    private function transformOptions($options)
    {
        return collect(json_decode($options, true))->map(function ($option) {
            return collect([
                $option['label'],
                $option['value'] ?? null,
                $option['textValue'] ?? null,
                $option['numericValue'] ?? null,
                // isset($option['choices']) ? $this->transformChoices($option['choices']) : null,
                isset($option['choices']) ? collect($option['checked'])->implode(",") : null,
                $option['notes'] ?? null,
            ])->filter()->values()->implode(" - ");
        });
    }

    private function transformChoices($choices)
    {
        return collect($choices)->pluck('label')->implode(",");
    }

    private function isApproved($files)
    {
        $files = $files->reject(function ($file) {
            return $file->rejected_at !== null;
        });

        if ($files->count()) {
            return $files->filter(function ($file) {
                return $file->approved_at === null;
            })->count() === 0;
        }

        return false;
    }
}
