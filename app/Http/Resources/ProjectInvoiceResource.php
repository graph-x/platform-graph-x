<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class ProjectInvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'unique_code' => $this->unique_code,
            'project_id' => $this->project_id,
            'client_id' => $this->client_id,
            'client_name' => $this->client_name,
            'title' => $this->title,
            'description' => $this->description,
            'due_date' => $this->due_date,
            'paid' => $this->paid,
            'status' => $this->status,
            'currency' => $this->currency,
            'tps_tax_rate' => $this->tps_tax_rate,
            'tvq_tax_rate' => $this->tvq_tax_rate,
            'subtotal' => $this->subtotal,
            'tps_amount' => $this->tps_amount,
            'tvq_amount' => $this->tvq_amount,
            'total_amount' => $this->total_amount,
            'profit' => $this->profit,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'project_management' => $this->project_management_fee,
            'line_items' => $this->transformLineItems($this->line_items),
        ];
    }

    private function transformLineItems($line_items)
    {
        if (!$line_items->count()) {
            return;
        }

        return $line_items->map(function ($line_item) {
            $profit_type = collect($line_item::$profit_types)->filter(function ($i) use ($line_item) {
                return $i['key'] === $line_item['profit_type'];
            })->first();

            $line_item['cost_price'] = $line_item['cost_price'];
            $line_item['approved_price'] = $line_item['approved_price'];
            $line_item['customer_price'] = $line_item['approved_price'];
            $line_item['admin_profit'] = $line_item['admin_profit'];
            $line_item['delivery_date'] = Carbon::parse($line_item['delivery_date'])->format('Y-m-d');
            $line_item['profit_type'] = $profit_type;
            $line_item['profit_types'] = $line_item::$profit_types;
            $line_item['supplier'] = [
                'id' => $line_item['supplier_id'],
                'name' => $line_item['supplier_name'],
            ];

            $line_item['options'] = $line_item->quote()->exists() ? $line_item->quote->optionGroup->transformOptions() : [];;

            return $line_item;
        });
    }
}
