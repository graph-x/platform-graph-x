<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Message;

class SupplierItemQuotes extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $id = $this->id;
        $item_id = $this->item_id;
        $project_id = $this->project_id;
        $supplier_id = $this->supplier_id;
        $option_group_id = $this->option_group_id;

        return [
            'quote_id' => $id,
            'project_id' => $project_id,
            'id' => $item_id,
            'supplier_id' => $supplier_id,
            'option_group_id' => $option_group_id,
            'value' => (float) $this->value,
            'options' => $this->optionGroup ? json_decode($this->optionGroup->options, true) : [],
            'delivery_deadline' => $this->item->delivery_deadline,
            'name' => $this->item->name,
            'description' => $this->item->description,
            'deliveryAddress' => $this->item->deliveryAddress,
            'deliveryContact' => $this->item->deliveryContact,
            'quantity' => $this->quantity,
            'messages' => $this->transformMessages($item_id, $project_id, $supplier_id, $option_group_id),
            'supplier' => $this->supplier,
            'approved' => $this->approved_by_client,
            'declined' => $this->declined_at !== null,
        ];
    }

    /**
     * @param $item_id
     * @param $project_id
     * @param $supplier_id
     * @return mixed
     */
    private function transformMessages($item_id, $project_id, $supplier_id, $option_group_id)
    {
        return Message::where([
            'item_id' => $item_id,
            'option_group_id' => $option_group_id,
            'project_id' => $project_id,
            'supplier_id' => $supplier_id,
            'tab' => Message::SUPPLIER_TAB
        ])->get();
    }
}
