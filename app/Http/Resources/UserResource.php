<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;
use App\Supplier;
use App\Client;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $connected_entity = null;
        $url = null;
        $type = 'Admin';

        if ($this->role === 'client') {
            $client = Client::where('user_id', $this->id);

            if ($client->exists()) {
                $connected_entity = $client->first();
                $url = route('clients.show', $connected_entity);
                $type = 'Client';
            }
        }

        if ($this->role === 'supplier') {
            $supplier = Supplier::where('user_id', $this->id);

            if ($supplier->exists()) {
                $connected_entity = $supplier->first();
                $url = route('suppliers.show', $connected_entity);
                $type = 'Fournisseur';
            }
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'role' => Str::upper($this->role),
            'connected_entity' => [
                'object' => $connected_entity,
                'type' => $type,
                'url' => $url
            ]
        ];
    }
}