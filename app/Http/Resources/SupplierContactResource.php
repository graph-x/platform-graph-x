<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SupplierContactResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'supplier_id' => $this->supplier_id,
            'name' => $this->name,
            'phone' => $this->phone,
            'mobile' => $this->mobile,
            'email' => $this->email,
            'roles' => $this->formattedRoles(),
        ];
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function formattedRoles()
    {
        $roles = collect($this->roles)->map(function ($role) {
            switch ($role) {
                case 'estimator':
                    return [
                        'name' => 'estimator',
                        'checked' => true,
                        'translation' => "Estimateur",
                    ];
                    break;
                case 'expedition':
                    return [
                        'name' => 'expedition',
                        'checked' => true,
                        'translation' => "Expédition",
                    ];
                    break;
                case 'infographics':
                    return [
                        'name' => 'infographics',
                        'checked' => true,
                        'translation' => "Infographie",
                    ];
                    break;
                case 'installer':
                    return [
                        'name' => 'installer',
                        'checked' => true,
                        'translation' => "Installateur",
                    ];
                    break;
                case 'owner':
                    return [
                        'name' => 'owner',
                        'checked' => true,
                        'translation' => "Propriétaire",
                    ];
                    break;
                case 'salesrep':
                    return [
                        'name' => 'salesrep',
                        'checked' => true,
                        'translation' => "Représentant",
                    ];
                    break;

                default:
                    break;
            }
        });

        return $this->preformattedRoles()->merge($roles->keyBy('name'))->values();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    private function preformattedRoles()
    {
        return collect([
            [
                'name' => 'estimator',
                'checked' => false,
                'translation' => "Estimateur",
            ],
            [
                'name' => 'expedition',
                'checked' => false,
                'translation' => "Expédition",
            ],
            [
                'name' => 'infographics',
                'checked' => false,
                'translation' => "Infographie",
            ],
            [
                'name' => 'installer',
                'checked' => false,
                'translation' => "Installateur",
            ],
            [
                'name' => 'owner',
                'checked' => false,
                'translation' => "Propriétaire",
            ],
            [
                'name' => 'salesrep',
                'checked' => false,
                'translation' => "Représentant",
            ],
        ])->keyBy('name');
    }
}