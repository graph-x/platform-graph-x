<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    const SUPPLIER_TAB = 'supplier';
    const EPREUVES_TAB = 'epreuves';

    protected $fillable = [
        'project_id',
        'supplier_id',
        'option_group_id',
        'quote_id',
        'item_id',
        'user_id',
        'text',
        'tab',
    ];

    protected $with = ['creator:id,name,email'];
    protected $appends = ['created_time'];

    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Function description
     *
     * @param
     * @return
     */
    public function getCreatedTimeAttribute()
    {
        return Carbon::parse($this->created_at)->format('Y-m-d');
    }
}