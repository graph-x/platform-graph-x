<?php

namespace App\Filters;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ProjectFilters extends Filters
{
    protected $filters = [
        'month',
        'client',
        'status',
        'search',
    ];

    public function month()
    {
        /** Can be current, previous, last three months, or all months */
        $method = $this->request->month;
        return $this->$method();
    }

    public function current()
    {
        return $this->builder->where('created_at', '>=', now()->startOfMonth());
    }

    public function previous()
    {
        $from = new Carbon('first day of last month');
        $to = new Carbon('last day of last month');

        return $this->builder->whereBetween('created_at', [$from, $to]);
    }

    public function lastThree()
    {
        return $this->builder->where('created_at', '>=', now()->subMonths(2)->startOfMonth());
    }

    public function all()
    {
        return;
    }

    public function status()
    {
        if ($this->request->status === 'tous') {
            return $this->builder;
        }

        return $this->builder->whereStatus($this->request->status);
    }

    public function client()
    {
        if ($this->request->client === 'tous') {
            return $this->builder;
        }

        return $this->builder->where('client_id', $this->request->client);
    }

    public function search()
    {
        $builder = $this->builder
        /** search by projects title and short description name */
            ->where('title', 'LIKE', "%{$this->request['search']}%")
            ->orWhere('short_description', 'LIKE', "%{$this->request['search']}%")
            ->orWhere('unique_code', 'LIKE', "%{$this->request['search']}%");

        if (Auth::user()->isAdmin()) {
            return $builder
            /** search by client name */
                ->orWhereHas('client', function ($query) {
                    $query->where('name', 'LIKE', "%{$this->request['search']}%");
                })

                /** search by children projects */
                ->orWhereHas('children', function ($query) {
                    $query->where('title', 'LIKE', "%{$this->request['search']}%")
                        ->orWhere('short_description', 'LIKE', "%{$this->request['search']}%")
                        ->orWhere('unique_code', 'LIKE', "%{$this->request['search']}%");
                })

                /** search by children projects clients */
                ->orWhereHas('children.client', function ($query) {
                    $query->where('name', 'LIKE', "%{$this->request['search']}%");
                });
        }

        return $builder;
    }
}