<?php

namespace App\Filters;

class UserFilters extends Filters
{
    protected $filters = ['name', 'search'];

    public function name()
    {
        return $this->builder->orderBy('name', $this->request->name);
    }

    public function search()
    {
        return $this->builder
            ->where('name', 'LIKE', "%{$this->request['search']}%")
            ->orWhere('email', 'LIKE', "%{$this->request['search']}%")
            ->orWhere('role', 'LIKE', "%{$this->request['search']}%");
    }
}