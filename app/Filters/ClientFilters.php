<?php

namespace App\Filters;

class ClientFilters extends Filters
{
    protected $filters = ['name', 'status', 'search'];

    public function name()
    {
        return $this->builder->orderBy('name', $this->request->name);
    }

    public function status()
    {
        if ($this->request['status'] === 'tous') {
            return $this->builder;
        }

        return $this->builder->whereHas('projects', function ($query) {
            $query->where('status', $this->request['status']);
        });
    }

    public function search()
    {
        return $this->builder
            ->where('name', 'LIKE', "%{$this->request['search']}%")
            ->orWhere('phone', 'LIKE', "%{$this->request['search']}%")
            ->orWhere('address_line_1', 'LIKE', "%{$this->request['search']}%")
            ->orWhere('city', 'LIKE', "%{$this->request['search']}%")
            ->orWhere('country', 'LIKE', "%{$this->request['search']}%")
            ->orWhereHas('projects', function ($query) {
                $query->where('title', 'LIKE', "%{$this->request['search']}%")
                    ->orWhere('short_description', 'LIKE', "%{$this->request['search']}%");
            });
    }
}