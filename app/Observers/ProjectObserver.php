<?php

namespace App\Observers;

use App\Project;
use App\Utilities\ItemsParser;

class ProjectObserver
{
    /**
     * Handle the project "updated" event.
     *
     * @param  \App\Project $project
     */
    public function saved(Project $project): void
    {
        if ($this->itemsChanged($project)) {
            ItemsParser::handle($project);
        }
    }
    
    /**
     * Are the project items changed?
     *
     * @param  \App\Project $project
     */
    private function itemsChanged(Project $project): bool
    {
        return $project->getOriginal('items') != $project->items;
    }
}
