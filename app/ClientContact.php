<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientContact extends Model
{
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'roles' => 'array',
    ];

    protected $dispatchesEvents = [
        'saved' => \App\Events\ClientContactSaved::class,
    ];

    /**
     * Accessor to retrieve divisions.
     */
    public function getDivisionsAttribute($value)
    {
        return $this->divisions()->pluck('division_id')->toArray();
    }

    /**
     * Mutator to link divisions.
     */
    public function setDivisionsAttribute($value)
    {
        /**
         * We just assign the value to the object so it can be processed by the event listener
         * Using the mutator is required here as otherwise an array to string conversion error is thrown
         */
        $this->divisions = $value;
    }

    /**
     * Defines the {@link ClientContact} to {@link User} model relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * The divisions that has contact
     */
    public function divisions()
    {
        return $this->belongsToMany('App\Division');
    }

    /**
     * The divisions that has contact
     */
    public function divisionsRelation()
    {
        return $this->belongsToMany('App\Division', 'client_contact_division');
    }

    /**
     * The client that has the contact.
     */
    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    /**
     * The projects that has contact
     */
    public function projects()
    {
        return $this->hasMany('App\Project');
    }
}