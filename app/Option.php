<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Str;

class Option extends Model
{
    const TEXT = 'text';
    const NUMERIC = 'numeric';
    const RADIO = 'radio';
    const CHECKBOX = 'checkbox';
    const SELECT = 'select';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function process()
    {
        return $this->belongsTo(Process::class);
    }

    public static function toQueryBuilderRules($includeQuantity = true)
    {
        $validTypes = ['text', 'numeric', 'radio', 'checkbox', 'select'];

        $results = [];

        if ($includeQuantity) {
            $results[] = [
                'id' => 'quantity',
                'type' => 'numeric',
                'label' => __('Quantity'),
            ];
        }

        foreach (self::get() as $option) {
            $result = [
                'id' => Str::slug($option->title, '_'),
                'label' => $option->title,
                'type' => $option->type,
            ];

            if (!in_array($option->type, $validTypes)) {
                $result['type'] = 'text';
            }

            if (in_array($option->type, ['radio', 'checkbox', 'select'])) {
                $result['choices'] = array_map(function ($value) {
                    $value = trim($value);

                    return [
                        'label' => $value,
                        'value' => Str::slug($value),
                    ];
                }, explode("\n", $option->values));
            }

            $results[] = $result;
        }

        return $results;
    }
}