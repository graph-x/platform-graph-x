<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    const OPEN = 'open';
    const PAID = 'paid';
    const DRAFT = 'draft';
    const PRE_INVOICE = 'pre_invoice';

    const DEFAULT_CURRENCY = 'CAD';

    const TPS_TAX_RATE = 0.05; // 5%
    const TVQ_TAX_RATE = 0.09975; // 9.975%

    protected $guarded = [];

    protected $casts = [
        'paid' => 'boolean'
    ];

    protected $with = ['line_items'];

    public function line_items(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(InvoiceLineItem::class, 'invoice_id');
    }

    public function project_management_fee(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(ProjectManagementFee::class, 'invoice_id');
    }

    public function project(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Project::class, 'project_id');
    }
}
