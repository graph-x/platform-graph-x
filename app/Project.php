<?php

namespace App;

use Carbon\Carbon;
use App\Traits\Filterable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;
use App\Events\ProjectUpdated;

class Project extends Model implements Searchable
{
    use Filterable;

    /**
     * Days after the project has been created, in which the estimation is allowed.
     *
     * @var integer
     */
    public const ESTIMATION_PERIOD_DAYS = 7;

    const FIXED_AMOUNT_PROFIT = 'fixed_amount';

    public static $profit_types = [
        [
            'key' => self::FIXED_AMOUNT_PROFIT,
            'name' => 'Montant fixe'
        ]
    ];

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'start' => 'date',
        'end' => 'date',
        'estimates_end_date' => 'date',
        'favorite' => 'boolean',
        'require_pdf' => 'boolean',
        'require_physical' => 'boolean',
        'choose_estimates_automatically' => 'boolean'
    ];

    protected $dispatchesEvents = [
        'saved' => \App\Events\ProjectSaved::class,
        'updated' => ProjectUpdated::class
    ];

    /**
     * The relationships to append to the model's array form.
     *
     * @var array
     */
    protected $with = ['client', 'projectItems'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'formattedDate',
        'buyer',
        'accounting',
        'physical',
        'startDate',
        'endDate',
        'estimatesEnd',
        'canBeEstimated'
    ];
    /**
     * @var string
     */
    public $searchableType = 'Projet';

    /**
     * @return \Spatie\Searchable\SearchResult
     */
    public function getSearchResult(): SearchResult
    {
        $url = route('projects-client.show', $this->id);
        return new SearchResult($this, $this->title, $url);
    }

    /**
     * The client that owns the project.
     */
    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    /**
     * The client that has the division.
     */
    public function division()
    {
        return $this->belongsTo('App\Division');
    }

    /**
     * The client that has the division.
     */
    public function address()
    {
        return $this->belongsTo('App\Address');
    }

    /**
     * The client contacts that belongs to the project.
     */
    public function contacts()
    {
        return $this->belongsToMany('App\ClientContact')->withPivot('role');
    }

    /**
     * The items (DB entity - not the column) of the project.
     */
    public function projectItems(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Item::class, 'project_id');
    }

    public function parent(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Project::class, 'parent_id');
    }

    public function children(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Project::class, 'parent_id');
    }

    public function invoice(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Invoice::class, 'project_id');
    }

    public function pre_invoice(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Invoice::class, 'project_id')->where('status', Invoice::PRE_INVOICE);
    }

    public function getBuyerAttribute()
    {
        $contact = $this->contacts()->wherePivot('role', 'buyer')->first();

        return ($contact) ? $contact->id : false;
    }

    public function setBuyerAttribute($value)
    {
        $this->buyer = $value;
    }

    public function getAccountingAttribute()
    {
        $contact = $this->contacts()->wherePivot('role', 'accounting')->first();

        return ($contact) ? $contact->id : false;
    }

    public function setAccountingAttribute($value)
    {
        $this->accounting = $value;
    }

    public function getDeliveryAttribute()
    {
        // ??
        $contact = $this->contacts()->wherePivot('role', 'delivery')->first();

        return ($contact) ? $contact->id : false;
    }

    public function setDeliveryAttribute($value)
    {
        $this->delivery = $value;
    }

    public function getPdfAttribute()
    {
        $contact = $this->contacts()->wherePivot('role', 'pdf_approval')->first();

        return ($contact) ? $contact->id : false;
    }

    public function setPdfAttribute($value)
    {
        $this->pdf = $value;
    }

    public function setPhysicalAttribute($value)
    {
        $this->physical = $value;
    }

    public function getPhysicalAttribute()
    {
        $contact = $this->contacts()->wherePivot('role', 'physical_approval')->first();

        return ($contact) ? $contact->id : false;
    }

    /**
     * @return mixed
     */
    public function getStartDateAttribute()
    {
        return $this->start->format('yy-m-d');
    }

    /**
     * @return mixed
     */
    public function getEndDateAttribute()
    {
        return $this->end->format('yy-m-d');
    }

    /**
     * @return mixed
     */
    public function getEstimatesEndAttribute()
    {
        return $this->estimates_end_date->format('yy-m-d');
    }

    /**
     * Defines the {@link Project} to {@link Supplier} model relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function suppliers()
    {
        return $this->belongsToMany(Supplier::class, 'supplier_projects')
            ->withPivot('quotes_request_sent_at', 'quotes_submitted_at');
    }

    /**
     * Scope only the projects that belong to logged in supplier.
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function scopeAssignedToAuthSupplier($query)
    {
        return $query->whereHas('suppliers.user', function ($query) {
            return $query->where('suppliers.user_id', auth()->user()->id);
        });
    }

    /**
     * Get the projects formatted start and end date.
     */
    public function getFormattedDateAttribute(): string
    {
        return "{$this->start->format('d/m/y')} - {$this->end->format('d/m/y')}";
    }

    /**
     * @return mixed
     */
    public function unnotifiedSuppliers()
    {
        return $this->projectItems->map(function ($item) {
            return $item->suppliers()->withPivot('notified_at')->whereNull('notified_at')->get();
        })->unique()->flatten();
    }

    /**
     * Check if the project has unnotified suppliers.
     */
    public function getHasUnnotifiedSuppliersAttribute(): bool
    {
        return $this->unnotifiedSuppliers()->isNotEmpty();
    }

    /**
     * Check if the project has items
     */
    public function hasItems(): bool
    {
        return $this->projectItems()->exists();
    }

    /**
     * Scope a query to only include parent projects.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeParentsOnly($query)
    {
        return $query->whereNull('parent_id');
    }

    /**
     * Does the project have children projects.
     */
    public function hasChildren(): bool
    {
        return $this->children()->exists();
    }

    /**
     * Toggle favorite column on project.
     */
    public function toggleFavorite()
    {
        return $this->favorite
        ? $this->update(['favorite' => false])
        : $this->update(['favorite' => true]);
    }

    /**
     * Scope only the projects that belong to given user with supplier role.
     *
     * @param            $query
     * @param  \App\User $user
     * @return mixed
     */
    public function scopeBelongingToSupplier($query, User $user)
    {
        return $query->whereHas('suppliers', function ($q) use ($user) {
            return $q->where('user_id', $user->id);
        });
    }

    /**
     * Scope only the projects that belong to given user with client role.
     *
     * @param            $query
     * @param  \App\User $user
     * @return mixed
     */
    public function scopeBelongingToClient($query, User $user)
    {
        return $query->whereHas('client', function ($q) use ($user) {
            return $q->where('user_id', $user->id);
        });
    }

    /**
     * Is the project in production?
     *
     * @return bool
     */
    public function isInProduction(): bool
    {
        $statuses = [
            'tests_approved_work_in_production',
            'partially_completed_work',
            'work_completed_billing',
        ];

        return in_array($this->status, $statuses);
    }

    /**
     * Scope only the projects that can be estimated.
     *
     * @param $query
     * @return mixed
     */
    public function scopeEstimatable($query)
    {
        return $query->where('status', 'awaiting_supplier_prices');
    }

    /**
     * Can this project be estimated?
     *
     * @return bool
     */
    public function getCanBeEstimatedAttribute(): bool
    {
        return ! $this->isInProduction() && Carbon::parse($this->estimates_end_date)->gte(now());
    }

    /**
     * Approved items of the project. (The items that should be shown in the epreuves tab).
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function approvedItems(): \Illuminate\Database\Eloquent\Collection
    {
        return $this->projectItems()->whereHas('suppliers', function ($query) {
            return $query->whereNotNull('item_supplier.quotes_approved_at');
        })->get();
    }

    /**
     * Approved items of the project. (The items that should be shown in the epreuves tab on supplier).
     *
     * @param  \App\Supplier $supplier
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function approvedItemsForSupplier(Supplier $supplier)
    {
        return $this->projectItems()->whereHas('suppliers', function ($query) use ($supplier) {
            return $query
                ->where('item_supplier.supplier_id', $supplier['id'])
                ->whereNotNull('item_supplier.quotes_approved_at');
        })->get();
    }

    /**
     * Quotes submitted for the project.
     */
    public function quotes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Quote::class, 'project_id');
    }

    /**
     * Approved quotes of the project.
     */
    public function approvedQuotes()
    {
        return $this
            ->quotes()
            ->whereNotNull('approved_at')
            ->with(['item', 'supplier', 'optionGroup'])
            ->get();
    }


    /**
     * Approved quotes of the project.
     */
    public function quotesApprovedByAdmin()
    {
        return $this
            ->quotes()
            ->whereNotNull('approved_at')
            ->with(['item', 'supplier', 'optionGroup'])
            ->get();
    }


    /**
     * Approved quotes for the supplier. (The items that should be shown in the epreuves tab on supplier).
     *
     * @param  \App\Supplier $supplier
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function approvedQuotesForSupplier(Supplier $supplier)
    {
        return $this->quotes()
            ->whereNotNull('approved_at')
            ->where('supplier_id', $supplier->id)
            ->with(['item', 'supplier', 'optionGroup']);
    }

    public function option_groups()
    {
        return $this->hasMany(OptionGroup::class);
    }

    public function management_fee(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(ProjectManagementFee::class, 'project_id');
    }
}
