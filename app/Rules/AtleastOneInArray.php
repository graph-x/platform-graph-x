<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class AtleastOneInArray implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $itemsArray = $value;

        return count($itemsArray) !== 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Au moins un est requis.');
    }
}