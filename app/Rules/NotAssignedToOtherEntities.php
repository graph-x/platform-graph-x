<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Client;
use App\Supplier;

class NotAssignedToOtherEntities implements Rule
{
    public $type;
    public $supplier;
    public $client;

    public function __construct(string $type, Supplier $supplier = null, Client $client = null)
    {
        $this->type = $type;
        $this->supplier = $supplier;
        $this->client = $client;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user_id = $value;

        // For edit form - compare the given user id with the current supplier user id.
        if (!is_null($this->supplier) && ($this->supplier->user) && ($this->supplier->user->id === $user_id)) {
            return true;
        }

        // For edit form - compare the given user id with the current client user id.
        if (!is_null($this->client) && ($this->client->user) && ($this->client->user->id === $user_id)) {
            return true;
        }

        return collect([
            Supplier::where('user_id', $user_id)->count(),
            Client::where('user_id', $user_id)->count(),
        ])->sum() === 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Le {$this->type} est déjà attribué à cet utilisateur.";
    }
}