<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class AtleastOneChecked implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $items = collect($value);

        return $items->count() && $this->hasCheckedItems($items);

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('At least one is required.');
    }

    private function hasCheckedItems($items)
    {
        return $items->contains(function ($item) {
            return isset($item['checked']) ? $item['checked'] === true : false;
        });

    }
}
