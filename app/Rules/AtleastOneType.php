<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class AtleastOneType implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $types = collect($value);

        return $types->count() && $types->contains(function ($type) {
            return $type === true;
        });
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Atleast one type is required.');
    }
}