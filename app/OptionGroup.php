<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OptionGroup extends Model
{
    const DEFAULT = 'default';
    const CUSTOM_MADE = 'custom_made';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $casts = [
        'assign_suppliers' => 'boolean'
    ];


    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function quotes()
    {
        return $this->hasMany(Quote::class);
    }


    public function transformOptions()
    {
        $options = json_decode($this->options, true);

        $options = collect($options)->map(function ($option) {
            switch ($option['type']) {
                case Option::TEXT:
                    return collect([
                        $option['label'],
                        $option['value'] ?? null,
                        $option['textValue'] ?? null,
                        $option['numericValue'] ?? null,
                        isset($option['choices']) ? collect($option['checked'])->implode(",") : null,
                        $option['notes'] ?? null,
                    ])->filter()->values()->implode(" - ");
                    break;

                case Option::NUMERIC:
                    return collect([
                        $option['label'],
                        $option['value'] ?? null,
                        $option['textValue'] ?? null,
                        $option['numericValue'] ?? null,
                        isset($option['choices']) ? collect($option['checked'])->implode(",") : null,
                        $option['notes'] ?? null,
                    ])->filter()->values()->implode(" - ");
                    break;

                case Option::RADIO:
                    return collect([
                        $option['label'],
                        $option['value'] ?? null,
                        $option['textValue'] ?? null,
                        $option['numericValue'] ?? null,
                        isset($option['picked']) ? $option['picked']['label'] : null,
                        $option['notes'] ?? null,
                    ])->filter()->values()->implode(" - ");
                    break;

                case Option::CHECKBOX:
                    return collect([
                        $option['label'],
                        $option['value'] ?? null,
                        $option['textValue'] ?? null,
                        $option['numericValue'] ?? null,
                        isset($option['choices']) ? collect($option['checked'])->implode(",") : null,
                        $option['notes'] ?? null,
                    ])->filter()->values()->implode(" - ");
                    break;

                case Option::SELECT:
                    return collect([
                        $option['label'],
                        $option['value'] ?? null,
                        $option['textValue'] ?? null,
                        $option['numericValue'] ?? null,
                        isset($option['choices']) ? collect($option['choices'])->implode(",") : null,
                        $option['notes'] ?? null,
                    ])->filter()->values()->implode(" - ");
                    break;

                default:
                    return collect([
                        $option['label'],
                        $option['value'] ?? null,
                        $option['textValue'] ?? null,
                        $option['numericValue'] ?? null,
                        isset($option['choices']) ? collect($option['choices'])->implode(",") : null,
                        $option['notes'] ?? null,
                    ])->filter()->values()->implode(" - ");
                    break;
            }
        });

        return $options;
    }

    public function scopeCustomMade($query)
    {
        return $query->where('type', self::CUSTOM_MADE);
    }
}
