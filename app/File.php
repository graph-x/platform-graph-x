<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Uploader of the file.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function uploader()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Item that the file belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }

    /**
     * Project that the file belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class, 'project');
    }

    /**
     * Approve the file.
     */
    public function approve()
    {
        $this->update([
            'approved_at' => now(),
            'rejected_at' => null,
        ]);
    }

    /**
     * Reject the file.
     */
    public function reject()
    {
        $this->update([
            'approved_at' => null,
            'rejected_at' => now(),
        ]);
    }
}
