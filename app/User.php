<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\Filterable;

class User extends Authenticatable
{
    use Notifiable, Filterable;

    /**
     *  Roles.
     */
    const ADMIN = 'admin';
    const CLIENT = 'client';
    const SUPPLIER = 'supplier';
    const DEFAULT = 'not assigned';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role',
        'name',
        'email',
        'password',
        'connected_entity_id',
        'connected_entity_class'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Determine whether the user is admin.
     *
     * @return boolean
     */
    public function isAdmin()
    {
        return $this->role === self::ADMIN;
    }

    /**
     * Determine whether the user is a client.
     *
     * @return boolean
     */
    public function isClient()
    {
        return $this->role === self::CLIENT;
    }

    /**
     * Get the user roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user')->withTimestamps();
    }

    public function assignRole($role)
    {
        if (is_string($role)) {
            $role = Role::where('id', $role)->orWhere('name', $role)->firstOrFail();
        }

        $this->roles()->sync($role);
    }

    public function permissions()
    {
        return $this->roles->map->permissions->flatten()->pluck('name')->unique();
    }

    public function assignEntity($role, $model)
    {
        $this->assignRole($role);

        $this->update([
            'role' => $role,
            'connected_entity_id' => $model->id,
            'connected_entity_class' => get_class($model),
        ]);
    }

    public function assignedRole()
    {
        return $this->roles()->first();
    }

    public function scopeAvailable($query)
    {
        return $query->where([
            'role' => self::DEFAULT,
            'connected_entity_id' => NULL
        ]);
    }

    public function clearConnectedEntity()
    {
        $this->update([
            'connected_entity_id' => NULL,
            'connected_entity_class' => NULL,
            'role' => self::DEFAULT,
        ]);
    }
}