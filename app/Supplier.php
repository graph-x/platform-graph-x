<?php

namespace App;

use App\Traits\Filterable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Supplier extends Model implements Searchable
{
    use Filterable;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    protected $dispatchesEvents = [
        'saved' => \App\Events\SupplierSaved::class,
    ];

    protected $with = ['user', 'processesRelation'];

    /**
     * @var string
     */
    public $searchableType = 'Fournisseur';

    /**
     * @return \Spatie\Searchable\SearchResult
     */
    public function getSearchResult(): SearchResult
    {
        $url = route('suppliers.show', $this->id);
        return new SearchResult($this, $this->name, $url);
    }

    /**
     * Accessor to retrieve activities.
     */
    public function getProcessesAttribute($value)
    {
        return $this->processes()->pluck('process_id')->toArray();
    }

    /**
     * Mutator to link activities.
     */
    public function setProcessesAttribute($value)
    {
        /**
         * We just assign the value to the object so it can be processed by the event listener
         * Using the mutator is required here as otherwise an array to string conversion error is thrown
         */
        $this->processes = $value;
    }

    /**
     * The activities that belongs to the supplier.
     */
    public function processes()
    {
        return $this->belongsToMany('App\Process');
    }

    /**
     * The activities that belongs to the supplier.
     */
    public function processesRelation()
    {
        return $this->belongsToMany('App\Process', 'process_supplier');
    }

    /**
     * The contacts that belongs to the supplier.
     */
    public function contacts()
    {
        return $this->hasMany('App\SupplierContact');
    }

    /**
     * Defines the {@link Supplier} to {@link Project} model relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany(Project::class, 'supplier_projects')
            ->withPivot('quotes_request_sent_at', 'quotes_submitted_at');
    }

    /**
     * Defines the {@link Supplier} to {@link User} model relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Defines the {@link Supplier} to {@link Item} model relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function items()
    {
        return $this->belongsToMany(Item::class)->withPivot('notified_at', 'quotes_approved_at', 'quotes_declined_at');
    }

    public function quotes(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Quote::class);
    }
}
