<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    protected $guarded = [];

    protected $casts = [
        'custom_made' => 'boolean' // if made by admin in soumission tab for client.
    ];

    public function project(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function item(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Item::class, 'item_id');
    }

    public function supplier(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function quantity(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Quantity::class, 'quantity_id');
    }

    /**
     * Query scope for Quotes that belong to given user with supplier role.
     *
     * @param  $query
     * @param  $id
     * @return mixed
     */
    public function scopeBelongingToSupplier($query, $id)
    {
        return $query->where('supplier_id', $id);
    }

    public function optionGroup()
    {
        return $this->belongsTo(OptionGroup::class, 'option_group_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function fournisseur_tab_messages()
    {
        return $this->hasMany(Message::class)->where('tab', Message::SUPPLIER_TAB);
    }

    public function files()
    {
        return $this->hasMany(File::class, 'quote_id');
    }

    public function approved()
    {
        return $this->approved_at !== null;
    }

    /**
     * Function description
     *
     * @param
     * @return
     */
    public function invoice_line_item()
    {
        return $this->hasOne(InvoiceLineItem::class, 'quote_id');
    }
}
